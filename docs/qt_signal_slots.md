# QT Signal et slot <!-- omit in toc -->

Jusqu'à présent nous n'avons rien fait avec le QPushButton qui était dans la fenêtre. 

Le mécanisme QT pour communiquer entre les objets s'appel: `signal et slots`. 

C'est une implémentation du patron de conception [observateur](https://fr.wikipedia.org/wiki/Observateur_(patron_de_conception)). Ce patron est utilisé lorsqu'un objet veut notifier d'autres objets lorsque ce premier change d'état. 
Dans notre cas, nous voulons que le programme se termine lorsque nous appuierons sur le bouton. 

Un `signal` est un message qu'un objet peut envoyer. Par exemple, QPushButton peut envoyer les signaux suivant: `clicked`, `pressed`, `released`. 

Une `slot` est une fonction qui recevra et répondra au signal. Par exemple, la classe QApplication a une slot `quit` qui peut être associée à un signal. 

Afin d'associer une signal à une slot, il faut utiliser la fonction [connect](https://doc.qt.io/qt-6/qobject.html#connect) de la classe [QObject](https://doc.qt.io/qt-6/qobject.html).

Voyons un exemple. Nous allons connecter notre bouton à la slot quit() de l'application. 

Afin d'accéder à l'application, il suffit d'utiliser `QApplication::instance()` ou plus simplement `qApp`. 

Vous devez donc ajouter cette ligne au début de `fenetre.cpp`

```c
#include <QApplication>
```

et celle-ci à la fin

```c
    connect(m_bouton, &QPushButton::clicked, QApplication::instance(), &QApplication::quit);
```

!> notez le `&` devant le nom de la classe. C'est donc l'adresse de la fonction (eh oui, les fonctions aussi ont des adresses)


Si vous redémarrez le programme et appuyez sur le bouton, le programme se fermera. 

!> ATTENTION une autre syntaxe pour le `connect` est la suivante:\
 `connect(m_bouton, SIGNAL (clicked()), QApplication::instance(), SLOT (quit()));`\
 Bien qu'elle soit fonctionnelle, le problème est que le compilateur ne peut pas vérifier si \
 la fonction quit() existe et si les paramètres sont bons.\
 Il est donc recommandé d'utiliser la syntaxe tel que démontré ci-haut.\
 Mais parfois (comme nous allons le voir dans le 2e exemple ci-dessous) la syntaxe recommandée peut être \
 difficile à utiliser. Vous pourrez alors utiliser cette seconde syntaxe avec SIGNAL et SLOT.

L'exemple précédent était un peu trivial, essayons en un plus parlant. 

Changez `main.cpp` pour :

```c
#include <QApplication>
#include <QProgressBar>
#include <QSlider>

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 QWidget fenetre;
 fenetre.setFixedSize(200, 80);

 // Création d'une bar de progression 
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QProgressBar *barDeProgression = new QProgressBar(&fenetre);
 barDeProgression->setRange(0, 100);
 barDeProgression->setValue(0);
 barDeProgression->setGeometry(10, 10, 180, 30);

 // Création d'un curseur horizontal
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QSlider *curseur = new QSlider(&fenetre);
 curseur->setOrientation(Qt::Horizontal);
 curseur->setRange(0, 100);
 curseur->setValue(0);
 curseur->setGeometry(10, 40, 180, 30);

 fenetre.show();

 // Création de la connection entre le curseur et la bar de progression
 QObject::connect(curseur, &QSlider::valueChanged, barDeProgression, &QProgressBar::setValue);

 return app.exec();
}
```

Lorsque la valeur du curseur change, le signal [valueChanged](https://doc.qt.io/qt-6/qabstractslider.html#valueChanged) est envoyé, et la barDeProgression écoute via la slot [setValue](https://doc.qt.io/qt-6/qprogressbar.html#value-prop).

!> Remarquez ici que `fenetre` n'est plus de la classe Fenetre. C'est un simple widget. Je l'ai changé pour vous montrer que le fait de mettre un widget sans parent fait en sorte qu'il devient une fenêtre. 

!> Remarquez aussi que le fait d'avoir mis les widgets dans le main() fait en sorte qu'on ne peut pas utiliser simplement connect(). On doit utiliser QObject::connect() car main n'utilise pas la macro Q_OBJECT.

## Plusieurs slots connectées au même signal. 

 Changez le code de `main.cpp` pour :

```c
#include <QApplication>
#include <QProgressBar>
#include <QSlider>
#include <QLCDNumber>

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 QWidget fenetre;
 fenetre.setFixedSize(200, 120);

 // Création d'une bar de progression
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QProgressBar *barDeProgression = new QProgressBar(&fenetre);
 barDeProgression->setRange(0, 100);
 barDeProgression->setValue(0);
 barDeProgression->setGeometry(10, 10, 180, 30);

 // Création d'un affichage LCD
 QLCDNumber *volumeLCD = new QLCDNumber(&fenetre);
 volumeLCD->setPalette(Qt::red);
 volumeLCD->setGeometry(10, 40, 180,30);

 // Création d'un curseur horizontal
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QSlider *curseur = new QSlider(&fenetre);
 curseur->setOrientation(Qt::Horizontal);
 curseur->setRange(0, 100);
 curseur->setValue(0);
 curseur->setGeometry(10, 70, 180, 30);

 fenetre.show();

 // Création de la connection entre le curseur et la bar de progression
 QObject::connect(curseur, SIGNAL (valueChanged(int)), barDeProgression, SLOT (setValue(int)));
 
 // Création de la connection entre le curseur et l'affichage LCD
 QObject::connect(curseur, &QSlider::valueChanged, volumeLCD, qOverload<int>(QLCDNumber::display));

 return app.exec();
}
```

!> Notez `qOverload` dans la dernière connexion. Cette macro est nécessaire afin d'indiquer au compilateur que c'est la fonction `diplay` prenant un `int` en paramètre qu'il faut utiliser. En effet, [QLCDNumber](https://doc.qt.io/qt-6/qlcdnumber.html#display-2) à plusieurs slot display

!> Notez aussi que les deux syntaxes pour le connect sont utilisées. 


## Plusieurs signaux à la même slot

```c
#include <QApplication>
#include <QProgressBar>
#include <QSlider>
#include <QLCDNumber>
#include <QDial>

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 QWidget fenetre;
 fenetre.setFixedSize(200, 200);

 // Création d'une bar de progression
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QProgressBar *barDeProgression = new QProgressBar(&fenetre);
 barDeProgression->setRange(0, 100);
 barDeProgression->setValue(0);
 barDeProgression->setGeometry(10, 10, 180, 30);

 // Création d'un affichage LCD
 QLCDNumber *volumeLCD = new QLCDNumber(&fenetre);
 volumeLCD->setPalette(Qt::red);
 volumeLCD->setGeometry(10, 40, 180,30);

 // Création d'un bouton de volume
 QDial *volumeDial= new QDial(&fenetre);
 volumeDial->setNotchesVisible(true);
 volumeDial->setMinimum(0);
 volumeDial->setMaximum(100);
 volumeDial->setGeometry(10,70,180,50);

 // Création d'un curseur horizontal
 // ayant des valeurs entre 0 et 100, et une valeur de départ de 0
 QSlider *curseur = new QSlider(&fenetre);
 curseur->setOrientation(Qt::Horizontal);
 curseur->setRange(0, 100);
 curseur->setValue(0);
 curseur->setGeometry(10, 130, 180, 30);

 fenetre.show();

 // Création de la connection entre le curseur et la bar de progression
 QObject::connect(curseur,&QSlider::valueChanged, barDeProgression, &QProgressBar::setValue);

 // Création de la connection entre le curseur de volume et l'affichage LCD
 QObject::connect(curseur, &QSlider::valueChanged, volumeLCD, qOverload<int>(&QLCDNumber::display));

 // Création de la connection entre le `dial` et l'affichage LCD
 QObject::connect(volumeDial, &QDial::valueChanged, volumeLCD , qOverload<int>(&QLCDNumber::display));

 return app.exec();
}


```

Ici, l'affichage LCD est connecté au curseur et au *dial*. Notez que la `valeur` du dial est transférée directement à l'affichage, et non pas additionnée.  

Ici aussi, `qOverload` est requis. 