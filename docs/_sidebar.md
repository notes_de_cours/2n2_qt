<!-- docs/_sidebar.md -->
- 0  TPs
  - [TP3](tp3_reversi_h23.md)
  - [TP4](tp4_code_a_barre.md)
  - 
- 0.5
  - [Temps tp2](temp_tp2.md)                                  <!-- 1h -->
- 1 Introduction seconde partie: QT
    - [Introduction](/)
- 2 Premier contact  
   - [Premier contact avec QT](qt_intro.md)                    <!-- 0.25h-->
   - [Widget](qt_widget.md)                                    <!-- 0.25h-->
   - [Widget sousclasse](qt_widget_sousclasse.md)              <!-- 0.25h-->
   - [Signal et slots](qt_signal_slots.md)                     <!-- 1h-->
   - [Creation de signal et slot](qt_signal_slots_creation.md) <!-- 0.5h--> 
   - [Exercices](ex_utilisation_widget.md)
   - [Fin du cours](fin_cours8.md)
- Layout <!-- semaine 9 -->
   
   - [Layout](qt_layout.md)                                    <!-- 1h -->
   - [tic tac toe](qt_demo_tictactoe.md)                       <!-- 2h -->
   - [TP3](tp3_reversi_h23.md)
- Documentation du code  <!-- semaine 10 -->
  - [Doxygen](doxygen.md) <!-- 0.25h -->
  - [Documentation](c_documentation.md) <!-- 1h -->
- Algorithmes pour TP3 Reversi
  - [Détection de voisins](ex_reversi_algo_adjacent.md)
  - [Capture](ex_reversi_algo_capture.md) 
- QT Designer
  - [Introduction à Designer](qt_designer_1.md)
- Internationalisation
  - [Projet designer](qt_localisation1.md)
  - [widgets](qt_localisation2.md)
- TP4
  - [TP4](tp4_code_a_barre.md)


   <!-- montrer quelques widgets hors contex QMainWindow, setcentralewidget, QGroupBox -->
   <!-- retour Tp 2 -->
   <!-- enonce TP 3 : refaire tp2 en graphique -->
<!-- - Gestionnaire de Repas <!-- Semaine 9 -->
<!--    - [demo Resto](qt_layout_demo.md)                   <!-- 2h -->

 <!--  - [revue](qt_layout_demo_revue.md)                         <!-- 0.5h -->
 <!--  - [demo Resto bouton](qt_layout_demo_part2.md)             <!-- 0.5h -->
 <!--  - [demo Resto fichier](qt_layout_demo_part3.md)               <!-- 1h   -->
 <!--  - [demo Resto final](qt_layout_demo_part4.md)               <!-- 1h -->
 <!--  - [demo Resto bug](qt_layout_demo_part5.md)                 <!-- 0.5h --> 

   <!-- explicit https://en.cppreference.com/w/cpp/language/converting_constructor -->
   <!-- cast obligatoire sur calloc ( devrais-je faire une classe pour les menu et repas ? )-->
   <!-- QString a char* https://wiki.qt.io/Technical_FAQ#How_can_I_convert_a_QString_to_char.2A_and_vice_versa.3F -->
   <!-- CString vs string >
   <!-- dialog -->
   <!--  https://medium.com/genymobile/how-c-lambda-expressions-can-improve-your-qt-code-8cd524f4ed9f -->
   <!-- image son et vidéos -->
   <!-- TP3 GUI refaire tp2 en GUI -->
   <!-- TP4 ajouter image son video avec accès direct dans le fichier-->
   <!-- thread -->
   <!-- [Documentation](c_commentaire.md) doxygen  -->
   <!-- localisation : qt linguist -->
   <!-- testing : Qtest -->
   <!-- compilation manuelle pour generer un .exe msys2 make-->
   <!-- command line argument argc argv -->
   <!-- cast -->
   <!-- preprocessor -->
   <!-- typedef vs define -->
   <!-- Exercices https://www.w3schools.com/c/exercise.php?filename=exercise_syntax1 -->
   <!-- tableau multidimension ? -->
   <!-- bitwise operator  << >> -->
   <!-- récursivité ? non, c'est dans poo1 -->
