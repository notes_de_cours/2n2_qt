# QT Layout demo partie 4: finalisation <!-- omit in toc -->

Il reste les étapes suivantes à faire:

- sauvegarder le menu dans le fichier
- permettre de vider le menu à l'écran

Voici le code au complet, suivit d'explications.

<details>
<summary>code final</summary>

<details>
<summary>gestionnairerepas.h</summary>

```c 
#ifndef GESTIONNAIREREPAS_H
#define GESTIONNAIREREPAS_H

#include "repas.h"

class GestionnaireRepas
{
public:
    GestionnaireRepas(char *aNomFichier); // D7
    void chargerMenu(menu *aMenu);

    void sauvegarderMenu(menu aMenu);
private:
    char nomFichier[50];  // D5
};

#endif // GESTIONNAIREREPAS_H

```
</details>
<details>
<summary>gestionnairerepas.cpp</summary>

```c 
#include "gestionnairerepas.h"
#include <string.h>
#include <stdio.h>
#include <cstdlib>

GestionnaireRepas::GestionnaireRepas(char *aNomFichier)  // D6
{
    strcpy( nomFichier, aNomFichier); // D8
}


/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void GestionnaireRepas::chargerMenu(menu *aMenu) {  // D9
    FILE *lFichier = fopen(nomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = (repas *)calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = (item *)calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = (repas **)realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}

/**
 * @brief sauvegarde sur disque du menu au complet
 *
 * par simplicité, le fichier est écrasé à chaque écriture
 *
 * @param aMenu le menu à sauvegarder
 */
void GestionnaireRepas::sauvegarderMenu(menu aMenu) { //E1
    FILE *lFichier = fopen(nomFichier, "w");
    repasFichier lRepasASauvegarder;
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        strcpy(lRepasASauvegarder.titre, aMenu.listeRepas[i]->titre);
        lRepasASauvegarder.nombreItem =  aMenu.listeRepas[i]->nombreItem;
        fwrite(&lRepasASauvegarder, sizeof(repasFichier),1, lFichier);
        for(int j = 0; j<aMenu.listeRepas[i]->nombreItem; j++) {
            fwrite(&aMenu.listeRepas[i]->listeItem[j], sizeof(item), 1, lFichier);
        }
    }
    fclose(lFichier);
}

```
</details>
<details>
<summary>fenetreprincipale.h</summary>

```c 
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>
#include "repas.h"

class QPushButton;
class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void slotAjouterRepas();
    void slotAjouterItem();
    void slotItemModifie(QString valeur);
    void slotQuitter();
    void slotChargerMenu();
    void slotSauvegarderMenu();
    void slotViderMenu();
    void slotEffacerItem();
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();
    void convertirMenuEnQTableWidget(menu *aMenu);

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
    QLineEdit *nomItem;
    QLineEdit *prixItem;

    QPushButton *boutonAjoutRepas; // C1
    QPushButton *boutonAjoutItem;
    menu convertirQTableIWidgetEnMenu(QTableWidget *aTable);
};
#endif // FENETREPRINCIPALE_H


```
</details>
<details>
<summary>fenetreprincipale.cpp</summary>

```c 
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"
#include <QMenu>
#include <QMenuBar>
#include <QApplication>

#include "gestionnairerepas.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier")); // D1
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::slotQuitter);

    QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));  // D2
    menuMenu->addAction("Charger", this, &FenetrePrincipale::slotChargerMenu);
    menuMenu->addAction("Sauvegarder", this, &FenetrePrincipale::slotSauvegarderMenu); // E2
    menuMenu->addAction("Vider", this, &FenetrePrincipale::slotViderMenu); // E3

    QWidget *fenetre = new QWidget(); // 2a
    QHBoxLayout *grille = new QHBoxLayout; // 3

    grille->addWidget(constructionBoiteEntrees()); // 4 // 9
    grille->addWidget(constructionSectionAffichage()); // 17

    fenetre->setLayout(grille); // 3a
    setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18 // C2
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterRepas); // 19

    //création de l'entrée pour le nom et le prix de l'item
    nomItem = new QLineEdit; // B1
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00"); //B2
    connect(nomItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie); // C7
    connect(prixItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie);

    QFormLayout *champsItemLayout = new QFormLayout; // B3
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4 // C2a
    boutonAjoutItem->setEnabled(false); // C3 // disable par defaut, tant qu'un prix n'est pas entré.

    connect(boutonAjoutItem, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterItem);

    // bouton pour effacer  un item
    QPushButton *boutonEffacerItem = new QPushButton("Effacer la ligne sélectionnée"); // E12
    connect(boutonEffacerItem, &QPushButton::clicked, this, &FenetrePrincipale::slotEffacerItem);

    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    grille->addLayout(champsItemLayout); // B5
    grille->addWidget(boutonAjoutItem);
    grille->addWidget(boutonEffacerItem); // E12a

    boite->setLayout(grille);

    return boite; // 8
}

/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    //change le mode et le comportement de la selection
    affichageMenu->setSelectionBehavior( QAbstractItemView::SelectRows ); // E13
    affichageMenu->setSelectionMode( QAbstractItemView::SingleSelection );

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}


void FenetrePrincipale::convertirMenuEnQTableWidget(menu *aMenu)
{
    for(int i = 0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        affichageMenu->insertRow(affichageMenu->rowCount());
        affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(QString::fromLatin1(lRepas->titre)));
        for(int j=0; j<lRepas->nombreItem; j++) {
            item lItem = lRepas->listeItem[j];
            affichageMenu->insertRow(affichageMenu->rowCount());
            affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(QString::fromLatin1(lItem.titre)));
            affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(QString::number(lItem.prix)));
        }
    }
}

menu FenetrePrincipale::convertirQTableIWidgetEnMenu(QTableWidget *aTable) // E5
{
    menu lMenu;
    repas *lRepas;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    for(int i = 0; i<aTable->rowCount(); i++) {
        if((aTable->item(i,0))) {
            //s'il y a qqchose dans la colonne 0, c'est donc un repas
            lRepas = (repas *)calloc(1, sizeof(repas)); //sur le tas
            //conversion du titre qui est dans la QString qui est dans la table, vers char*
            QByteArray ba = aTable->item(i,0)->text().toLocal8Bit(); // E6
            strcpy(lRepas->titre, ba.data()); // E7
            if(!lMenu.listeRepas) { // E8
                lMenu.listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                lMenu.listeRepas = (repas **)realloc(lMenu.listeRepas, (lMenu.nombreRepas+1) *sizeof(repas*));
            }
            lMenu.listeRepas[lMenu.nombreRepas] = lRepas;
            lMenu.nombreRepas++;
        } else {
            //sinon c'est un item
            if(!lRepas->listeItem) { // E9
                lRepas->listeItem = (item *)calloc(1, sizeof(item));
            } else {
                lRepas->listeItem = (item *)realloc(lRepas->listeItem, sizeof(item)*(lRepas->nombreItem+1));
            }
            QByteArray ba = aTable->item(i,1)->text().toLocal8Bit();
            strcpy(lRepas->listeItem[lRepas->nombreItem].titre, ba.data());
            //conversion du prix qui est dans la QSTring vers float.
            lRepas->listeItem[lRepas->nombreItem].prix = aTable->item(i,2)->text().toFloat(); // E10
            lRepas->nombreItem++;
        }
    }
    return lMenu;
}
// slots
void FenetrePrincipale::slotAjouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
  //boutonAjoutItem->setEnabled(true); // C4
  boutonAjoutRepas->setEnabled(false); // C5
}

void FenetrePrincipale::slotAjouterItem() { //B6
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
    boutonAjoutRepas->setEnabled(true); // C6
    boutonAjoutItem->setEnabled(false); // C9
}

/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::slotItemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." ) { // C8
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}


/**
 * @brief sortie de l'application
 */
void FenetrePrincipale::slotQuitter() { // D3
    QApplication::quit();
}

void FenetrePrincipale::slotChargerMenu() {  // D4
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt"); //D10
    menu lMenu;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    gestionRepas->chargerMenu(&lMenu); // D11
    convertirMenuEnQTableWidget(&lMenu); // D12
}

void FenetrePrincipale::slotSauvegarderMenu() { // E4
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt");
    gestionRepas->sauvegarderMenu(convertirQTableIWidgetEnMenu(affichageMenu));
}

void FenetrePrincipale::slotViderMenu() { // E11
    affichageMenu->setRowCount(0);

}

void FenetrePrincipale::slotEffacerItem() { // E14
    QList itemsSelectionnes = affichageMenu->selectedRanges();
    if(!itemsSelectionnes.isEmpty()) {
        int ligneSelectionnee = itemsSelectionnes.first().topRow();
        affichageMenu->removeRow(ligneSelectionnee);
        affichageMenu->clearSelection();
    }
}
```
</details>

</details>

## Explications

**GestionnaireRepas**

`E1` ajout de la fonction pour sauvegarder le menu. C'est une copie de ce que nous avions fait en mode console.

**FenetrePrincipale**

`E2` et `E3` ajout des options dans le menu

`E4` la slot pour sauvegarder le menu. Notez l'appel a convertirQTableIWidgetEnMenu. 

`E5` la fonction qui converti la table en menu. 

Cette fonction parcourt le tableau. S'il y a du texte dans la colonne 1, c'est le début d'un repas. Si non, c'est un item d'un repas. Notez qu'aucune validation n'est faite. Si un item n'a pas de repas avant, ou que deux repas se suivent, ca va planter. 

`E6` et `E7` conversion d'un QString en char. 

`E8` si c'est le premier repas, on fait un calloc, sinon un realloc afin d'agrandir la liste des repas. 

`E9` ajout d'un item dans un repas
 
`E10` conversion d'un QString en float. 

`E11` la slot pour vider le menu. 

`E12` et `E12a` ajout du bouton pour effacer une ligne dans la table

`E13` change le mode et le comportement de la selection de la table

`E14` la slot pour effacer une ligne dans la table

**Explications pour l'effacement d'une ligne.**

Pour effacer une ligne dans la table, il faut que la table soit mise en mode de sélection uni-ligne (E13).

Le bouton pour effacer (E12) appel la slot (E14) qui va chercher la ligne qui est sélectionnée dans la table et l'enlève de la table. 

Les différents modes de sélection et les comportements peuvent être trouvés sur :

https://doc.qt.io/qt-6/qabstractitemview.html#selectionBehavior-prop

## Questions

Dans la fonction `convertirQTableIWidgetEnMenu` avez-vous remarqué que la variable retournée `lMenu` a été créée sur la pile?

Alors comment se fait-il qu'on puisse la retourner?

<details>
<summary>Réponse</summary>

Lorsqu'elle est passée en paramètre ou retournée d'une fonction, une structure est copiée comme tout autre type de base (int, char, float).

Il faut quand même faire attention de ne pas surcharger le programme en copiant sans cesse les structures. Si la structure prend beaucoup de mémoire ou que vous en manipulez une grande quantité, il serait peut-être plus efficace d'utiliser un pointeur. 
</details>


