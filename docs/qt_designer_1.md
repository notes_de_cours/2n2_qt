# Introduction à Qt designer <!-- omit in toc -->

Jusqu'à maintenant, nous avons construit nos interfaces manuellement. Mais Qt Creator offre un outil permettant de créer les interfaces graphiquement en déposant les widgets et les layouts sur un canevas. 

Il existe un troisième mode que nous n'allons pas explorer: QT quick.

## L'interface de designer

Ouvrez un nouveau projet dans QT, choisissez `Qt Widgets Application`. Appelez le projet `demo_designer`. 

Cette fois-ci, sur la page `Class Information` gardez toutes les options par défaut. Vous devez donc laisser la sélection pour `Generate form` sélectionnée. 

C'est cette dernière option qui fera en sorte que nous pourrons utiliser le designer. 

Ouvrez le fichier `mainwindow.cpp`. 

On peut voir que le constructeur contient la ligne `ui->setupUi(this)`. C'est cette ligne qui va démarrer l'interface que nous allons développer dans le designer.

Ouvrez maintenant la section `Formulaires` (ou `Forms`) et double cliquez sur `mainwindow.ui`. Nous voilà maintenant dans `designer`. 


![interface_designer](_media/designer_1.png)

1) liste des widgets et des layouts
2) le canevas
3) liste des composants sur le canevas (utile pour connaitre leur nom)
4) propriétés du composant sélectionné
5) éditeur de signal et slot
6) sélection du mode d'édition

## Premier exemple

Sélectionnez-le `layout vertical` et déposez-le en haut à gauche du canevas. 

Déposez un widget `Dial` dans ce layout.

Déposez ensuite un `Progress bar` sous le dial.

Vous pouvez agrandir le layout afin de laisser de la place aux 2 widgets. 

Sauvegardez (ctrl-s)

Sortez de designer en cliquant sur le X à la droite de mainwindow.ui.

Exécutez le programme...  rien d'excitant!

Nous allons maintenant associer le dial à la progress bar via des signaux et slot. 

Réouvrez mainwindow.ui. 

Sélectionnez le mode `Éditez signaux et slot` en sélectionnant le 2e icône de la section 6 de l'interface (ou F4). 

Sélectionnez le dial. Il devrait devenir rouge. 

Glissez vers la progress bar et relâchez. 

Un menu listant les signaux de QDial et les slots de QProgressBar s'ouvrira. Sélectionnez `valueChange(int)` et `setValue(int)`

![designer_signal_et_slot](_media/designer_signal_slot.png)

La connexion s'affichera dans la section 5 de l'interface. 

Redémarrez le programme et changez la valeur du dial. 

Si vous avez remarqué, au début, le slider est à la valeur 24. Pour changer cette valeur de départ, retournez en mode édition en sélectionnant la première icône de la section 6 (ou F3).  Sélectionnez le slider et allez changer la valeur de `value` dans ses propriétés. Mettez 0 et redémarrez le programme. 

## Communication avec Mainwindow.

Nous venons de voir comment faire interagir 2 widgets entre eux. Mais si je veux interagir avec le programme, comment faire? Il est possible de communiquer directement avec mainwindow. 

Sélectionnez le dial, clic de droit, et `Aller aux slots...`

Sélectionnez `valueChanged(int)` et OK. L'éditeur s'ouvrira sur la fonction `on_dial_valuedChanged(int value)`. Dans celle-ci, inscrivez le code suivant:

```c
qDebug() << value;
```

Redémarrez le programme et changez la valeur du dial. Dans la `sortie de l'application` vous verrez apparaitre la valeur du dial. 

Notez que dans `mainwindow.h`, `on_dial_valueChanged()` a été ajouté dans la section `private slots`.

Si vous créez une connexion par erreur, il suffit d'effacer son nom dans le .h ainsi que la fonction dans le .cpp. 

## Communication de mainwindow à un widget. 

Nous allons maintenant faire la communication inverse: envoyer une valeur de mainwindow vers un widget. 

Pour ce faire, ajouter un `Label` sur le canvas. Remarquez que dans la section 3 de l'interface, les widgets et layouts sont tous listés. La première colonne est le nom du widget ou layout. Ce nom est utilisé afin de communiquer avec ce composant. 

Changez le nom du label pour `affichage`. 

Sauvegardez le ui, et fermez-le. 

Dans la slot `on_dial_valueChanged()`, ajoutez le code suivant 

```c
    ui->affichage->setNum(value);
```

Notez que `affichage` est le nom que nous venons de donner au label. 

Redémarrez le programme et changez le dial. 

### version alternative

Vous aurez peut-être remarqué que la dernière connexion aurait aussi été possible en connectant directement le dial au label. Essayons-le! 

Commencez par commenter la dernière ligne ajoutée dans `on_dial_valueChanged()`

Retournez dans designer et ajoutez une connexion entre le dial et le label. Utilisez le signal `valueChanged(int)` et la slot `setNum(int)`. 

Redémarrez le programme ... oups!  erreur???

La slot `setNum()`  de QLabel a 2 `overload`. Il est donc impossible pour designer de savoir laquelle prendre (en tous cas, je n'ai pas trouvé comment le faire :) )

## Ajout de menu

Pour ajouter un menu, vous pouvez cliquer sur le bouton `Taper ici` en haut de l'interface dans designer. 

![taperici](_media/taperici.png)

Cela vous permettra d'ajouter un menu et d'ensuite ajouter les options de ce menu. 

Pour ajouter la connexion à partir des options du menu, vous devez utiliser `Éditeur d'action` (dans la section 5 de l'interface). Vous n'avez qu'à faire un clic de droit sur l'option et utiliser `Aller aux slots`.

![action](_media/action_menu.png)





