# QT Widget <!-- omit in toc -->

Les `widgets` sont les composants avec lesquels l'interface usager est contruite. Nous avons déjà vu `QPushButton`.

Voici quelques notions de base à propos des widgets:

- tout widget peut avoir un widget parent et des widgets enfants;
- un widget qui n'a pas de parent deviendra une fenêtre lors de l'appel de `show()`;
- un widget enfant est affiché à l'intérieur de son parent. 

Les widgets sont organisés dans une hierarchie d'héritage :

![QObject](_media/QObject-Class-Hierarchy.jpg)

Au sommet de la hierarchie on retrouve **QObject**. Cette classe contient, entre autre, la mécanique pour les signaux et les *slots* que nous verrons plus tard. 

Encore ici, nous n'étudierons pas en détails le fonctionnement de toutes ces classes. Nous nous limiterons au strict nécessaire; mais si vous avez des questions, n'hésitez pas à les poser et nous pourrons explorer un peu plus en détails. 

Utilisons un deuxième exemple afin de voir quelques autres fonctionnalités rapidement. 

Remplacé `main.cpp` par ce code:

```c
#include <QApplication>
#include <QPushButton>

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 QWidget fenetre;
 fenetre.setFixedSize(100, 50);

 QPushButton *bouton = new QPushButton("Hello World", &fenetre);
 bouton->setGeometry(10, 10, 80, 30);

 fenetre.show();
 return app.exec();
}
```

Ici nous commençons par créer un `QWidget` qui servira de fenêtre principale (un widget qui n'a pas de parent est une fenêtre). Ensuite nous créons un `QPushButton` en indiquant que son parent est la fenêtre `&fenetre`. 

La taille de la fenêtre est fixée avec [setFixedSize](https://doc.qt.io/qt-6/qwidget.html#setFixedSize), ainsi que la [géométrie](https://doc.qt.io/qt-6/qwidget.html#setGeometry-1) du button.

