# QT Layout demo partie 3: ajout des fichiers <!-- omit in toc -->

Dans cette leçon nous allons ajouter les fichiers permettant de charger un menu existant et de sauvegarder un nouveau menu. Nous allons réutiliser en partie le code que nous avions concu pour la gestion en mode console. 

## ajout des options dans le menu 

Commençons par ajouter une option dans le menu de l'application afin de charger un menu existant. 

La classe FenetrePrincipale étant une sous-classe de QMainWindow, elle a déjà une barre de menu dans laquelle il suffit d'insérer les menus que l'on veut. 

Ajoutez ce code au début du constructeur de FenetrePrincipale :

```c
QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier")); // D1
fichierMenu->addAction("Quitter", this, &FenetrePrincipale::slotQuitter);

QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));  // D2
menuMenu->addAction("Charger", this, &FenetrePrincipale::slotChargerMenu);    
```

!> Notez que vous devez ajouter `#include <QMenuBar>`

Nous allons régler le cas de la slot pour quitter. Ajoutez cette slot:

```c
/**
 * @brief sortie de l'application
 */
void FenetrePrincipale::slotQuitter() { // D3
    QApplication::quit();
}
```


Et ajoutez la fonction pour slotChargerMenu() afin que le programme compile. 

```c
void FenetrePrincipale::slotChargerMenu() { // D4
    
}
```


!> Notez qu'en écrivant ce code, je me suis apercu que je n'avais pas préfixé le nom des fonctions de slot avec le mot slot. Nous avons ici un bon exercice de "refactoring". Vous pouvez renommer les fonctions en utilisant le bouton `refactor` de l'IDE. 


## lecture du fichier

Nous avons déjà un fichier contenant un menu. Nous l'avions fait dans l'exercice de la section des fichiers. 

Vous pouvez donc vous servir de l'interface console pour créer le fichier, et le copier dans le répertoire pour ce projet. 

!> Rappelez-vous que les fichiers sont conservés dans le répertoire indiqué dans la section `Working directory` dans `Projets/Run`. Vous pouvez changer cette option pour que les fichiers soient entreposés directement dans le répertoire du code de votre projet. 

Rappelons-nous la structure de ce fichier car nous allons réutiliser la même 

![structure_resto](_media/qt_structure_resto.png)


Étant donné que nous allons avoir quelques opérations à faire avec cette structure, il serait bon d'avoir une classe en faisant la gestion. 

Nous allons donc créer une classe . Dans le menu `Fichier`, choisissez `New File`, et prenez  `C/C++` et  `C++ class`. Nommez cette classe `GestionnaireRepas' (notez la majuscule au début)

Cette classe aura comme responsabilité la lecture et l'écriture des données d'un repas. Nous allons donc ajouter un attribut contenant le nom du fichier à traiter. 

Dans `gestionnairerepas.h`, ajoutez une `variable privée` 

```c
private:
    char nomFichier[50];  // D5
```

Dans gestionnairerepas.cpp, ajoutez son initialisation dans le constructeur
```c
GestionnaireRepas::GestionnaireRepas(char *aNomFichier)  // D6
{
    strcpy( nomFichier, aNomFichier); // D8
}
```

Bien entendu, étant donné que la signature du constructeur a changé, il faut la mettre à jour dans le .h

```c
GestionnaireRepas(char *aNomFichier); // D7
```

Nous allons maintenant ajouter la fonction pour lire le fichier. Mais nous avons besoin des structures pour un repas et les items d'un repas. Nous les avons déjà défini lors de l'exercice sur les fichiers. Nous allons donc ré-utiliser le fichier repas.h. Vous devriez l'avoir dans le projet console, mais je vous le redonne ici. Vous devez donc créer ce fichier :

<details>
<summary>repas.h</summary>

```c
#ifndef REPAS_H
#define REPAS_H
#define LONGUEUR_TITRE 20

// un item dans un repas
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;

// un repas contenant une série d'item
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;

// un repas pour la sauvegarde sur disque car item *listeItem ne doit pas être sauvegardé
typedef struct repasFichier {
    char titre[LONGUEUR_TITRE];
    int nombreItem;
} repasFichier;

//un menu contenant une série de repas
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;

#endif // REPAS_H
```
</details>

et l'inclure dans `gestionnairerepas.h`

Nous sommes maintenant prêt à lire le fichier. Ca aussi, nous l'avons déjà fait dans l'exercice sur les fichiers. 
Nous allons donc simplement copier la fonction chargerMenu dans notre projet. 

<details>
<summary>chargerMenu</summary>

```c
/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void chargerMenu(menu *aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}
```
</details>

Ce code n'est pas tout à fait fonctionnel. 

La première des choses à changer, c'est la signature de la fonction afin de mettre la fonction dans la classe. De plus, le nom du fichier n'est plus passé à la fonction, il est initialisé dans le constructeur. 

On doit donc changer l'entête de la fonction et renommer `aNomFichier` à `nomFichier` :

```c
void GestionnaireRepas::chargerMenu(menu *aMenu) {  // D9
    FILE *lFichier = fopen(nomFichier, "r");
```

On peut maintenant ajouter la déclaration de la fonction dans le .h. dans la section `public` car nous allons en avoir besoin dans `FenetrePrincipale`

La première erreur que vous remarquerez est que `FILE` n'est pas définit. Il suffit d'ajouter 

```c 
#include <stdio>
```

Vous remarquerez aussi que les `calloc` sont tous en erreur. Il y a deux raisons. 

La première étant que calloc n'est pas définit par défaut en C++. Il faut donc :

```c
#include <cstdlib>
```
La deuxième est que nous venons de passer de C à C++. Ce dernier est un peu plus sévère sur le typage. Il faut donc indiquer quel type de pointeur sera retourné par le calloc. 

Ajoutez donc le type nécessaire devant le calloc. Ce type est simplement le type de la variable qui est à la gauche du = devant le calloc. 

Voila, nous avons donc une fonction pour lire un menu (une liste de repas contenant une liste d'item)

## lecture du menu

Maintenant que nous avons une fonction pour lire le fichier (en espérant qu'elle fonctionne), nous  allons l'utiliser dans notre fenêtre principale afin de populer le menu à l'écran. Nous allons donc compléter `slotChargerMenu` que nous avions créée précédemment. 

Cette fonction va créer une GestionnaireRepas et appeler la fonction chargerMenu. 

Ajoutez ce code dans chargerMenu

```c
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt"); //D10
    menu lMenu;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    gestionRepas->chargerMenu(&lMenu); // D11
```

!> notez que vous devez changer le nom du fichier. 

Nous allons commencer par vérifier que ca fonctionne. 

Mettez un arrêt sur D11.  

Démarrez le programme en mode debug. 

Lors de l'arrêt, faites avancer le programme d'un pas, et allez vérifier si `lMenu` c'est chargé?

S'il est vide, veuillez  vérifier que vous n'avez pas une de ces erreurs:
- le fichier est vide 
- le fichier n'est pas au bon endroit (vérifier Projets/Run/Workin directory)

Si après vérification, le fichier ne se lit toujours pas, veuillez lever la main pour que j'aille vérifier ce qui n'a pas fonctionné. 

## transfert de lMenu vers l'affichage. 

Nous avons maintenant le fichier chargé en mémoire. Mais il faut l'afficher dans le `QTableWidget` qui est à la droite de l'application. 

Nous avons déjà manipulé l'affichage dans `slotAjouterRepas` et `slotAjouterItem`. Il suffit de faire la même chose, mais à partir de lMenu. 

Nous allons donc ajouter cette fonction dans FenetrePrincipale. 

>Pourquoi dans FenetrePrincipale et non pas dans GestionnaireRepas? \
>Réponse: GestionnaireRepas s'occupe du fichier. Ici, nous manipulons l'écran. 

```c
void FenetrePrincipale::convertirMenuEnQTableWidget(menu *aMenu)
{
    for(int i = 0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        affichageMenu->insertRow(affichageMenu->rowCount());
        affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(QString::fromLatin1(lRepas->titre)));
        for(int j=0; j<lRepas->nombreItem; j++) {
            item lItem = lRepas->listeItem[j];
            affichageMenu->insertRow(affichageMenu->rowCount());
            affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(QString::fromLatin1(lItem.titre)));
            affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(QString::number(lItem.prix)));
        }
    }
}
```

et appeler cette fonction juste après D11

```c
    convertirMenuEnQTableWidget(&lMenu); // D12
```

Si tout est bien fait, vous pouvez utiliser l'option charger de Menu et votre menu apparaitra à l'écran. 

Comme à l'habitude, voici le code au complet

<details>
<summary>code complet</summary>

<details>
<summary>fenetreprincipale.h</summary>

```c
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>
#include "repas.h"

class QPushButton;
class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void slotAjouterRepas();
    void slotAjouterItem();
    void slotItemModifie(QString valeur);
    void slotQuitter();
    void slotChargerMenu();
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();
    void convertirMenuEnQTableWidget(menu *aMenu);

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
    QLineEdit *nomItem;
    QLineEdit *prixItem;

    QPushButton *boutonAjoutRepas; // C1
    QPushButton *boutonAjoutItem;
};
#endif // FENETREPRINCIPALE_H

```

</details>
<details>
<summary>fenetreprincipale.cpp</summary>

```c
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"
#include <QMenu>
#include <QMenuBar>
#include <QApplication>

#include "gestionnairerepas.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier")); // D1
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::slotQuitter);

    QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));  // D2
    menuMenu->addAction("Charger", this, &FenetrePrincipale::slotChargerMenu);
    QWidget *fenetre = new QWidget(); // 2a
    QHBoxLayout *grille = new QHBoxLayout; // 3

    grille->addWidget(constructionBoiteEntrees()); // 4 // 9
    grille->addWidget(constructionSectionAffichage()); // 17

    fenetre->setLayout(grille); // 3a
    setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18 // C2
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterRepas); // 19

    //création de l'entrée pour le nom et le prix de l'item
    nomItem = new QLineEdit; // B1
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00"); //B2
    connect(nomItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie); // C7
    connect(prixItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie);

    QFormLayout *champsItemLayout = new QFormLayout; // B3
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4 // C2a
    boutonAjoutItem->setEnabled(false); // C3 // disable par defaut, tant qu'un prix n'est pas entré.

    connect(boutonAjoutItem, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterItem);


    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    grille->addLayout(champsItemLayout); // B5
    grille->addWidget(boutonAjoutItem);
    boite->setLayout(grille);

    return boite; // 8
}


/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}


void FenetrePrincipale::convertirMenuEnQTableWidget(menu *aMenu)
{
    for(int i = 0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        affichageMenu->insertRow(affichageMenu->rowCount());
        affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(QString::fromLatin1(lRepas->titre)));
        for(int j=0; j<lRepas->nombreItem; j++) {
            item lItem = lRepas->listeItem[j];
            affichageMenu->insertRow(affichageMenu->rowCount());
            affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(QString::fromLatin1(lItem.titre)));
            affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(QString::number(lItem.prix)));
        }
    }
}

// slots
void FenetrePrincipale::slotAjouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
  //boutonAjoutItem->setEnabled(true); // C4
  boutonAjoutRepas->setEnabled(false); // C5
}

void FenetrePrincipale::slotAjouterItem() { //B6
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
    boutonAjoutRepas->setEnabled(true); // C6
    boutonAjoutItem->setEnabled(false); // C9
}

/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::slotItemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." ) { // C8
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}


/**
 * @brief sortie de l'application
 */
void FenetrePrincipale::slotQuitter() { // D3
    QApplication::quit();
}

void FenetrePrincipale::slotChargerMenu() {  // D4
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt"); //D10
    menu lMenu;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    gestionRepas->chargerMenu(&lMenu); // D11
    convertirMenuEnQTableWidget(&lMenu); // D12
}

```


</details>
<details>
<summary>gestionnairerepas.h</summary>

```c
#ifndef GESTIONNAIREREPAS_H
#define GESTIONNAIREREPAS_H

#include "repas.h"

class GestionnaireRepas
{
public:
    GestionnaireRepas(char *aNomFichier); // D7
    void chargerMenu(menu *aMenu);

private:
    char nomFichier[50];  // D5
};

#endif // GESTIONNAIREREPAS_H

```

</details>
<details>
<summary>gestionnairerepas.cpp</summary>

```c
#include "gestionnairerepas.h"
#include <string.h>
#include <stdio.h>
#include <cstdlib>

GestionnaireRepas::GestionnaireRepas(char *aNomFichier)  // D6
{
    strcpy( nomFichier, aNomFichier); // D8
}


/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void GestionnaireRepas::chargerMenu(menu *aMenu) {  // D9
    FILE *lFichier = fopen(nomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = (repas *)calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = (item *)calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = (repas **)realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}

```

</details>
<details>
<summary>repas.h</summary>

```c
#ifndef REPAS_H
#define REPAS_H
#define LONGUEUR_TITRE 20

// un item dans un repas
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;

// un repas contenant une série d'item
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;

// un repas pour la sauvegarde sur disque car item *listeItem ne doit pas être sauvegardé
typedef struct repasFichier {
    char titre[LONGUEUR_TITRE];
    int nombreItem;
} repasFichier;

//un menu contenant une série de repas
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;

#endif // REPAS_H

```


</details>

</details>