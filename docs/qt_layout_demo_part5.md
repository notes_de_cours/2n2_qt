# QT Layout demo partie 5: Testing <!-- omit in toc -->

Ce code est loin d'être parfait, profitons en pour faire un exercice de testing. 

Vous avez quelques minutes pour trouver les bugs. 

Voici ceux que j'ai trouvé, ne les regardez qu'après avoir testé.

<details>
<summary>bug</summary>

- créer un repas, et charger un menu existant. Le repas créé n'a pas d'item
- créer un repas, et charger un menu existant. Le bouton ajouter repas reste éteint malgré le fait qu'il serait possible d'ajouter un repas étant donné que la dernière ligne est un item. 
- charger un menu, ajouter un repas, vider le menu. Le bouton ajouter le repas reste éteint 
- il est possible d'ajouter un item sans avoir de repas.
- bien entendu, si on ajoute un item sans repas, la sauvegarde plante. 
- il est possible d'effacer un repas contenant des items
</details>

Ce genre de testing porte plusieurs noms:
* black box testing
* tests usagers

C'est le type de tests le plus facile à organiser, mais le plus difficile à répéter. 


