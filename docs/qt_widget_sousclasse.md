# QT Widget sous-classe <!-- omit in toc -->

Comme nous avons déjà vu en C, il est fortement recommandé de séparer notre programme en plusieurs fichiers. Nous allons donc séparer le programme simple que nous venons de réaliser. 

Nous allons mettre la `fenetre` dans son propre fichier. Étant donné que nous programmons en C++ maintenant, nous allons créer une classe pour représenter la fenêtre. 

Pour créer une classe, il suffit de créer un nouveau fichier de type `C++ class`. Le nom de la Classe sera `Fenetre` (notez la majuscule), et la classe de base sera `QWidget`. Conservez l'option `include QWidget` ainsi que `Add Q_OBJECT`. 

Vous devriez obtenir les fichiers `fenetre.h` et `fenetre.cpp`. 

Si vous regardez dans fenetre.h, vous y retrouverez une section `signals:` (et aussi `public slots:` selon la version de QT utilisée). Nous reviendrons sur ces sections dans le chapitre suivant. 

Nous allons maintenant modifier `main.cpp` afin d'utiliser cette nouvelle classe. 

```c 
#include <QApplication>
#include "fenetre.h"

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 Fenetre fenetre;
 fenetre.show();

 return app.exec();
}
```

Ainsi que `fenetre.h` afin d'ajouter QPushButton

```c
#ifndef FENETRE_H
#define FENETRE_H

#include <QWidget>

class QPushButton;

class Fenetre : public QWidget
{
    Q_OBJECT
public:
    explicit Fenetre(QWidget *parent = nullptr);
private:
    QPushButton *m_bouton;

signals:

};

#endif // FENETRE_H
```

Ici nous définissons une variable `private`, c'est à dire qu'elle ne pourra être accédée que par une instance de cette classe. C'est le modificateur d'accès par défaut car il est une bonne pratique de garder les variables accessibles que par la classe elle même. 

!> Jusqu'à maintenant, nous avons préfixé les variables avec `l` pour locale, `a` pour argument. Remarquez ici l'utilisation de `m_` pour `member`. C'est une terminologie introduite par le c++ qui a été souvent reprise dans d'autres langages.

Notez que `QPushButton` n'est pas `inclue` dans le fichier .h. Il est simplement défini d'avance (`forward declaration`). Il faut simplement indiquer au compilateur qu'il y a qqchose qui est une class et qui s'appel QPushButton.  Il devra être inclue lorsqu'il sera vraiment utilisé dans le `.cpp`. Notez que vous auriez pu l'inclure aussi, mais ca ralenti la compilation car le fichier devra alors être inclue au complet dans le .h alors que ce n'est pas nécessaire. 

Et finalement, modifions `fenetre.cpp`

```c
#include "fenetre.h"
#include <QPushButton>

Fenetre::Fenetre(QWidget *parent)
    : QWidget{parent}
{
    setFixedSize(100, 50);
    m_bouton = new QPushButton("Hello World", this);
    m_bouton->setGeometry(10, 10, 80, 30);

}
```

!> Notez l'utilisation de `this` lors de la création du QPushButton.  
`this` représente l'objet courant (ici une instance de Fenetre). Ici, nous indiquons que m_bouton sera un enfant (child) de `this` qui est une fenetre. Il sera donc affiché à l'intérieur d'une fenetre. 

Compilez le code, et vous devriez optenir le même résultat. 



