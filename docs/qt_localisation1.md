# Internationalisation d'un projet fait avec designer<!-- omit in toc -->

Si vous désirez que votre jeu de tictactoe soit pris au sérieux à l'international, il faudra que l'interface soit dans plusieurs langages. 

Qt permet, assez facilement, de créer une interface multilingue. On utilise aussi le terme `localisation`. 

## Premier exemple 

Créez un nouveau projet. 

Éditez mainwindow.ui afin d'ajouter le menu `Langue`, un pushbutton et un label. 

![demo_traduction](_media/demo_traduction.png)


Dans le menu Langues ajoutez les actions `Francais` et `English`.

![demo_traduction_action](_media/demo_traduction_actions.png)


Créez un répertoire `langue` dans le répertoire de votre projet. 

Ajoutez dans .pro
```c
TRANSLATIONS += langue/traduction_fr_ca.ts \
                langue/traduction_en_us.ts
```

Le format du nom de fichier pour la traduction doit se terminer par `_langue_pays.ts`

La partie langue est définie par [iso 639](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)

La partie pays est définie par [iso 3166](https://fr.wikipedia.org/wiki/ISO_3166)

!> N'oubliez pas de sauvegarder le fichier .pro

Ouvrez un cmd dans le répertoire de votre projet et inscrivez la commande suivante:

```
lupdate nom_de_votre_projet.pro
```

En inscrivant le nom de votre projet. 


!> Si la commande `lupdate` ne fonctionne pas, vous devez ajouter le path de bin de votre QT.   Ex: C:\Qt\6.4.2\mingw_64\bin\

Vérifier que deux fichiers ont bien été créés dans le répertoire `langue`:
```
traduction_en_us.ts
traduction_fr_ca.ts
```

Entrez les commandes suivantes:

```
cd langue
linguist traduction_fr_ca.ts traduction_en_us.ts
```

Faites toutes les traductions et sauvegardez. 

Pour en savoir plus sur linguist: https://doc.qt.io/qt-6/linguist-translators.html

Retournez dans le cmd: 

```
cd ..
lrelease qt_demo_traduction.pro
```

Cette dernière commande crée des fichiers `.qm` dans le répertoire langue. Il faut ajouter ces fichiers dans la liste des ressources de QT. 


Retournez dans Qt Creator. 

Ajoutez un nouveau fichier de ressources au projet. 

Nouveau fichier/Qt/Qt Resource File/`ressources.qrc`

Ajouter un préfixe : `traductions`

Ajouter les deux fichiers `.qm` du répertoire `langue`

La section Ressources de votre projet devrait ressembler à :

![ressources_projet](_media/demo_traduction_ressources.png)

Dans MainWindow::Mainwindow(), ajouter ce code après `ui->setupUi(this)`

```c
QTranslator* translator = new QTranslator(this);
qApp->removeTranslator(translator);
if(translator->load(":/traductions/langue/traduction_en_us.qm")) {
    qApp->installTranslator(translator);
    ui->retranslateUi(this);
} else {
    qDebug() << "Fichier de langue non trouvé";
}
```

Démarrez l'application, elle devrait utiliser les termes anglais. 


## Connexion au menu de langue

Éditez mainwindow.ui

En bas, dans `Éditeur d'action`, clic de droit sur `actionFrancais` et `Aller au slot...`. 

Choisissez le signal `triggered()`

Recopiez le code qui vient d'être ajouté dans Mainwindow, mais cette fois-ci utilisez `_fr_ca.qm`. 

Faites la même chose pour actionAnglais, et cette fois-ci copiez intégralement le code ajouté dans Mainwindow().

Essayez le programme. Vous devriez pouvoir changer de langage grâce au menu. 

## Mise en fonction

Bien entendu, on vient de dupliquer du code. Ce qui n'est pas bien. 

Nous allons donc mettre tout ça dans une fonction. 

Ajouter dans mainwindow.h

```c
QString m_langPath; // le path pour les fichiers de langue
QString m_langCourrant; // le nom du langage présentement chargé
```

et dans MainWindow.cpp


```c
void MainWindow::changerLangue(QString aLangueEtPays) {
    QTranslator* lTraducteur = new QTranslator(this);
    qApp->removeTranslator(lTraducteur);
    QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
    if(lTraducteur->load(lLangue)) {
        qApp->installTranslator(lTraducteur);
        ui->retranslateUi(this);
        m_langCourrant = aLangueEtPays;
    } else {
        qDebug() << "Fichier de langue non trouvé";
    }
}
```

et changer les fonctions `on_actionFrancais_triggered` et `on_actionAnglais_triggered` pour un appel à cette fonction avec le bon nom de fichier. Pour le français, il faut appeler la fonction avec `fr_ca`, et pour l'anglais `en_us`. 

Et changez le code ajouté précédemment dans `MainWindow::MainWindow` pour :

```c
m_langPath = ":/traductions/langue";
changerLangue(QString("fr_ca"));
```


## Menu des langues dynamique

Si on ajoute une autre langue, il faudra changer le code. Le nombre de langues et leur gestion est donc `hard codé` dans notre programme, c.-à-d. un changement des données externes du programme requiert un changement du code. 

Nous allons donc changer le programme afin que le menu soit basé sur les fichiers de traduction disponibles. 

Première des choses, il faut effacer les actions du menu `Langue`. Effacez donc les fonctions `on_actionFrancais_triggered` et `on_actionEnglish_triggered` du .h et du .cpp. 

Effacez aussi les options Francais et Anglais dans le menu Langues dans mainwindow.ui.


Ensuite, nous allons créer une fonction permettant de créer ce menu à partir des fichiers de traductions présents dans les ressources. 


Ajoutez dans MainWindow.cpp

```c
void MainWindow::creerMenuLangue() {

  QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangue);
  lGroupeLangue->setExclusive(true);

  connect(lGroupeLangue, &QActionGroup::triggered, this,
          &MainWindow::slot_menu_action_triggered);

  // Va chercher le langage par défaut sur cet ordi
  QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
  localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

  // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

  QDir dir(m_langPath);
  QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

  for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
    // extraction du langage à partir du nom de fichier
    QString localeLangue;
    QString localeLangueEtPays;
    localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
    localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
    localeLangueEtPays = localeLangue;
    localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
    localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
    localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

    QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
    nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

    QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
    QAction *action = new QAction(ico, nomDeLangue, this);

    action->setCheckable(true);
    action->setData(localeLangueEtPays);

    ui->menuLangue->addAction(action);
    lGroupeLangue->addAction(action);

    // Sélectionne la langue du système par défaut dans le menu
    if (localeSysteme == localeLangue) {
      action->setChecked(true);
    }
  }
}
```

Et la slot qui réagira quand un langage est sélectionné dans le menu:

```c
void MainWindow::slot_menu_action_triggered(QAction *action)
{
    if (0 != action) {
      // Charge le langage selon le data de l'action
      changerLangue(action->data().toString());
    }
}
```
!> Notez que si vous utilisez la nomenclature de QT et appelez la slot `on_menu_action_triggered()`, vous obtiendrez un avertissement dans la console de l'application. 

Et ajoutez l'appel à `creerMenuLangue()`à la fin du constructeur de MainWindow. 

Réessayez le programme. 

<details>
<summary>Solution complète</summary>

!> Bien entendu, l'interface et les fichiers de traductions ne sont pas fournis

```c
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QActionGroup>
#include <QDir>
#include <QTranslator>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_langPath = ":/traductions/langue";
    changerLangue(QString("fr_ca"));
    creerMenuLangue();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::changerLangue(QString aLangueEtPays) {
    if (m_langCourrant != aLangueEtPays) {
        QTranslator* lTraducteur = new QTranslator(this);
        qApp->removeTranslator(lTraducteur);
        QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
        if(lTraducteur->load(lLangue)) {
            qApp->installTranslator(lTraducteur);
            ui->retranslateUi(this);
            m_langCourrant = aLangueEtPays;
        } else {
            qDebug() << "Fichier de langue non trouvé";
        }
    }
}

void MainWindow::creerMenuLangue() {

  QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangue);
  lGroupeLangue->setExclusive(true);

  connect(lGroupeLangue, &QActionGroup::triggered, this,
          &MainWindow::slot_menu_action_triggered);

  // Va chercher le langage par défaut sur cet ordi
  QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
  localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

  // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

  QDir dir(m_langPath);
  QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

  for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
    // extraction du langage à partir du nom de fichier
    QString localeLangue;
    QString localeLangueEtPays;
    localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
    localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
    localeLangueEtPays = localeLangue;
    localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
    localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
    localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

    QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
    nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

    QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
    QAction *action = new QAction(ico, nomDeLangue, this);

    action->setCheckable(true);
    action->setData(localeLangueEtPays);

    ui->menuLangue->addAction(action);
    lGroupeLangue->addAction(action);

    // Sélectionne la langue du système par défaut dans le menu
    if (localeSysteme == localeLangue) {
      action->setChecked(true);
    }
  }
}
// slots

void MainWindow::slot_menu_action_triggered(QAction *action)
{
    if (0 != action) {
      // Charge le langage selon le data de l'action
      changerLangue(action->data().toString());
    }
}

```

```c
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void slot_menu_action_triggered(QAction *action);
private:
    Ui::MainWindow *ui;
    QString m_langPath ; // le path pour les fichiers de langue
    QString m_langCourrant; // le nom du langage présentement chargé
    void changerLangue(QString nomLangue);
    void creerMenuLangue();
};
#endif // MAINWINDOW_H

```

</details>