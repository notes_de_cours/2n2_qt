# TP3 2023 : jeu de reversi/othello<!-- omit in toc -->

Pour ce TP, vous devez coder un jeu de reversi. 

Vous pouvez explorer les règles de ce jeu [ici](https://www.ffothello.org/othello/regles-du-jeu/)

Vous pouvez fortement vous baser sur le jeu de tictactoe développé en cours. 

# Fonctionnement

Vous devez afficher un tableau de reversi et permettre aux joueurs de jouer chacun leur tour. 

Vous pouvez utiliser la technique prise pour le tictactoe, ou toute autre technique. (Notez que si vous utilisez des techniques non démontrées en cours, vous devrez expliquer clairement ce que ca fait)

Vous devez permettre à un joueur de passer un tour. 

Si les 2 joueurs passent leur tour, il doit alors leur être possible de terminer la partie. 

Si toutes les cases sont remplies, la partie se termine automatiquement et le gagnant est déterminé. 

Lors de la fin d'une partie, un décompte des pièces est fait afin de déterminer le gagnant. 

Si un joueur dépose une pièce à un mauvais endroit, il doit en être averti par un son, et il ne perd pas son tour. 


# Exemple de tableau de jeu 

![reversi](_media/reversi_ecran_principal.png)

Cet exemple n'a pas besoin d'être reproduit. Vous êtes libre de réorganiser l'écran, et même d'ajouter des fonctionnalités.


# Structure du code et format de remise

Vous devez mettre votre projet sur un dépôt git de votre choix (gitlab, github, tigit... ) et me remettre sur Léa un fichier texte indiquant l'adresse de ce dépôt. 

Comme à l'habitude, vous devez séparer votre projet en .cpp et .h. 

# Correction 

Ce TP vaut (~~25~~) 40 points sur la note finale 

* Non-respect des lois de nommage : jusqu'à -2 points
* Français dans la documentation : jusqu'à -3 points
* Code fonctionnel répondant aux critères de fonctionnement et aux règles du jeu : 10 points
* Qualité du code : 8 points 
* Qualité de la documentation : 5 points
* Utilisation de git : 2 points

!> Notez que le pointage est sur 25. La conversion à 40 sera faite dans Léa. 

# Éléments évalués

### Fonctionnement 

Les éléments suivants seront évalués pour le respect des critères de fonctionnement:

* Une pièce doit être déposée adjacente à une pièce déjà sur le jeu. 
  * ex: au départ, le premier joueur ne pourrait déposer sa pièce en C2.
  * si la pièce n'est pas à un bon endroit, vous devez émettre une alarme sonore. 
  * le joueur ne perd pas son tour. Il peut faire plusieurs essais. 
* Un joueur peut passer son tour
  * lors de la pose d'une pièce, il doit obligatoirement faire une capture. Si aucune capture n'est possible, il peut passer son tour.
* Il doit être possible de terminer la partie si aucun joueur ne peut jouer, ou si toutes les cases sont occupées. 
  * le programme compte alors les pièces et le vainqueur est affiché dans un popup. 
  * quand le popup est fermé, le jeu est remis au point de départ et une nouvelle partie peut commencer. 
* Il doit être possible de quitter le jeu. 
  * un popup de confirmation est affiché.
    * si l'usager confirme, le jeu se ferme
    * sinon, le jeu continue là ou il en était. 
* Le pointage actuel doit être affiché après chaque coup. 
* et finalement: lors de la pose d'une pièce, il est obligatoire de faire au moins une capture. 
  * le programme doit vérifier qu'une capture sera faite, sinon il doit émettre une alarme sonore
  * si des captures sont possibles, le programme doit les faire

!> L'algorithme pour faire la capture vous sera expliqué en cours. 


### Qualité du code 

Les éléments suivants seront évalués pour la qualité du code:

* indentation;
* ménage dans les includes (si un include ne sert a rien, vous perdez des points);
* constance dans le style de code (espacement, {}, tabulation);
* ménage du code inutile (code inutile restant en commentaire, fonction inutile);
* complexité du code (si c'est difficile à comprendre, ça devrait être commenté; ou mieux, refactorisé);

### Documentation

Les éléments suivants seront évalués pour la qualité de la documentation:

* Fonctions documentées;
* Attributs de fonctions documentées;
* Respect des normes expliquées en cours;
* Texte clair et expliquant bien ce que ça fait;
* Documentation Doxygen générée et à jour. 
* README écrit en markdown incluant le contenu tel qu'expliqué dans le cours;

### Git

Les éléments suivants seront évalués pour l'utilisation de git:

* messages significatifs dans les commit;
* au moins 3 commit espacés dans le temps et avec un contenu significatif. 



# Date de remise

Ce TP doit être remis à minuit, le jour du cours de la semaine 12. 
Si vous poussez sur votre dépôt après cette date, ça sera considéré comme un retard. 

