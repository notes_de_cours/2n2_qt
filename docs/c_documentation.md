# Documentation d'un programme en C/C++ et du dépôt GIT <!-- omit in toc -->

Il est important de bien documenter son code. Même si vous le comprenez au moment de l'écriture, ça ne veut pas dire que, dans 6 mois ou plus, vous vous en souviendrez encore. De plus, vous ne serez pas la seule personne à lire ce code. 




## Documentation du code

La documentation du code consiste à décrire la structure et le fonctionnement tel que perçu de l'extérieur du code. Le terme *black box* est parfois utilisé, car cette documentation permet de comprendre ce que fait le code sans avoir à le lire. 

Dans le cadre de ce cours, étant donné que l'approche orientée objet n'est pas réellement utilisée, nous nous contenterons de décrire les fonctions/méthodes ainsi que leurs arguments/paramètres. (nous ne documenterons pas les quelques classes que nous rencontrerons)

Des exemples de documentation de fonctions sont facilement trouvés sur des sites décrivant le langage c. 

Par exemple : https://devdocs.io/c/io/fprintf

Sans savoir `comment` est codé le printf, on peut découvrir ce qu'il peut faire et quels sont ses arguments. 

Cette documentation doit être tenue à jour lors de la modification d'une fonction. Cette documentation est souvent la seule description qu'un autre programmeur aura de nos fonctions étant donné que seul le .h sera habituellement distribué , accompagné du code en format compilé. Il est donc difficile à un utilisateur de notre code de savoir ce que fait une fonction si la documentation est manquante, difficile à comprendre, ou, pire, erronée.

## Commentaire de code

Les commentaires de code sont nécessaires lorsque le code est complexe. Ils permettent d'expliquer ce que fait le code sans qu'on ait à le relire et se casser la tête à le déchiffrer. Ces commentaires demandent d'avoir accès au code source. Ils s'adressent donc à ceux qui doivent modifier ce code. 

Les commentaires de code ne doivent pas être utilisés à outrance afin de compenser pour du code mal écrit. Si le code est tellement difficile à comprendre qu'un commentaire semble obligatoire, peut-être serait-il mieux de clarifier le code (ex.: renommer des variables, restructurer, créer une fonction ayant un nom significatif pour un bout de code complexe...). Une école de pensée veut qu'il ne doive pas y avoir de commentaire dans le code, que celui-ci doive être autodescriptif. Sans aller dans cet extrême, il est tout de même recommandé de relire son code et de le rendre plus facile à lire. 

Par exemple, ce code, bien que fonctionnel, n'est pas très clair:

![codepasclair](_media/code.png)



Le code lui-même est une source de vérité. Il n'y a rien de pire que du code et des commentaires qui se contredisent. Dans ce cas, il faut confirmer soit que le commentaire est erroné, ou pire, que le code est erroné. 

N'hésitez pas à relire vos commentaires après quelque temps. S'ils ne sont pas clairs, n'hésitez pas à les modifier. 


Certains commentaires de code servent à indiquer des bugs connus ou des améliorations à apporter. En effet, il arrive qu'on sache qu'il y a un bug dans un bout de code, mais qu'on ne sache pas comment le régler à court terme; ou qu'on ait écrit du code pour faire fonctionner le programme, mais qu'on sache que ça cause un problème structurel Vous verrez plus tard certaines techniques de programme qui ne deraient pas être utiliser. Mais parfois, il est difficile de trouver une solution rapide et/ou facile à un problème et on se retrouve *obligé* d'utiliser une mauvaise technique.

Dans de telles situations vous pouvez inclure un commentaire `//TODO` ou `//FIXME`. Ce genre de commentaires (ou un dérivé) est souvent reconnu par les IDE et peut être facilement localisé.  

## Exemple


Nous allons reprendre le jeu de tictactoe, et documenter certaines sections. 

cd dans le répertoire du projet de tictactoe.

Afin de générer le fichier de configuration de doxygen, entrez la commande :

```
doxygen -g
```


Et changez le fichier de config afin que toutes les fonctions soient affichées :

``` 
EXTRACT_PRIVATE        = YES
```

!> Normalement, la documentation est faite uniquement pour les fonctions publiques. Mais étant donné que notre code contient majoritairement des fonctions private, nous allons tout documenter. 

Vous pouvez dès maintenant générer la documentation. Bien entendu, elle sera pratiquement vide. 

Entez la commande `doxygen` dans le répertoire du projet. 

Vous pouvez ensuite ouvrir `html\index.html`. 

Je vous fournis le code déjà documenté du projet tictactoe. Vous pourrez vous vous en servir comme référence lors de la remise du TP3 et TP4. 

J'y ai inclus de bonnes et mauvaises pratiques de documentation. 

Voici maintenant le code documenté :

<details>
<summary>mainwindow.h</summary>

```c
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
#define TAILLE 50
#define NBR_CELL 3

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void slotJouer();
    void slotVerifierGagnant();
private:
    int joueur = 1; /**< l'id du joueur actif */
    int tableau[NBR_CELL][NBR_CELL]; /**< les valeurs du tableau du jeu.

                         - Contient **0,1 ou 2**.
                         - 0 indique une case vide.
                         - 1 ou 2 indiquant quel joueur occupe cette case
        */

    const QString ETIQUETTE = "ABCDEFGHI"; /**< étiquette des coordonnées */

    QLineEdit *choixXY;     /**< le champ pour entrer les coordonnées où jouer */
    QGraphicsScene *scene;  /**< la scène où est dessiné le jeu */
    QSoundEffect *buzzer;   /**< le son quand le joueur entre une mauvaise coordonnée */

    //11
    int derniereCol = 0;   /**< la dernière colonne entrée, utilisée pour déterminer le gagnant */
    int derniereLigne = 0; /**< la dernière ligne entrée, utilisée pour déterminer le gagnant */


    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);

    int verifierGagnant(int aCol, int aLigne);
};
#endif // MAINWINDOW_H

```

</details>

<details>
<summary>mainwindow.c</summary>

```c
#include "mainwindow.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

#include <QSoundEffect>

/*!
 * \brief MainWindow::MainWindow
 * Construction du tableau de jeu.
 * \param parent le widget dans lequel insérer cette fenêtre.
 *
 * Le tableau consiste en une grille de jeu, d'un champ de texte
 * pour faire l'entrée de la coordonnée où insérer le O ou le X, et
 * d'un bouton pour jouer le coup.
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);

    // 10
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotVerifierGagnant);

    setCentralWidget(fenetre);

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/game-show-buzz.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);
}

MainWindow::~MainWindow()
{
}

/*!
 * \brief MainWindow::initialiserJeu
 *          mise à 0 de toutes les cellules du jeu
 */
void MainWindow::initialiserJeu()
{
    // 7
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }
    // 12

    dessinerJeu();
}

/*!
 * \brief MainWindow::dessinerJeu
 *  Affiche le système de coordonnées autour des cases
 *  ainsi que les cases du jeu.
 *
 *  Le tableau a NBR_CELL x NBR_CELL, et la taille des
 *  carrés est définie par TAILLE
 *
 *  Les coordonnées des colonnes sont déterminées par ETIQUETTE.
 *  Les coordonnées des lignes sont numériques et partent à 1.
 */
void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);

        // dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(tableau[i][j] != 0)
            {
                // 8
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}

/*!
 * \brief MainWindow::dessinerPiece
 * Affichage du X ou du O
 * \param aCol[in], aLigne[in] les coordonnées où insérer la pièce
 * \param aJoueurId[in] L'id du joueur: 1=O, 2=X (0 est réservé pour les cases vides)
 */
void MainWindow::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    QGraphicsPixmapItem *item;
    if(aJoueurId==1) {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/rond.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/x.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);
};


// Slots

/*!
 * \brief MainWindow::slotJouer
 *
 *  **mauvais** : connectée au bouton jouer
 *  (ce n'est pas ce que ca fait, et ca pourrait être appelé autrement)
 *
 *  *bon* : Place un X ou un O sur le jeu.
 *
 *  **mauvais**: Les coordonnées sont dans le QLineEdit choixXY
 *  (ce n'est pas assurement un QLineEdit, et on ne sait pas comment c'est stocké)
 *
 *  *bon* : La colonne est dans choixXY[0], et la ligne dans choixXY[1]
 *
 *  La colonne doit être dans ETIQUETTE.
 *
 *  La ligne doit être un numérique entre 1 et NBR_CELL inclusivement.
 *
 *  Si les coordonnées sont mauvaises, un buzzer se fera entendre.
 */
void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];

        // conversion en index numérique de la lettre représentant la colonne
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
//8
        // vérification de la validité des coordonnées
        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();

            // 11
            //conserve les coordonnées afin de savoir où s'est fait le
            // dernier coup afin de déterminer s'il y a un gagnant.
            // FIXME: comment séparer slotJouer et slotVerifierGagnant?
            //        ces 2 variables ne devraient pas être nécessaires.
            derniereCol = col;
            derniereLigne = ligne;

            // calcul de l'id du prochain joueur: 1 ou 2
            joueur =  ((joueur)%2)+1;
        } else { // mauvaise coordonnées
            // 5
            buzzer->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}


// 12
/*!
 * \brief MainWindow::slotVerifierGagnant
 *
 * Détermine s'il y a un gagnant suite au dernier coup.
 *
 * Pour gagner, il faut avoir soit une ligne horizontale, verticale,
 * ou une grande diagonale pleine.
 *
 * Si un gagnant est détecté, un popup apparaitra. Le jeu est par la
 * suite effacé, et la partie peut recommencer.
 *
 * Si aucun gagnant, rien ne se passe.
 *
 *  **mauvais** : dernierCol et derniereLigne sont assignées dans slotJouer
 *   ( C'est une considération interne qui ne doit pas être dans la documentation
 *   externe)
 */
void MainWindow::slotVerifierGagnant() {
    int gagnant = 0;

    //l'id du joueur qui a joué le dernier coup
    //TODO: changer couleur pour joueurId (afin de clarifier le code)
    int couleur = tableau[derniereCol][derniereLigne];

    //compteurs du nombre de cases sur une même ligne/colonne/diagonale
    //appartenant au joueur ayant fait le dernier coup
    //Si un de ces compteurs atteint NBR_CELL, nous avons un gagnant
    int compteurLigne = 0;
    int compteurCol = 0;
    int compteurDiag1 =0;
    int compteurDiag2 = 0;

    //l'id du joueur peut être 0 si de mauvaises coordonnées sont entrées au premier coup.
    if(couleur != 0)  {
        for(int i=0; i<NBR_CELL; i++) {
            // verifie la colonne dans laquelle le dernier coup a été fait
            if(tableau[derniereCol][i]==couleur) {
                compteurCol++;
            }
            //verifie la ligne
            if(tableau[i][derniereLigne] == couleur) {
                compteurLigne++;
            }

        }

        // verifie si le dernier coup est sur une diagonale
        // soit que col==ligne, soit que col+ligne = NBR_CELL-1
        // FIXME ce genre de commentaire demande une clarification mathématique
        //   pourquoi col+ligne = NBR_CELL-1 indique que c'est sur une diag ?
        if(derniereLigne==derniereCol || derniereLigne+derniereCol == NBR_CELL-1) {
            for(int i=0; i<NBR_CELL; i++) {
                if(tableau[i][i] == couleur){
                    compteurDiag1++;
                }
                if(tableau[(NBR_CELL-1) -i][i] == couleur) {
                    compteurDiag2++;
                }
            }
        }

        gagnant = (compteurCol == NBR_CELL) || (compteurLigne == NBR_CELL)
                ||(compteurDiag1 == NBR_CELL) || (compteurDiag2 == NBR_CELL);

        if(gagnant) {
            QMessageBox msgBox;
            msgBox.setText("Nous avons un gagnant!!");
            msgBox.exec();

            scene->clear();
            joueur = 1;
            initialiserJeu();
        }
    }
}

```
</details>

!> Notez qu'il est recommandé de mettre la documentation de code avec la `déclaration` des fonctions. Les commentaires placés devant les fonctions devraient donc être dans le .h. Si vous développez une bibliothèque de fonctions, seul le .h sera fourni avec votre code compilé. Si la documentation n'est pas dans le .h, il est alors impossible de savoir ce que font les fonctions. Il est courant de dupliquer la documentation entre le .h et le .cpp.  Mais dans le cadre du cours, je vous demanderai de mettre la documentation dans le .cpp uniquement. 


# LISEZMOI

Tout bon projet de développement devrait être accompagné d'un fichier LISEZMOI (ou README). 

Ce fichier est souvent la première chose qui sera lu par ceux qui désirent utiliser votre projet. Si vous utilisez un dépôt git (github, gitlab, ou autre), le fichier README.md sera affiché sur la page d'accueil du projet. 

Tout le monde a son format pour l'information devant être incluse dans le fichier. Voici celle que je vous demande d'inclure dans le cadre du cours:

- Le nom du projet
- Une description
- Une ou plusieurs captures d'écran du projet
- Les instructions de fonctionnement de l'application
- Les contributeurs (votre nom :smile: )

Le fichier doit être écrit en Markdown et être mis à la racine du projet et s'appeler `README.md`. 


Si vous désirez voir des exemples de *beaux* fichiers README : https://github.com/matiassingers/awesome-readme