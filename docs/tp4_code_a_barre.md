# TP4 2023 : Générateur de code à barres <!-- omit in toc -->

Pour ce TP, vous devez coder un générateur de code à barres. 
 

## L'application

Votre application doit permettre d'entrer un texte et de le convertir en code à barres. 

Vous devez donc avoir une entrée pour le texte et un bouton pour convertir ce texte. 

Voici un exemple de l'interface de cette application. 

![codeabarre](_media/code_a_barre.png)

Cette application doit être générée à l'aide de `QT Designer`. 


## Code39

Ce générateur utilisera l'encodage [code39](https://fr.wikipedia.org/wiki/Code_39). 

Bien que ce code soit un peu démodé, il est toujours compris par plusieurs lecteurs de codes à barres. Il a été choisi car il est simple à générer.

Ce code ne peut représenter que les lettres suivantes: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 -$%./+*"

!> Notez que le caractère * (étoile) ne peut être affiché étant donné qu'il est le caractère de départ de fin. 

Afin de vous simplifier un peu la vie, je vous fournis une déclaration d'un tableau permettant de convertir ces caractères en code 

<details>
<summary >tableau de convertion</summary>

```c
const QString CONVERSION = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 -$%./+";
int code39[44][9] = {
    {1,0,0,0,0,1,0,0,1},   //A 0  
    {0,0,1,0,0,1,0,0,1},   //B 1
    {1,0,1,0,0,1,0,0,0},   //C 2
    {0,0,0,0,1,1,0,0,1},   //D 3
    {1,0,0,0,1,1,0,0,0},   //E 4
    {0,0,1,0,1,1,0,0,0},   //F 5
    {0,0,0,0,0,1,1,0,1},   //G 6
    {1,0,0,0,0,1,1,0,0},   //H 7
    {0,0,1,0,0,1,1,0,0},   //I 8
    {0,0,0,0,1,1,1,0,0},   //J 9
    {1,0,0,0,0,0,0,1,1},   //K 10
    {0,0,1,0,0,0,0,1,1},   //L 11
    {1,0,1,0,0,0,0,1,0},   //M 12
    {0,0,0,0,1,0,0,1,1},   //N 13
    {1,0,0,0,1,0,0,1,0},   //O 14
    {0,0,1,0,1,0,0,1,0},   //P 15
    {0,0,0,0,0,0,1,1,1},   //Q 16
    {1,0,0,0,0,0,1,1,0},   //R 17
    {0,0,1,0,0,0,1,1,0},   //S 18
    {0,0,0,0,1,0,1,1,0},   //T 19
    {1,1,0,0,0,0,0,0,1},   //U 20
    {0,1,1,0,0,0,0,0,1},   //V 21
    {1,1,1,0,0,0,0,0,0},   //W 22
    {0,1,0,0,1,0,0,0,1},   //X 23
    {1,1,0,0,1,0,0,0,0},   //Y 24
    {0,1,1,0,1,0,0,0,0},   //Z 25
    {0,0,0,1,1,0,1,0,0},   //0 26
    {1,0,0,1,0,0,0,0,1},   //1 27
    {0,0,1,1,0,0,0,0,1},   //2 28
    {1,0,1,1,0,0,0,0,0},   //3 29
    {0,0,0,1,1,0,0,0,1},   //4 30
    {1,0,0,1,1,0,0,0,0},   //5 31
    {0,0,1,1,1,0,0,0,0},   //6 32
    {0,0,0,1,0,0,1,0,1},   //7 33
    {1,0,0,1,0,0,1,0,0},   //8 34
    {0,0,1,1,0,0,1,0,0},   //9 35
    {0,1,1,0,0,0,1,0,0},   //espace 36
    {0,1,0,0,0,0,1,0,1},   //- 	37
    {0,1,0,1,0,1,0,0,0},   //$ 38
    {0,0,0,1,0,1,0,1,0},   //% 39
    {1,1,0,0,0,0,1,0,0},   //. 40
    {0,1,0,1,0,0,0,1,0},   // / 41
    {0,1,0,0,0,1,0,1,0},   //+ 42
    {0,1,0,0,1,0,1,0,0}   //* 43
};
```
</details>

Vous pouvez insérer ce code dans `mainwindow.h`.

Dans ce tableau, chaque ligne représente le code pour une lettre. 

Chaque ligne est composé de 9 nombres binaires. Un 0 indique que la ligne doit être mince, un 1 indique que la ligne doit être le double d'une ligne mince. 

Chaque caractère commence et se termine par une ligne noire.

Une ligne mince blanche doit être insérée entre chaque caractère. 

La variable `conversion` peut être utilisée de la même façon que `ETIQUETTE` dans le tictactoe afin de convertir une QString en index pour le tableau. 

Un code39 commence et se termine par une `étoile` (caractère 43). 

## Affichage d'une barre du code à barres. 

Pour ajouter une ligne de largeur variable, le plus simple est de dessiner un rectangle. 

Pour ajouter un rectangle, vous pouvez utiliser la syntaxe suivante:

```c
scene->addRect(p1, p2, p3, p4, p5, p6);
```

- p1 est le x de départ du rectangle
- p2 est le y de départ du rectangle
- p3 est la largeur du rectangle
- p4 est la hauteur du rectangle
- p5 est le crayon utilisé pour la bordure du rectangle (QPen)
- p6 est la brosse utilisée pour remplir le rectangle (QBrush)

Je vous laisse chercher comment créer un QPen et une QBrush de la bonne couleur (soit noir ou blanc).

!> Bien entendu, vous devez nommer p1...p6 avec un nom respectant les normes. 

# Fonctionnement

Vous devez permettre à l'usager d'entrer un texte. 

Vous devez verifier que ce texte comporte uniquement des caractères valides (excluant `*` ). 

L'usager doit pouvoir générer le code à barres. 

Ce code s'affiche à l'écran. Vous pouvez le valider à l'aide d'une application de lecture de code à barres sur votre téléphone (si vous n'en avez pas, vous pouvez me demander de vérifier le code généré). 

L'usager doit avoir une option dans le menu pour effacer le texte entré (ce qui effacera aussi le code généré).

L'usager doit pouvoir effacer le code généré à l'aide d'un bouton dans l'interface (en plus du menu).

L'interface doit être multilingue (aux moins 2). (si vous ne connaissez pas la traduction pour un terme, vous n'avez qu'à metre un préfixe dans la traduction). 

L'interface doit avoir un menu permettant de changer de langue, et ce menu doit être dynamique (comme montré dans le cours).

# Structure du code et format de remise

Vous devez mettre votre projet sur un dépôt git de votre choix (gitlab, github, tigit... ) et me remettre sur Léa un fichier texte indiquant l'adresse de ce dépôt. 

Comme à l'habitude, vous devez séparer votre projet en .cpp et .h. 

# Correction 

Ce TP vaut (~~50~~) 35 points sur la note finale

* Non-respect des lois de nommage : jusqu'à -2 points
* Français dans la documentation : jusqu'à -3 points
* Code fonctionnel répondant aux critères de fonctionnement : 13 points
* Qualité du code : 10 points 
* Utilisation de QT Designer : 2 points
* Qualité de la documentation : 8 points
* Utilisation de git : 2 points

# Éléments évalués

### Fonctionnement 

Les éléments suivants seront évalués pour le respect des critères de fonctionnement:

* Validation des caractères entrés.
* Génération correcte du code à barres. 
* Permettre d'effacer le texte
* Permettre d'effacer le code généré de deux façons (menu et bouton)
* Interface multilingue
* Menu de langues dynamique 
* Le menu Fichier doit avoir l'option pour quitter
* Le menu Edition doit avoir l'option pour vider le texte et le code généré. 
* Et vous aurez besoin d'un menu Langue. 

### Français

Les commentaires et tout ce qui est affiché à l'écran sera évalué. 

Notez que si vous ne mettez aucun commentaire ni de readme, vous perdez les 3 points. 

Vous perdez 0.5 point par faute. 

### Qualité du code 

Les éléments suivants seront évalués pour la qualité du code:

* utilisation de Qt designer;
* indentation;
* ménage dans les includes (si un include ne sert a rien, vous perdez des points);
* constance dans le style de code (espacement, {}, tabulation);
* ménage du code inutile (code inutile restant en commentaire, fonction inutile);
* complexité du code (si c'est difficile à comprendre, ça devrait être commenté; ou mieux, refactorisé);

### Documentation

Les éléments suivants seront évalués pour la qualité de la documentation:

* Fonctions documentées;
* Attributs de fonctions documentées;
* Respect des normes expliquées en cours;
* Texte clair et expliquant bien ce que ça fait;
* Documentation Doxygen générée et à jour. 
* README écrit en markdown incluant le contenu tel qu'expliqué dans le cours;

### Git

Les éléments suivants seront évalués pour l'utilisation de git:

* messages significatifs dans les commit;
* au moins 3 commit espacés dans le temps et avec un contenu significatif. 


# Date de remise

Ce TP doit être remis à minuit, le jour du cours de la semaine 15.   
Si vous poussez sur votre dépôt après cette date, ça sera considéré comme un retard. 


