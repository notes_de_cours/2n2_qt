# Premier contact avec QT <!-- omit in toc -->

Nous allons commencer par créer un nouveau projet, mais cette fois-ci le modèle sera `Application (Qt)` et `Qt Widgets Application`.

Donnez lui le nom `helloworld_qt`.

Notez dans quel répertoire sera créé le projet. 

Choisissez `qmake` dans la section `Build system`

Dans `Details`, **déselectionnez** `Generate form`   (si vous oubliez de le faire, ce n'est pas grave, vous aurez un fichier supplémentaire que nous n'utiliserons pas)

Conservez les autres paramètres par défaut. 

Vous devriez obtenir 2 fichiers dans la section `Sources` et 1 dans `Headers`.  Pour l'instant, nous n'allons utiliser que `Sources/main.cpp`

Remplacez le code de `main.cpp` par:

```c
#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QPushButton bouton ("Hello world !");
    bouton.show();
    return app.exec();
}
```

et exécutez le code....

... rien de très intéressant, mais ca permet de vérifier que tout fonctionne. 

Il y a plusieurs façons de faire la même chose. Changez le code pour:

```c
#include <QApplication>
#include <QPushButton>

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 QPushButton bouton;
 bouton.setText("Allo!!");
 bouton.setToolTip("le tooltip");
 bouton.show();

 return app.exec();
}
```

Il y a plusieurs autres paramètres qui peuvent être changés. Ajoutez ces lignes juste avant button.show()

```c
QFont font ("Courier");
bouton.setFont(font);
```

