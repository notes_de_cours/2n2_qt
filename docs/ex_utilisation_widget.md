# Exercices utilisant des Widgets de QT <!-- omit in toc -->

Ces exercices vous permettrons de vous familiarisez avec les widgets, mais surtout à trouver la documentation. 


## Exercice 1

Créez un nouveau projet. Utilisez les paramètres par défaut lors de la création, à l'exception de l'option `Generate form` sur l'écran `Class Information`. Déselectionnez cette option.

!> Votre code doit aller dans MainWindow (ne créez pas Fenetre)

Mettez la taille de MainWindow à 300x300.

Ajoutez dans cette fenêtre un QLabel avec le mot "allo". Ce QLabel doit avoir la MainWindow comme parent.(1) 

Changez la géométrie du QLabel pour le déplacer au coordonnée 10,10, et que sa taille soit 90x30 (2)

Essayez votre programme. 

Changez le styleSheet du QLabel pour avoir un border de 1px noir solide. (3)

Essayez votre programme

Ajoutez un QPushButton. Placez le aux coordonnées 10,50, avec une taille de 80,30.

Essayez votre programme. 

Ajoutez une connexion entre le QPushButton et la MainWindow. Appelez la slot `slotChangerText'. (4)

Dans cette slot, changez le text du QLabel pour "cliqué".


<details>
<summary>Aide</summary>

1. utilisez `this` comme parent.   
2. setGeometry  
3. setStyleSheet  
4. Vous devez créer une variable dans mainwindow.h afin d'y mettre le QLabel afin de pouvoir y accéder pour changer son texte dans la slot. 

</details>


<details>
<summary>Solution</summary>

```c
#include "mainwindow.h"

#include <QLabel>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    setFixedSize(300, 300);


    m_etiquette = new QLabel("allo",this);
    m_etiquette->setGeometry(10,10,90,30);
    m_etiquette->setStyleSheet("border: 1px solid black");
    QPushButton *bouton1 = new QPushButton("Hello World", this);
    bouton1->setGeometry(10, 50, 80, 30);

    connect(bouton1, &QPushButton::clicked, this, &MainWindow::slotChangerText);
}

MainWindow::~MainWindow()
{
}


void MainWindow::slotChangerText()
{
   m_etiquette->setText("click.");

}
```

```c
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QLabel *m_etiquette;
private slots:
    void slotChangerText();
};
#endif // MAINWINDOW_H

```

</details>

## Exercice 2

À partir de la solution de l'exercice précédent:

Ajoutez un QLineEdit

Ajoutez une connexion sur MainWindow pour les signaux suivants: `editingFinished`, `textEdited`, `textChanged`.

Vous devez donc créer 3 slots dans MainWindow et connecter chacun des signaux émis par le QLineEdit à une de ces slots. Il serait donc utile de nommer vos slots slotEditingFinished, slotTextEdited, slotTextChanged. 

Dans les slots, ajoutez une qDebug() indiquant laquelle a été appelée. 

Maintenant, observez ce qui se passe quand vous modifiez le text dans le QLineEdit, quand vous appuyez sur tab, ou sur return/enter. 

Pouvez-vous expliquer la différence entre textEdited et textChanged ?

<details>
<summary>Réponse</summary>

Allez voir la documentation :smile:

</details>

Faites la même étude avec les slots `returnPressed` et `selectionChanged`

À quoi sert `selectionChanged` ?

<details>
<summary>Réponse</summary>

Allez voir la documentation :smile:

Sélectionnez un bout de texte dans le QLineEdit...

</details>