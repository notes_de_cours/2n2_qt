# reversi/othello : algorithme pour pièces adjacentes<!-- omit in toc -->

Une des règle au reversi est qu'une pièce doit être déposée adjacente à une autre.

Voici un algorithme que vous pourriez utiliser afin de faire cette vérification (il doit y en avoir d'autres)

Supposons qu'une pièce est déposée à la position (x,y), il faut vérifier si une des cases adjacentes est occupée. 

Nous pouvons représenter cette position avec les coordonnées suivantes:

![xy](_media/algo_reversi_1.png)

Il faut donc parcourir les 8 cases entourant (x,y). 

Deux boucles imbriquées seront nécessaires. La première pour parcourir les x, la deuxième pour les y.

```
indicateur = faux
Boucle1 de -1 à 1
  Boucle2 de -1 à 1
    Si !(boucle1 == 0 et que boucle2 == 0) 
    //1
    et que la position (boucle1+x, boucle2+y) n'est pas vide, alors indicateur = vrai
```

La position où boucle1 et boucle2 sont à 0 ne doit pas être vérifiée car c'est là qu'on veut mettre la pièce. Bien entendu, cette position doit être libre car c'est là qu'on veut mettre notre pièce. Cette vérification est à faire avant même de faire la vérification d'adjacence. 

Si l'indicateur est vrai, c'est qu'au moins une des cases adjacentes est occupées. 

Une autre condition à prendre en compte est le fait que boucle1+x ou boucle2+y pourrait donner une coordonnée à l'extérieur du tableau. 

Il y a 2 façons de remédier à ce problème:

1) ajouter une vérification dans les boucles afin de s'assurer qu'on ne déborde pas.   
   Il faudrait donc ajouter une vérification de débordement en //1
2) ajouter une ligne et une colonne vide dans le tableau des positions au nord, sud, est, ouest afin de s'assurer de ne jamais déborder lors de cette recherche.   
    Cette solution demande quelques modifications dans le programme, mais simplifie plusieurs algorithmes. Bien entendu, cela requiert aussi un peu de mémoire pour entreposer ces cases vides. Il faut souvent faire une balance entre une simplicité algorithmique ou une plus grande utilisation de la mémoire. 

Je vous recommande d'utiliser la première solution... mais si vous avez le temps, vous pouvez explorer la deuxième!

Si vous utilisez la solution 1, la condition à ajouter sera:

``` 
Si !((boucle1+x < 0 ou >= NBR_CELL) ou (boucle2+y < 0 ou >= NBR_CELL))
```

ou si vous préférez une logique positive
```
Si ((boucle1+x >=0 et < NBR_CELL) et (boucle2+y >= 0 et < NBR_CELL))
```
