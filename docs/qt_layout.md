# QT Layout <!-- omit in toc -->

Jusqu'à maintenant, nous avons vu des exemples ne comprenant qu'un ou deux widgets placés simplement un après l'autre dans l'écran. Mais comment faire pour placer les widgets précisément à l'écran?

QT fournit un ensemble de classe permettant de diviser l'interface: les `layouts`

Nous n'allons couvrir qu'un sous-ensemble de ces classes. 

La documentation se trouve sur https://doc.qt.io/qt-6/layout.html et nous allons essayer les exemples de cette page.

!> Une chose importante à noter: Un `layout` est installé dans un widget (appelons le: `widget_parent`), et tout les widgets qui seront ajoutés dans le layout deviendront des enfants du widget_parent et non pas du layout.

# exemple plus complexe

Nous allons maintenant regarder un exemple plus complexe. 

Allez dans le menu `Accueil | Welcome` de QT Creator.

Cherchez pour `layout`, et ouvrez `Basic Layouts Example`. Vous devez ensuite appuyer sur le bouton `Configure project` dans QT Creator (l'écran expliquant le démo sera probablement par dessus QT Creator).

!> Pour l'instant, ne vous occupez pas des textes qui sont dans `tr()`. (Nous nous en servirons plus tard pour faire une application multilingue.) 


Quelques détails à remarquer:

- la création des différentes sections sont dans des fonctions. 
- le menu `File` est créé dans `createMenu`
- il est courant de grouper plusieurs composants dans un `QGroupBox`
- `QFormLayout` permet de rapidement créer des questionnaires.
- pour avoir des scrollbar, voir ligne 40. 
