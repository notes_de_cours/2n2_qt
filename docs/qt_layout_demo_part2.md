# QT Layout demo partie 2 : ajout de la gestion des boutons <!-- omit in toc -->

Lors du dernier cours, nous avons bâtit les bases pour notre programme qui permettra de faire la création des menus qui seront utilisés pour le TP3 et TP4. 

Dans cette leçon, nous allons continuer ce programme en ajoutant 
- la logique pour gérer les entrées;
- un meilleur affichage;
- la gestion des fichiers pour entreposer les données;
- la logique pour permettre d'effacer un item. 

## gestion des boutons

Nous allons ajouter un peu de logique afin
- d'empêcher l'usager de créer 2 repas sans avoir mis d'item dans le premier;
- d'empêcher de créer un item s'il n'y a pas de repas de créé.

Pour ce faire, nous allons changer l'état des boutons afin de les mettre `disabled` quand ils ne devraient pas être utilisés. 

Nous allons repartir de la dernière version du code de la leçon précédente. 

Première question : quelle est la logique que je dois coder?

Nous avons 2 boutons:
- 1 pour sauvegarder le repas
- 1 pour sauvegarder l'item

La logique est la suivante:
1. celui du repas doit être allumé en partant
1. celui de l'item doit être éteint en partant (on ne peut entrer un item s'il n'y a pas de repas)
1. si on sauvegarde un repas, le bouton repas devient éteint (on ne peut sauvegarder 2 repas bout à bout), et celui de l'item devient allumé (dès qu'on a un repas, on peut entrer autant d'item que désiré)
1. dès qu'on sauvegarde un item, le bouton de repas redevient allumé (on a un item dans le menu, on peut donc ajouter un nouveau repas)
1. le bouton pour l'item doit être éteint si le nom ou le prix de l'item est vide (les deux champs doivent contenir une valeur si on veut sauvegarder). 

Il faudra donc changer l'état des boutons à différentes places dans le code. Il faut donc conserver le pointeur vers les 2 boutons. Présentement, ils sont locaux à la fonction qui les créer. Nous allons donc les mettres disponibles au niveau de la classe. 

Dans fenetreprincipale.h, ajoutez les lignes

```c
    QPushButton *boutonAjoutRepas; // C1
    QPushButton *boutonAjoutItem;
```

et modifier fenetreprincipale.cpp afin d'utiliser ces variables privées:

```c
    boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18 // C2

    et 

    boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4 // C2a
```

Maintenant que nous pouvons accéder nos boutons de partout dans la classe, nous allons modifier leur état. 

Le premier que nous allons faire, est d'éteindre par défaut le bouton de sauvegarde des items. 

Ajoutez cette ligne immédiatement après la création de boutonAjoutItem. 

```c
    boutonAjoutItem->setEnabled(false); // C3 // disable par defaut, tant qu'un prix n'est pas entré.
```

Si on regarde la logique exprimée ci-haut, ce bouton devrait être allumé lorsqu'on ajouter un repas. C'est donc dans la slot ajouterRepas que nous devons le mettre `enabled` de nouveau. 

```c
  boutonAjoutItem->setEnabled(true); // C4
```

Nous pouvons aussi éteindre le bouton de sauvegarde du repas au même endroit car on ne devrait pas pouvoir ajouter un nouveau repas tant qu'on aura pas ajouter un item. 

Ajoutez donc aussi cette ligne

```c
  boutonAjoutRepas->setEnabled(false); // C5
```

Nous avons donc traité les cas 1, 2, et 3. 

Pour 4, il suffit de ré-allumer le bouton pour le repas lorsqu'on sauvegarde un item, ce qui est fait dans la slot ajouterItem. 

```c
    boutonAjoutRepas->setEnabled(true); // C6
```

Il reste le cas #5. Pour que le bouton de sauvegarde de l'item soit allumé, il faut qu'il y a ait du texte dans le nom et dans le prix de l'item. Afin de répondre à cette condition, nous allons utiliser un signal qui nous avertira dès que l'usager entre du texte dans le nom ou le prix. 

Ajoutez donc ces lignes immédiatement après la création de nomItem et prixItem (B1)

```c
    connect(nomItem, &QLineEdit::textEdited, this, &FenetrePrincipale::itemModifie); // C7
    connect(prixItem, &QLineEdit::textEdited, this, &FenetrePrincipale::itemModifie);
```

Il faut donc ajouter la slot `itemModifie())`. Comme vous le voyez, le signal émet un parametre, qui sera récupéré par la slot. Il suffit d'indiquer le type du paramètre. Ici, il s'agit d'une chaine de caractère. Elle contiendra la valeur du texte du QLineEdit. 

La fonction/slot vérifie si nomItem ou prixItem est vide. Si c'est le cas, le bouton est éteint. Si les deux ont du contenu, alors le bouton est allumé. 

```c
/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::itemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." ) { // C8
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}
```

Notez que le prix contient toujours "." à cause du masque. 

Si vous essayez ce code, vous allez voir que le bouton s'allume un peu trop vite. Ce bug est dûe au fait qu'on l'allume en C4. 

Enlevez la ligne C4 et la logique sera corrigé.  (tester, toujours tester !!!)

De plus, le bouton ne s'éteint pas une fois qu'un item est entré et que les champs nom et prix sont vidé. 
Il faut donc éteindre le bouton chaque fois qu'item est ajouté dans ajouterItem. 

```c
    boutonAjoutItem->setEnabled(false); // C9
```

Voila, notre logique est presque parfaite... que manque-t-il?

<details>
<summary>réponse </summary>

Il est possible d'ajouter un repas vide. 

Je vous laisse en exercice d'ajouter la logique pour gérer ca. Venez me voir avec votre solution si vous le désirez.

</details>

Et le code complet (sans la solution à l'exercice)
<details>
<summary>Code complet </summary>

fenetrepricipale.h

```c
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>

class QPushButton;
class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void ajouterRepas();
    void ajouterItem();
    void itemModifie(QString valeur);
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
    QLineEdit *nomItem;
    QLineEdit *prixItem;

    QPushButton *boutonAjoutRepas; // C1
    QPushButton *boutonAjoutItem;
};
#endif // FENETREPRINCIPALE_H
```

fenetreprincipale.cpp

```c
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1

       QWidget *fenetre = new QWidget(); // 2a
       QHBoxLayout *grille = new QHBoxLayout; // 3

       grille->addWidget(constructionBoiteEntrees()); // 4 // 9
       grille->addWidget(constructionSectionAffichage()); // 17

       fenetre->setLayout(grille); // 3a
       setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18 // C2
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::ajouterRepas); // 19

    //création de l'entrée pour le nom et le prix de l'item
    nomItem = new QLineEdit; // B1
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00"); //B2
    connect(nomItem, &QLineEdit::textEdited, this, &FenetrePrincipale::itemModifie); // C7
    connect(prixItem, &QLineEdit::textEdited, this, &FenetrePrincipale::itemModifie);

    QFormLayout *champsItemLayout = new QFormLayout; // B3
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4 // C2a
    boutonAjoutItem->setEnabled(false); // C3 // disable par defaut, tant qu'un prix n'est pas entré.

    connect(boutonAjoutItem, &QPushButton::clicked, this, &FenetrePrincipale::ajouterItem);


    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    grille->addLayout(champsItemLayout); // B5
    grille->addWidget(boutonAjoutItem);
    boite->setLayout(grille);

    return boite; // 8
}


/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}

// slots
void FenetrePrincipale::ajouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
  //boutonAjoutItem->setEnabled(true); // C4
  boutonAjoutRepas->setEnabled(false); // C5
}

void FenetrePrincipale::ajouterItem() { //B6
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
    boutonAjoutRepas->setEnabled(true); // C6
    boutonAjoutItem->setEnabled(false); // C9
}

/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::itemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." ) { // C8
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}
```

</details>