# QT Création de signal et slot <!-- omit in toc -->

Bien que les classes existantes dans QT aient déjà plusieurs signaux et plusieurs slots pour des actions courantes, il arrive qu'on veuille définir une nouvelle action. 

Nous allons revenir à notre exemple du départ qui contient simplement un bouton, et nous allons ajouter une action permettant de changer le texte du bouton lorsqu'on appuie dessus, et autre action qui fermera le programme lorsqu'on aura appuyé le bouton 5 fois. 

Reprenons donc notre code de base (vous devriez encore avoir la partie fenetre, mais je l'inclue pour être complet)

<details>
<summary> Code original  </summary>

**main.cpp**

```c
#include <QApplication>
#include "fenetre.h"

int main(int argc, char **argv)
{
 QApplication app (argc, argv);

 Fenetre fenetre;
 fenetre.show();

 return app.exec();
}

```

**fenetre.h**

```c
#ifndef FENETRE_H
#define FENETRE_H

#include <QWidget>

class QPushButton;

class Fenetre : public QWidget
{
    Q_OBJECT
public:
    explicit Fenetre(QWidget *parent = nullptr);
private:
    QPushButton *m_bouton;

signals:

};

#endif // FENETRE_H

```

**fenetre.cpp**

!>Notez que la connexion entre le bouton et la fenêtre a été enlevée puisque nous allons la remplacer


```c
#include "fenetre.h"
#include <QPushButton>
#include <QApplication>

Fenetre::Fenetre(QWidget *parent)
    : QWidget{parent}
{
    setFixedSize(100, 50);
    m_bouton = new QPushButton("Hello World", this);
    m_bouton->setGeometry(10, 10, 80, 30);

}

``` 

</details>

## Ajout de la slot dans la fenetre

Nous allons commencer par ajouter une slot dans `fenetre.h`. 

!> Notez que la macro `Q_OBJECT` est nécessaire. Si vous désirez en savoir plus, vous pouvez aller lire cette [documentation](https://doc.qt.io/archives/qt-4.8/moc.html). En résumé: si un objet utilise des signaux et des slots, il est nécessaire d'ajouter Q_OBJECT. 

Nous ajoutons donc la section `private slots` :


```c
#ifndef FENETRE_H
#define FENETRE_H
#include <QWidget>
class QPushButton;
class Fenetre : public QWidget
{
    Q_OBJECT
public:
    explicit Fenetre(QWidget *parent = nullptr);
    
private slots:
    void slotBoutonClicked(bool checked);     //<----

private:
    QPushButton *m_bouton;
signals:
};
#endif // FENETRE_H
``` 


Notez qu'il est courant de préfixer le nom de la fonction par `slot`. 

Il faut maintenant implémenter la nouvelle fonction `slotBoutonClicked` dans `fenetre.cpp`.

Tout d'abord, il faut indiquer que notre bouton est `checkable`, ensuite ajouter la `connexion`, et enfin ajouter la nouvelle fonction. Ce qui donne:

```c
#include "fenetre.h"
#include <QPushButton>
#include <QApplication>
Fenetre::Fenetre(QWidget *parent)
    : QWidget{parent}
{
    setFixedSize(100, 50);


    m_bouton = new QPushButton("Hello World", this);
    m_bouton->setGeometry(10, 10, 80, 30);
    m_bouton->setCheckable(true); //<----

    connect(m_bouton, &QPushButton::clicked, this, &Fenetre::slotBoutonClicked); //<----

}

void Fenetre::slotBoutonClicked(bool checked) //<----
{
 if (checked) {
 m_bouton->setText("Checked");
 } else {
 m_bouton->setText("Hello World");
 }
}

```

Exécutez le programme et appuyez sur le bouton. Le texte sur le bouton devrait changer à chaque click. 

## Ajout du signal dans la fenêtre. 

Nous allons maintenant ajouter un compteur qui nous permettra d'envoyer un signal lorsque le bouton aura été cliqué un certain nombre de fois. 

Ajoutez donc cette ligne dans la section `private:` de `fenetre.h`

```c
int m_compteur;
```

Ainsi que cette ligne dans le constructeur de `fenetre`. 

```c
m_compteur = 0;
```

et cette ligne à la fin de `slotBoutonClicked`

```c
m_compteur++;
```

Nous somme maintenant prêt à ajouter le signal.

Ajoutez ces lignes dans `fenetre.h`

```c
signals:
    void compteAtteint();
```

Même si un signal est déclaré comme une fonction, il n'est pas nécessaire de l'implémenter. La macro Q_OBJECT s'en chargera. 

Il faut maintenant émettre le signal. Nous voulons l'émettre lorsque le compteur atteindra la valeur désirée. Ajoutez donc ces lignes dans `slotBoutonClicked`, juste après l'incrémentation du compteur que nous venons d'ajouter

```c
if (m_compteur == 5) {
    emit compteAtteint();
}
```

Le mot `emit` sert à émettre le signal. 

Il ne reste qu'à connecter le signal à une slot. Nous voulons quitter le programme, ca sera donc la même slot que nous avions originalement.

Ajoutez ce code à la fin du constructeur de Fenetre. 

```c
connect(this, &Fenetre::compteAtteint, QApplication::instance(), &QApplication::quit);
```