# reversi/othello : algorithme pour capture des pièces de l'adversaire<!-- omit in toc -->

Une règle au reversi est qu'une capture d'au moins une pièce de l'adversaire doit être fait lorsqu'une pièce est mise sur le jeu. 

Voici un algorithme que vous pourriez utiliser afin de faire cette vérification (il doit y en avoir d'autres)

## 1) Détection des pièces de l'adversaire

Reprenons le diagramme utilisé pour trouver si une pièce était adjacente

![algo](_media/algo_reversi_1.png)

Nous avons déjà un algorithme qui parcourt ce tableau. Cette fois-ci, au lieu vérifier si une pièce est adjacente, il faut vérifier si cette pièce appartient à l'adversaire. 

Pour chacune des pièces de l'adversaire, il faut vérifier si une de nos pièces ce trouve sur cette ligne dans cette direction. Si c'est le cas, il faut convertir toutes les pièces entre la nouvelle qui vient d'être déposée et celle se trouvant sur la ligne. 

## 2) le parcours des lignes et diagonales

Le parcours des lignes et diagonales autour d'une pièce se résume à faire les calculs suivants:

![parcours](_media/algo_reversi_2.png)

On peut voir, par exemple, que sur la diagonale `A`, toutes les cases répondent à la formule `x-n`, `y-n`

Sur `B`, c'est `x, y-n`. 

Sur `C`, c'est `x+n, y-n` 

Sur `D`, c'est `x+n, y`

Et ainsi de suite pour les autres lignes et diagonales. 

On peut donc voir que la case adjacente à (x,y) sur une ligne ou diagonale nous donne la formule pour suivre cette ligne ou diagonale. 

Si on reprend la diagonale `A`, nous avons `(x-1, y-1)` et la formule de la diagonale est `x-1n, y-1n`

Sur B, nous avons `(x+0, y-1)`, et notre formule est `x+0n, y-1n`. 

Et ainsi de suite. 

Il suffit de prendre le nombre ( -1, 0 , +1 ) pour le x et le y et le multiplier par `n`dans une boucle afin de parcourir la ligne ou diagonale. 

## Exemple

Partons du jeu de départ

![jeu de base](_media/algo_reversi_3.png)

Si le joueur dépose une pièce noire en (D,3) . 

Commencons par convertir ces coordonnées en entier: ( 3,2 ) (étant donné que (A,1) = (0,0))

Si nous appliquons notre algorithme de recherche de pièce adverse, nous en trouverons une en position (D,4), soit (3,3).

Par rapport à (3,2) la pièce adverse, est dans la direction `( 0, +1 )`

Nous pouvons donc chercher dans cette direction pour trouver une de nos pièces. Donc en partant de (3,3) dans la  direction (0,1), on trouve la case (3,4)/(D,5), qui est noire.

On a donc détecté qu'il y a des pièces à inverser. 

## Exemple 2

Partons de ce jeu

![exemple2](_media/algo_reversi_4.png)

Si le joueur blanc dépose une pièce en (A, 1)/(0, 0), le programme détecte la pièce noire en (B, 2)/(1,1)

La direction est donc (1,1). 

L'algorithme arrivera donc au calcul suivants:

En partant de (1,1)/(B,2) allez dans la direction (1,1), ce qui donne (2,2)/(C,3), une pièce noire.   
En partant de (2,2)/(C,3) continuer dans la direction (1,1), ce qui donne (3,3)/(D, 4), une autre pièce noire.  
En partant de (3,3) + (1, 1), ce qui donne (4,4) (E, 5), une pièce blanche. 

Il y a donc une capture de possible, le coup est donc bon. 

## Exemple 3

Partons du même jeu que l'exemple 2, mais cette fois-ci, supposons que joueur blanc dépose sa pièce en (C,2)/(2,1).

Les calculs donneront:

Détection d'une pièce noire en (D, 3), la direction est donc (+1, +1).   
(D,3) + (1,1) = (E,4) = noire  
(E,4) + (1,1) = (F,5) = vide = aucune prise de possible dans cette diagonale  

Mais la boucle continue et détecte une autre pièce noire en (C,3), donc en direction (0,1)  
(C,3) + (0,1) = (C,4) = pièce blanche = prise possible. 


# Capture

Si la capture est possible, le coup est donc bon. Le programme doit alors `faire` la capture. 

L'algorithme est le même que pour la recherche, sauf que cette fois-ci au lieu de simplement détecter une pièce adverse, le programme la met de notre couleur. 

Il est à noter que l'algorithme de recherche tel qu'énoncé ici, ne fait que détecter si une capture est possible. Il n'indique pas dans quelle(s) direction(s) elle doit être faite. 

L'algorithme de capture doit donc chercher dans les 8 directions, vérifier si la capture est possible, et si oui repasser sur cette ligne/diagonale afin de faire la capture. 
 

## Exemple de capture

En partant du même tableau que les exemples précédents,

![exemple2](_media/algo_reversi_4.png)

 si le joueur blanc joue en E3, le programme doit faire les calculs suivants:

Détection d'une noire en E4, direction = (0,1).   
(E,4)+(0,1)= E5 = blanche = doit faire la capture de cette ligne  
Repart de E4 en la capturant  
Déplace en E5 = blanche = fin de la capture dans cette ligne   


Détection d'une noire en D4, direction = (-1,1)  
(D,4)+(-1,1) = C5 = blanche = doit faire la capture de cette ligne.  
Repart de D4 en la capturant.  
Déplace en C5 = blanche = fin de la capture dans cette ligne.


Détection d'une noire en D3, direction = (-1, 0)  
(D,3)+(-1,0)=(C,3) = noire  
(C,3)+(-1,0)=(B,3) = vide = il n'y pas de capture à faire sur cette ligne.   

