# QT Layout demo <!-- omit in toc -->

Nous allons recréer,  de facon graphique, l'application permettant de créer les menus qui devront être affichés dans votre TP 3 & 4. 

![demoCreationMenu](_media/demoCreationMenu.png ':size=400' )


!> Le code complet sera fourni à la fin des étapes principales. Si vous vous perdez, je vous demanderai de continuer à suivre les instructions même si ca ne compile pas. Vous pourrez toujours écraser votre code à la fin d'une étape.


Commencons par créer un nouveau projet. 


Fichier/Nouveau projet

Application (Qt) / Qt Widgets Application

Donnez lui le nom `qt_demoCreationMenu`

Sélectionnez `qmake`

Changez `MainWindow` pour `FenetrePrincipale`

Déselectionnez `Generate Form`

Nous ne toucherons pas à `main.cpp`

## étape 1 fenêtre principale

Allez dans `fenetreprincipale.cpp`

Nous allons changer le code du constructuer de FenetrePrincipale afin

- 1) de changer la taille de la fenêtre à l'aide de resize;
- 2) d'ajouter un widget qui sera le centre de notre fenêtre; 
- 3) ce widget contiendra un layout horizontal;
- 4) et nous allons mettre 2 sous-widgets: la partie de gauche servant à faire les entrées, et la partie de droite servant à faire l'affichage. Nous allons donc construire 2 QGroupBox et les ajouter dans le widget principal. 

Dans le `constructeur` de `FenetrePrincipale`, ajoutez ce code:

```c
    resize(800, 600); // 1

    QWidget *fenetre = new QWidget(); // 2a
    QHBoxLayout *grille = new QHBoxLayout; // 3

    grille->addWidget(new QGroupBox("Entrées")); // 4 
    grille->addWidget(new QGroupBox("Sortie"));

    fenetre->setLayout(grille); // 3a
    setCentralWidget(fenetre);  // 2b
```

Il faut bien entendu ajouter les include pour QHBoxLayout, QGroupBox. 

!> À partir de maintenant, si je n'indique pas de faire un include, vous êtes assez grand pour le faire vous même :smile:

## étape 2 séparation en fonctions

Les 2 QGroupBox que nous venons d'ajouter doivent être peuplés avec beaucoup de sous composants. Il serait possible de le faire directement dans le constructeur, mais ce code va rapidement devenir assez long. Il est donc mieux de séparer tout ca en fonctions. 

Commençons par la fonction qui créera les champs pour faire l'entré du nom du repas. L'appel de cette fonction va remplacer la création du QGroupBox de l'étape 4 ci-haut. 

Ajoutons donc le code pour 
- 5) crée l'éditeur pour demander le nom du repas;
- 5a) met l'éditeur dans un QFormLayout. Notez que le texte sera mis dans un QLabel automagiquement;
- 6) crée un layout vertical pour y mettre le formulaire (et plus tard le bouton de sauvegarde);
- 7) met ce layout dans une boite;
- 8) return cette boite afin qu'elle soit ajoutée dans le layout de la fenêtre principale;
- 9) remplacer l'étape 4 par l'appel de function.

Ce qui nous donne ce code:

```c
/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    QLineEdit *nomRepas = new QLineEdit; // 5
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    boite->setLayout(grille);

    return boite; // 8
}

```

et 

```c
       grille->addWidget(constructionBoiteEntrees()); // 4 // 9
```

> Question: pourquoi est-ce ok de retourner `boite` étant donné qu'il est sur la pile ?

<details>
<summary >Réponse</summary>

`boite` est un pointeur. Donc, quand on le retourne, c'est uniquement l'adresse, donc une valeur numérique, qui est retournée. L'objet sur lequel pointe boite est sur le tas. Il sera ajouté à la structure de la page via addWidget, et donc `free` (via le destructeur) en tant qu'enfant (indirect) de la fenêtre

</details>


## ajout de la boite d'affichage

Avant de continuer plus loin nos entrées, nous allons créer la section qui servira à l'affichage. De cette façon nous pourrons tester que ce que nous entrons est conservé. 

Nous allons donc prendre la même technique que pour les entrées et créer une fonction pour gérer cette section. 

Nous allons mettre les données entrées dans un tableau. La première colonne contiendra le nom du repas, la 2e le nom d'un item, et la 3e le prix. 

Ajoutons donc le code pour

- 10) crée un tableau ayant 3 colonnes;
- 11)  met la table en lecture.
- 12) met un nom aux colonnes;
- 13) enlève l'entête pour les lignes; (notez que vous aurez une erreur. Pour la corriger, il faut inclure QHeaderView )
- 14) crée un layout vertical pour y mettre la table;
- 15) met ce layout dans une boite, et la table dans le layout;
- 16) return cette boite afin qu'elle soit ajoutée dans le layout de la fenêtre principale;
- 17) remplacer l'étape 4 par l'appel de function. 

```c
/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    QTableWidget *affichageMenu = new QTableWidget; // 10
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}
```

## ajout d'un bouton de sauvegarde

Nous avons maintenant une boite pour faire l'entrée du nom du repas et une boite pour faire l'affichage. Il serait intéressant d'avoir un bouton pour transférer les données de l'entré à la sortie  

Mais comment le faire ?

Nous allons ajouter un bouton `Ajouter le repas`

Nous allons donc ajouter un `QPushButton` et lui associer une `action` (`SLOT`).

Suivons donc les étapes:

- 18) creation  du bouton dans la boite d'entrees;
- 19) ajout de la connexion entre le signal et la slot.
- 20) ajout du bouton dans le layout du repas;

```c
    //le bouton pour sauvegarder le nom du repas
    QPushButton *boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::ajouterRepas); // 19
    ....
    grille->addWidget(boutonAjoutRepas); // 20

```

Il faut maintenant créer la slot

Celle-ci devra aller chercher l'information dans l'entrée du nom de repas, et l'ajouter dans le tableau que nous venons d'ajouter en sortie. 

>> Première question: comment aller chercher les données dans l'entrée du nom de repas?

Présentement, nous avons une variable `nomRepas` contenant le QLineEdit. Mais celle-ci est locale à la fonction ConstructionBoiteEntrees. Il faudrait garder ce pointeur. Nous allons donc ajouter des variables dans notre classe pour garder ces pointeurs

- 21) ajout d'une variable `private` `nomRepas` dans `FenetrePrincipale.h`
- 22) changement de variable locale à variable 

dans FenetrePrincipale.h, dans la section private 
```c
    QLineEdit *nomRepas; // 21

```
Bien entendu, il faut ajouter soit le `#include`, soit `class` pour `QLineEdit`

dans Fenetreprincipale.cpp, à l'étape #4

```c
    nomRepas = new QLineEdit; // 5 //22
```

et il faudra aussi accéder à `affichageMenu` dans les sorties. Je vous laisse 2 minutes pour faire la modification afin d'avoir une variable dans la classe (23) et l'utiliser dans constructionSectionAffichage (24). 

Il nous reste maintenant à ajouter le code pour la slot `ajouterRepas()`

Elle sera déclarée dans une nouvelle section `private slots:` de `FenetrePrincipale.h`

Les étapes de cette fonction sont:

- 25) ajouter une ligne dans le tableau;
- 26) transférer le texte de `nomRepas` dans la première colonne de cette nouvelle ligne;
- 27) vider `nomRepas`
  
```c
// slots
void FenetrePrincipale::ajouterRepas() {
    affichageMenu->insertRow(affichageMenu->rowCount()); // 25
    affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
    nomRepas->clear(); // 27
}
```

<details>
<summary>FenetrePrincipale au complet </summary>

.h
```c
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>

class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void ajouterRepas();
    
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
};
#endif // FENETREPRINCIPALE_H
```

.cpp
```c
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1

       QWidget *fenetre = new QWidget(); // 2a
       QHBoxLayout *grille = new QHBoxLayout; // 3

       grille->addWidget(constructionBoiteEntrees()); // 4 // 9
       grille->addWidget(constructionSectionAffichage()); // 17

       fenetre->setLayout(grille); // 3a
       setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    QPushButton *boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::ajouterRepas); // 19

    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    boite->setLayout(grille);

    return boite; // 8
}


/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}

// slots
void FenetrePrincipale::ajouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
}

```
</details>

## Exercice

En exercice, vous devez refaire les mêmes étapes, mais cette fois-ci, vous devez ajouter l'entrée pour le nom de l'item qui sera à ajouter dans un repas (B1), ainsi que l'entrée pour le prix. Notez que ces 2 nouvelles entrées seront dans la même boite que l'entrée du repas (B5) (nous verrons plus tard comment mieux formater le tout en ajoutant des groupbox) Vous devez aussi ajouter un bouton pour faire le transfert de l'item et de son prix (B4) dans les 2e et 3e colonne d'une nouvelle ligne de la table de sortie (B6). 

Vous pouvez ajouter cette logique dans la fonction constructionSectionEntrees. 

Pour ajouter un masque permettant de forcer l'entrée d'un montant, il suffit d'ajouter 

```c
 prixItem->setInputMask("09.99") // B2
```

Notez que pour l'instant, rien n'empêche de créer un item sans qu'il y ait de repas, ou de créer un deuxième repas sans rien n'avoir mis dans le premier. Nous allons régler ces problèmes de logique un peu plus tard. 

Vous trouverez la solution complète ci-dessous. Bien entendu, essayez de trouver votre solution avant d'aller voir celle proposée. 

<details>
<summary>Solution </summary>
.h

```c
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>

class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void ajouterRepas();
    void ajouterItem();
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
    QLineEdit *nomItem;
    QLineEdit *prixItem;
};
#endif // FENETREPRINCIPALE_H
```

.cpp

```c
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1

       QWidget *fenetre = new QWidget(); // 2a
       QHBoxLayout *grille = new QHBoxLayout; // 3

       grille->addWidget(constructionBoiteEntrees()); // 4 // 9
       grille->addWidget(constructionSectionAffichage()); // 17

       fenetre->setLayout(grille); // 3a
       setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    QPushButton *boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::ajouterRepas); // 19

    //création de l'entrée pour le nom et le prix de l'item
    nomItem = new QLineEdit; // B1
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00"); //B2

    QFormLayout *champsItemLayout = new QFormLayout; // B3
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    QPushButton *boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4
    connect(boutonAjoutItem, &QPushButton::clicked, this, &FenetrePrincipale::ajouterItem);


    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    grille->addLayout(champsItemLayout); // B5
    grille->addWidget(boutonAjoutItem);
    boite->setLayout(grille);

    return boite; // 8
}


/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}

// slots
void FenetrePrincipale::ajouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
}


void FenetrePrincipale::ajouterItem() {
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
}
```

</details>

