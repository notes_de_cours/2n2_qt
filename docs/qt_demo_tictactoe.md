# Premier contact avec QT : tictactoe <!-- omit in toc -->

Nous allons développer un jeu de tictactoe à l'aide de QT afin de nous familiariser avec les concepts que nous venons de visiter.

Le résultat devrait ressembler à ceci:

![tictactoe](_media/tictactoe_ecran_principal.png)

Le fonctionnement sera très simple: l'usager entre les coordonnées xy de la cellule qu'il veut occuper, et appuie sur Jouer.

Démarrez un nouveau projet, `Application (Qt)` / `Qt Widgets Application`,  et donnez-lui le nom de `qt_tictactoe`

Changez `MainWindow` pour `MainWindow` , et décochez `Generate Form`

Nous ne toucherons pas à `main.cpp`

# Les variables

Nous allons commencer par définir certains paramètres du jeu, tels que la taille des cases, le nombre de celles-ci, ainsi que certaines variables qui nous seront utiles. 

Notez que ces éléments semblent sortir par magie... mais dans la vraie vie, on les définirait au besoin. 

Allez dans `mainwindow.h` et ajoutez ce code en dessous du `#include <QMainWindow>`

```c
#define TAILLE 50
#define NBR_CELL 3
```

Et ajoutez ce code juste avant la fin de la déclaration de la classe ( la } )

```c
private:

    const QString ETIQUETTE = "ABCDEFGHI";
    int tableau[NBR_CELL][NBR_CELL];
    QLineEdit *choixXY;
    QGraphicsScene *scene;
```

Bien entendu QLineEdit et QGraphicsScene ne sont pas définis, il faut ajouter 
`class QLineEdit` et   
`class QGraphicsScene`  
en `forward declaration`   
> **TRUC** clic de droit sur la ligne en erreur pour QLineEdit et `refactor / add forward declaration`

# Le layout de base

Allez maintenant dans `mainwindow.cpp` et changez le code du constructeur pour celui-ci:

```c
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    setCentralWidget(fenetre);
```

Nous avons donc une QMainWindow contenant un Qwidget (fenetre) contenant un QVBoxLayout (grille) pour faire la gestion de l'organisation des widgets qui y seront ajoutés. 

Nous ajoutons un QLineEdit qui servira à demander les coordonnées pour mettre le XO. Ce QLineEdit est inséré dans un QFormLayout afin d'être associé à un QLabel (automatiquement généré). Ce QFormLayout est ensuite inséré dans le QVBoxLayout. 

Nous ajoutons ensuite un QPushButton permettant au joueur de faire son coup. 

Finalement, le QWidget fenetre est sélectionné pour être la partie centrale de la QMainWindow. 


Essayons ce code !

# La grille

Nous sommes maintenant prêts à dessiner le tableau.

Commençons par les carrés. Ajoutez cette fonction:

```c
void MainWindow::initialiserJeu()
{
    // 7
    dessinerJeu();
}
```

suivit de celle-ci :

```c
void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2

        // dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                new QGraphicsRectItem(QRectF((i)*TAILLE, (j)*TAILLE, TAILLE, TAILLE)); //4
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            //6
        }
    }
}
```

!> Notez que `->` de scene->addItem est en erreur (rouge). Pour le corriger, ajoutez l'include pour `QGraphicsScene`

Ajoutez maintenant ce code à l'emplacement `// 1`

```c
scene = new QGraphicsScene;
initialiserJeu();
QGraphicsView *view = new QGraphicsView(scene);
grille->addWidget(view);
```

Essayons le code ! ... ça commence à prendre forme.

Il serait par contre intéressant d'avoir les entêtes de lignes et de colonnes afin d'aider l'usager à spécifier où il désire mettre son XO. 

Ajoutez ce code à l'emplacement `// 2`

```c
// dessine les coordonnées
QGraphicsTextItem *txt = new QGraphicsTextItem();
txt->setPlainText(ETIQUETTE[i]);
txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
scene->addItem(txt);
txt = new QGraphicsTextItem();
txt->setPlainText(QString::number(i+1));
txt->setPos(20, (i+1.5)*(TAILLE));
scene->addItem(txt);
```

et changez la ligne //4 pour celle-ci (qui fait en sorte que l'échiquier est déplacé afin de laisser de l'espace)

```c
new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));  
```

# Les actions

Il nous reste à permettre à l'usager d'entrer les coordonnées qu'il désire occuper, et d'appuyer sur le bouton pour jouer. 

Nous allons ajouter une slot sur le bouton qui ira chercher les coordonnées qui sont dans `choixXY` et faire un dessin à ces coordonnées. 

Allons-y pour le signal et la slot. 

Ajoutez cette ligne à l'emplacement `//3`  

```c
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);
```

Ajoutez ensuite cette fonction à la fin de mainwindow.c

```c
// Slots 

void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
//8
        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();
            joueur =  ((joueur)%2) +1;
        } else {
            // 5
             
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}
```

Il faut ajouter `slotJouer()` dans la section des slots dans fenetreprincipal.h. 

> **Truc**: faire un clic de droit sur slotJouer, choisir `Refactor`, et `ajouter a la declaration private slots`

Ajouter cette ligne dans mainwindow.h, dans la section `private`

```c
    int joueur = 1;
```

Cette fonction va chercher le caractère au début de choixXY, ainsi que le chiffre à la deuxième position. 
Il ne peut rien n'y avoir d'autre, car le masque de choixXY ne permet rien d'autre (`choixXY->setInputMask("A9");`)

Le caractère est converti en int en allant chercher la position de ce caractère dans ETIQUETTE (Il y a probablement de meilleures façons de le faire, mais c'est simple et efficace)

Le chiffre est converti en int, et diminué de 1. 

Ces deux int vont maintenant nous permettre de nous positionner dans le tableau bidimensionnel. 

Ce tableau indique le numéro du joueur ayant réservé cette case. 

Une vérification est faite pour s'assurer que les coordonnées font du sens (dans le tableau, et pas déjà occupée). Pour l'instant, on ne fait rien pour de mauvaises coordonnées, mais plus tard, ça sera une bonne place pour faire un buzzer!

Il reste à rafraichir le jeu avec un X ou un O à la bonne position. 

Il faut modifier dessinerJeu() afin qu'elle dessine le tableau par-dessus le quadriller du jeu. 

Ajoutez ce code à l'emplacement // 6

```c
//dessine les pieces
if(tableau[i][j] != 0)
    {
        // 8
        dessinerPiece(i,j,tableau[i][j]);
    }
```

et ajoutez cette fonction :

```c
void MainWindow::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 20, 20);
    if(aJoueurId==1) {
       rond->setBrush(Qt::black);
    } else {
       rond->setBrush(Qt::white);
    }
    scene->addItem(rond);
};
```

Notez que par simplicité, au lieu de X et O, c'est des O de couleurs différentes. Nous allons régler ce défaut un peu plus tard. 

Essayons ce code

Est-ce que ça fonctionne ? Y a-t-il des ronds de dessinés alors qu'aucun coup n'a été joué?

!> si ça fonctionne, déplacez la définition de tableau dans le .h.  

Pourquoi y a-t-il des ronds de temps en temps?

Réponse: le tableau n'est pas initialisé à 0. 

Pour le vérifier, nous allons ajouter cette ligne à l'emplacement `// 8`

```c
qDebug() << tableau[i][j];
```

C'est un peu l'équivalent du printf pour débugger en C. 

Redémarrez le programme, et ouvrez le tab `3 sortie de l'application ` (en bas complètement de Qt Creator)

Réessayez le programme en déplaçant la description de tableau dans le .h. Remarquez que l'affichage n'est pas 0 lorsque ça ne fonctionne pas. 

Pour régler ce problème, ajoutez cette ligne à l'emplacement `// 7`

```c
for(int i=0; i<NBR_CELL; i++) {
    for(int j=0; j<NBR_CELL; j++) {
        tableau[i][j] = 0;
    }
}
```


Réessayez, maintenant ça devrait fonctionner. Vous pouvez enlever le qDebug. 

Vous pouvez maintenant jouer en entrant des coordonnées

Notez que lorsqu'un joueur entre de mauvaises coordonnées, il ne perd pas son tour.

# Les images

Pour l'instant, nous avons dessiné des ronds pour représenter le X et le O. Nous allons maintenant utiliser des images. Rien de **fancy**, mais ça vous montre comment le faire. 

![x](_media/x.png)

![rond](_media/rond.png)

Créez un sous-répertoire `images` dans le répertoire de votre projet, et téléchargez-y ces 2 images. 

Dans votre projet, ajouter un nouveau fichier de type `Qt / Qt Resource File`. Nommez le `ressources.qrc`

Dans la fenêtre qui apparait, cliqué sur `Add prefix`, et inscrivez `res`

Appuyez maintenant sur `add files` et ajoutez les deux fichiers que nous venons de créer dans images. 

Sauvegardez ces nouvelles ressources  (ctrl-s).

Retournez à l'éditeur de mainwindow.cpp, et **remplacez** le code de dessinerPiece par celui-ci:

```c
QGraphicsPixmapItem *item;
if(aJoueurId==1) {
    item = new QGraphicsPixmapItem(QPixmap(":/res/images/rond.png"));
} else {
    item = new QGraphicsPixmapItem(QPixmap(":/res/images/x.png"));
}
item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
item->setScale(0.25);
scene->addItem(item);

```

Réessayez le code... 

# Multimédia

Afin d'ajouter du son, nous aurons besoin de la classe QSoundEffect. Cette classe est dans le composant `multimedia` de Qt et n'est probablement pas incorporé dans votre installation. 

Pour l'ajouter, allez dans le menu Outils/Qt Maintenance Tools/Start Maintenance tools

![maintenance](_media/multimedia.png)

Connectez-vous et choisissez la première option offerte: Ajouter ou supprimer des composants. 

Ouvrez Qt/Qt 6.4.2/Additionnal libraries   et sélectionnez `Qt Multimedia`

Faites suivant. 

Une fois l'installation faite, redémarrez QT Creator. 

# Son


Il reste à ajouter le son indiquant que l'usager a entré de mauvaises coordonnées. 

Téléchargez un son de buzzer ( par exemple https://mixkit.co/free-sound-effects/buzzer/ ) et placez-le dans le répertoire `sons` dans votre projet. 

Sélectionnez `ressources.qrc` dans votre projet et `Open in editor`

Ajoutez le fichier de son. 

Ajouter cette ligne dans la section `private` de `mainwindow.h`

```c
QSoundEffect *buzzer;
```

Ces lignes à la fin du constructeur de mainwindow :

```c
buzzer = new QSoundEffect;
buzzer->setSource(QUrl::fromLocalFile(":/res/sons/LENOMDEVOTREFICHIER.wav"));
buzzer->setLoopCount(1);
buzzer->setVolume(0.5f);
```
N'oubliez pas de mettre le nom de **votre** fichier. 

> **TRUC** Vous pouvez faire un clic de droit sur votre fichier de son dans les ressources et  copier le path. 

Ajoutez `#include <QSoundEffect>` au début de `mainwindow.c`.

Et ajoutez cette ligne au début du fichier `.pro` de votre projet

```
QT += multimedia
```

et finalement ajoutez cette ligne à l'emplacement `// 5`.

```c
buzzer->play();
```

Réessayez votre programme et entrez une mauvaise coordonnée. 

# Le menu

Il reste une dernière petite étape... bien que ce jeu soit addictif, il est possible qu'un joueur veuille en sortir. Nous allons donc ajouter un menu pour quitter le jeu. 

Ajoutez ces lignes au début du constructeur:

```c
 QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
 fichierMenu->addAction("Quitter", this, &MainWindow::slotQuitter);
```

Vous devez aussi ajouter `#include <QMenuBar>`. 


Et la fonction `slotQuitter()` à la fin de mainwindow.c

```c
void MainWindow::slotQuitter() 
{ 
    QApplication::quit(); 
}
```

Et voilà!


Bien entendu, il resterait à ajouter la logique permettant de déterminer s'il y a un vainqueur. 

!> EN EXERCICE : Quel serait l'algorithme pour déterminer un gagnant?




# Solution complète

Comme à l'habitude, la solution finale:

<details>
<summary>mainwindow.h</summary>

```c
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
#define TAILLE 50
#define NBR_CELL 3

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void slotJouer();

private:
    int joueur = 1;
    int tableau[NBR_CELL][NBR_CELL];

    const QString ETIQUETTE = "ABCDEFGHI";
    QLineEdit *choixXY;
    QGraphicsScene *scene;
    QSoundEffect *buzzer;

    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);

};
#endif // FENETREPRINCIPALE_H


```

</details>

<details>
<summary>mainwindow.cpp</summary>

```c
#include "mainwindow.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

#include <QSoundEffect>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);

    setCentralWidget(fenetre);

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/game-show-buzz.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);
}

MainWindow::~MainWindow()
{
}

void MainWindow::initialiserJeu()
{
    // 7
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }
    dessinerJeu();
}
void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);

        //dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(tableau[i][j] != 0)
            {
                // 8
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}

void MainWindow::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    /*
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 20, 20);
    if(aJoueurId==1) {
       rond->setBrush(Qt::black);
    } else {
       rond->setBrush(Qt::white);
    }

    scene->addItem(rond);
    */
    QGraphicsPixmapItem *item;
    if(aJoueurId==1) {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/rond.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/x.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);
};

// Slots

void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
//8
        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();
            joueur =  ((joueur)%2)+1;
        } else {
            // 5
            buzzer->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}


```

</details>

<details>
<summary>main.cpp</summary>

```c
#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

```

</details>

Bien entendu, il faut ajouter les images et le son et changer le code afin de refléter leur nom. 

Et voici ce qu'il faut ajouter pour afficher qu'il y a un gagnant:


<details>
<summary>solution pour gagnant</summary>

Ajoutez cette slot:

```c
void MainWindow::slotVerifierGagnant() {
    int gagnant = 0;
    int couleur = tableau[derniereCol][derniereLigne];
    int compteurLigne = 0;
    int compteurCol = 0;
    int compteurDiag1 =0;
    int compteurDiag2 = 0;
    if(couleur != 0)  {
        for(int i=0; i<NBR_CELL; i++) {
            // verifie la colonne
            if(tableau[derniereCol][i]==couleur) {
                compteurCol++;
            }
            //verifie la ligne
            if(tableau[i][derniereLigne] == couleur) {
                compteurLigne++;
            }

        }

        // verifie si sur une diagonale
        // soit que col==ligne, soit que col+ligne = NBR_CELL-1
        if(derniereLigne==derniereCol || derniereLigne+derniereCol == NBR_CELL-1) {
            for(int i=0; i<NBR_CELL; i++) {
                if(tableau[i][i] == couleur){
                    compteurDiag1++;
                }
                if(tableau[(NBR_CELL-1) -i][i] == couleur) {
                    compteurDiag2++;
                }
            }
        }

        gagnant = (compteurCol == NBR_CELL) || (compteurLigne == NBR_CELL)
                ||(compteurDiag1 == NBR_CELL) || (compteurDiag2 == NBR_CELL);

        if(gagnant) {
            QMessageBox msgBox;
            msgBox.setText("Nous avons un gagnant!!");
            msgBox.exec();

            scene->clear();
            joueur = 1;
            initialiserJeu();
        }
    }
}
```

Ajoutez-ce signal pour le bouton de jouer (après le signal déjà présent)

```c
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotVerifierGagnant);
```

Ces 2 variables private dans mainwindow.h:

```c
    int derniereCol = 0;
    int derniereLigne = 0;
```

Et ces 2 lignes dans initialiserJeu() juste avant l'appel de dessinerJeu() :


```c
    derniereCol = 0;
    derniereLigne = 0;
```

Et voilà!. 

</details>