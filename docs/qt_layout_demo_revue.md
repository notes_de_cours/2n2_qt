# QT Layout demo revue <!-- omit in toc -->


Avant de continuer, nous allons faire un résumé de tout ce que nous avons appris dans ce démo jusqu'à maintenant. 

## widget et layout

Afin de construire l'interface, nous avons utilisé des widgets afin d'afficher le visuel, et des layouts afin de placer ces widgets les uns par rapport aux autres. 

Nous avons utilisé les widgets suivants:

- `QWidget` : (2a) (2b) il nous a servi comme composant principal. Nous n'avions besoin que d'un composant pour contenir les autres. Il n'a pas de caractéristiques spéciales, c'est affiché simplement comme un fond qui prend tout l'espace. 
- `QGroupBox` : nous en avons mis 2 à l'intérieur du widget principal. Un group box sert à grouper d'autres widgets. Il s'affiche (par défaut) avec une petite bordure, et un nom. Nous l'avons utilisé pour nos Entrées et la Sortie.
- `QLineEdit` : (5) un widget permettant de faire l'entrée de texte sur une ligne. Il est souvent mis avec une QLabel dans un QFormLayout (5a) afin de s'afficher comme un champ d'entrée de données classique. 
- `QLabel` : nous n'en avons pas directement utilisé, mais lors de la création d'un QFormLayout (5a), la chaine de caractère qui est en premier paramètre est convertie en QLabel. Un QLabel peut contenir du texte ou une image, et peut être coloré, *disable* ... 
- `QTableWidget` : un widget permettant d'afficher un tableau de données. Nous l'avons utilisez pour afficher les repas et leur items. On doit spécifier le nombre de colonne, et on peut ajouter des lignes dynamiquement. 
- `QPushButton` : affiche un bouton qui peut être connecté à l'aide d'un signal vers une slot.

Afin de positionner les widgets les uns par rapports aux autres, nous avons aussi utilisé des layouts.

- `QHBoxLayout` : (3) place les widgets de gauche à droite sur une même ligne. La fenêtre principale consiste en 2 widgets placés un côté de l'autre (4) (3a).  
- `QFormLayout` : (5a) habituellement utilisé pour afficher un champ de données. Notez que l'utilisation de ce layout assure que le 'look' sera modifié selon le type d'interface de l'application (windows vs mac vs android vs iphone vs ...). 
- `QVBoxLayout` : (6) comme HBox, mais cette fois-ci les widgets sont placés les uns en dessous des autres. 

Habituellement, un layout sera inséré dans un widget, mais il est aussi possible d'ajouter un layout dans un autre layout (7). 

Qt maintient une hierarchie de widget. Lorsqu'on ajoute un layout à un widget A et qu'on ajoute un widget B dans ce layout, Qt s'occupe de créer la relation parent-enfant entre A et B. 

C'est pour cela qu'on a pas à s'occuper de détruire les objets. Ils le sont automagiquement par Qt lors de la destruction de la fenêtre principale (la `QMainWindow`). La seule facon de déconnecter un widget de cet arbre serait qu'il ne soit pas affiché.  


## Signal et Slot

Nous avons aussi vu que l'interaction entre les widgets se fait via l'utilisation de signal et de slot. 

Il faut connecter une action (le signal) d'un widget à une fonction (la slot) d'un autre widget.

La slot est déclarée dans la section `private slots` (ca peut être dans `public slot` si elle peut être appelée par d'autre classe que celle dans laquelle elle est déclarée). 

La connexion entre le signal et la slot se fait via `connect(source, signal, destination, slot)`.

Nous avons vu qu'il y a 2 syntaxes pour `connect`

```c
connect(objet_source, &classe_objet_source::signal_source, object_dest, &classe_objet_dest:slot_dest);

exemple:

 QObject::connect(curseur, &QSlider::valueChanged, barDeProgression, &QProgressBar::setValue);
```

ou

```c
connect(objet_source, SIGNAL( signal_source(type_arg)), objet_dest, SLOT(slot_dest(type_arg)));

exemple

connect(curseur, SIGNAL(valueChanged(int)), barDeProgression, SLOT(setValue(int)));
```

La première est recommandée car elle permet au compilateur de vérifier la syntaxe et la compatibilité des arguments, et un `refactoring` changera aussi ce code. La deuxième pourrait provoquer une erreur de *runtime* étant donné que l'association se fait lors de l'exécution du programme. Vous verriez alors passer un avertissement dans la console. Le programme continuerait à exécuter, mais l'association entre le signal et la slot ne se ferait pas. 

Mais parfois la première syntaxe peut donner une erreur si la slot est `overloadée`.  Dans ce cas, il faut indiquer au compilateur quel overload utiliser. 

```c
 QObject::connect(volumeDial, &QDial::valueChanged, volumeLCD , qOverload<int>(&QLCDNumber::display));
```