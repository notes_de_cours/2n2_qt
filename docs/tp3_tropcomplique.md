# TP3 2023 <!-- omit in toc -->

Pour ce 3e TP nous allons refaire le menu de restaurant, mais cette fois-ci en mode graphique. 

 
Tout comme le TP2, vous devez charger le menu à partir du fichier généré par l'exemple vue en cours. 

Comme dans le TP2, l'usager peut choisir plusieurs fois le même item. 

Vous devez afficher les items sélectionnés et leur prix. 
Vous devez aussi afficher le prix total, les taxes (15%) et le prix total avec taxes au fur et à mesure que les items sont sélectionnés. 

Il doit être possible d'effacer un item acheté, ce qui entrainera un recalcul et réaffichage du total, des taxes, et du grand total avec taxes. 

Tous les montants doivent être affichées avec 2 décimales. 

Lorsque l'usager paye:
- vous devez conserver la commande dans un fichier texte à la suite des autres commandes existantes. Vous devez conserver les items et leur prix; 
- pour chaque item sauvegardé,vous devez ajouter un identificateur permettant de regrouper tous les items d'une commande;
- cet identificateur doit être un entier qui s'incrémentera à chaque commande;
- cet identificateur doit continuer de s'incrémenter même lorsqu'on repart l'application (il doit donc être calculé à partir d'une valeur conservée dans le fichier);
- vous n'avez pas à conserver le repas d'où provient l'item, ni les totaux et les taxes;
- la commande s'efface à l'écran et le système est prêt pour la commande suivante.

Vous n'avez pas à relire la commande à partir du fichier.
   

