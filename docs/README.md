# 420-2N2-DM Développement d’applications natives I

    Section QT

    Hiver 2023

    Benoit Desrosiers

[comment]: #  (la source pour ce cours se trouve sous  https://gitlab.com/notes_de_cours/2n2_qt.git)

Cette deuxième partie du cours présente l'utilisation de QT (prononcer `cute`) comme outil de programmation afin d'apprendre à utiliser l'interface graphique. 

QT utilise le langage C++, qui est un descendant du C. C++ est un langage Orienté Objet. Certaines notions du cours 420-2N1-DM programmation orientée objet vous serons utiles, mais nous en utiliserons un minimum et vous serez guidés lors de leur utilisation. 

 Si vous désirez en apprendre plus sur QT, il y a beaucoup de livres et de tutoriels, je vous encourage à toujours en apprendre plus (exemple [QT for beginner](https://wiki.qt.io/Qt_for_Beginners)). Pour les besoins du cours, nous n'utiliserons pas QT de façon optimale. Les concepts nécessaires pour en faire une bonne utilisation vous serons enseignés plus tard, et pour ce cours nous voulons utiliser un environnement graphique le plus simplement possible, tout en améliorant vos connaissances en programmation. 

 Dans un premier temps, nous n'utiliserons pas à fond la technologie de QT. Dans la première partie, nous allons construire les interfaces seront bâties `à la main`. Une fois que vous serez un peu familier avec l'architecture, nous utiliserons un outil graphique (Qt designer) pour bâtir les interfaces. 

