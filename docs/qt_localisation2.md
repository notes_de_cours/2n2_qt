# Internationalisation de widget ajoutés dans le programme <!-- omit in toc -->

Commençons par ajouter un widget manuellement dans notre interface. 

Ajoutez dans mainwindow.h le code suivant:

```c
QLabel *texte1;
```

Ensuite à la fin du constructeur de MainWindow
```c
texte1 = new QLabel( this);
texte1->setText(tr("allo"));
texte1->setGeometry(10,150,50,30);
```

Sauvegardez mainwinddow.cpp.

Dans la console, entrez les commandes suivantes:

```
lupdate nomduprojet.pro
cd langue
linguist traduction_de_de.ts traduction_en_us.ts traduction_fr_ca.ts
... traduisez allo
cd .. 
lrelease nomduprojet.pro

```

Redémarrez le projet et changez de langue.... oups ça ne fonctionne pas pour le nouveau QLabel. 

En effet, la méthode `ui-retranslateUi(this)` ne fonctionne que pour les widgets ajoutés dans `designer`. 

Pour les widgets ajoutés manuellement, il faut réappeler la méthode `setText(tr())` chaque fois que la langue change. 

Voici comment le faire. 

Ajoutez la méthode suivante:

```c
void MainWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange) {
        texte1->setText(tr("allo"));
    }
    QWidget::changeEvent(event);
}
```

!> IMPORTANT cette méthode doit être `public`

Cette méthode est appelée sur chaque widgets.

La dernière ligne est importante afin de propager le message. 

Vous pouvez enlever le setText dans le constructeur afin de ne pas avoir de duplication de code. 

Il faut donc faire le setText pour tous les widget dans la méthode changeEvent. 

C'est un peu *tannant*, il est donc plus simple de mettre la majorité des widgets dans designer. 



