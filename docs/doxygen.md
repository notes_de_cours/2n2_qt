# Doxygen <!-- omit in toc -->

https://www.doxygen.nl/index.html

Doxygen est le standard *de facto* pour la génération de la documentation dans plusieurs langages. 

Dans le cadre de ce cours, nous allons utiliser le style de documentation préconiser par DOxygen : https://www.doxygen.nl/manual/docblocks.html

Afin de générer votre documentation, vous devez avoir installé doxygen sur votre ordi. 

Vous pouvez le télécharger à partir de https://www.doxygen.nl/download.html

Une fois l'installation fait, ouvrez un terminal (cmd) et essayé la commande `doxygen` 

Si ca indique que la commande n'est pas connue, vous devez modifier les variables d'environnement

Dans le menu démarrer, chercher pour `Modifier les variables d'environnement`

![variableenvironnement](_media/variableenvironnement.png)

Ajouter au path : `c:\Program Files\doxygen\bin` 


