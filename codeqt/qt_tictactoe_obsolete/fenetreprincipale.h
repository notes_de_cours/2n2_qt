#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H


#include <QMainWindow>

#define TAILLE 50
#define NBR_CELL 3

class QLineEdit;
class QTableWidget;
class QGraphicsScene;
class QSoundEffect;

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT



private slots:
    void quitter();
    void slotJouer();
public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private:

    const QString ETIQUETTE = "ABCDEFGHI";
    int tableau[NBR_CELL][NBR_CELL];
    QLineEdit *choixXY;
    QGraphicsScene *scene;
    int joueur = 0;
    QSoundEffect *effect;
    void dessinerJeu(QGraphicsScene *aScene);
    void rafraichirJeu(int aCol, int aLigne);
};

#endif // FENETREPRINCIPALE_H
