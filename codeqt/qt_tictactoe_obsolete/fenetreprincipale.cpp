#include "fenetreprincipale.h"
#include <QApplication>
#include <QMenuBar>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QFormLayout>
#include <QGraphicsPixmapItem>
#include <QSoundEffect>   //ajouter QT += multimedia  dans .pro

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent) {
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::quitter);

    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    scene = new QGraphicsScene;
    dessinerJeu(scene);
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotJouer);


    setCentralWidget(fenetre);
}

FenetrePrincipale::~FenetrePrincipale() {}

void FenetrePrincipale::dessinerJeu(QGraphicsScene *aScene) { //1

    effect = new QSoundEffect;
    effect->setSource(QUrl::fromLocalFile(":/pieces/sons/game-show-buzz.wav"));
    effect->setLoopCount(1);
    effect->setVolume(0.5f);

    for(int i =0; i<NBR_CELL; i++) {
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        aScene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        aScene->addItem(txt);
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            aScene->addItem(rectItem);
            tableau[i][j]=0;
        }
    }
}

void FenetrePrincipale::rafraichirJeu(int aCol, int aLigne) {

    /*QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 20, 20);
    if(joueur==0) {
       rond->setBrush(Qt::red);
    } else {
       rond->setBrush(Qt::blue);
    }
    scene->addItem(rond);
    */

    QGraphicsPixmapItem *item;
    if(joueur==0) {
        item = new QGraphicsPixmapItem(QPixmap(":/pieces/images/rond.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/pieces/images/x.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);

};
// slots

void FenetrePrincipale::quitter() { QApplication::quit(); }

void FenetrePrincipale::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;

        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur+1;
            rafraichirJeu(col, ligne);
            joueur =  ((joueur+1)%2);
        } else {

            effect->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}
