#include "fenetreprincipale.h"
#include <QMenuBar>
#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QTableWidgetItem>
#include <QStringList>
#include <QHeaderView>
#include <QFormLayout>
#include <QApplication>

#include "gestionnairemenu.h"


FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600);
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
    fichierMenu->addAction("Quitter", this, SLOT(quitter()));

    QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));
    menuMenu->addAction("Charger", this, SLOT(chargerMenu()));
    menuMenu->addAction("Vider", this, SLOT(viderMenu()));
    menuMenu->addAction("Sauvegarder", this, SLOT(sauvegarderMenu()));

    QWidget *fenetre = new QWidget();
    QHBoxLayout *grille = new QHBoxLayout;
    fenetre->setLayout(grille);

    grille->addWidget(constructionSectionAjout());
    grille->addWidget(constructionSectionAffichage());
    setCentralWidget(fenetre);
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox* FenetrePrincipale::constructionSectionAjout() {

    // création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit;
    nomRepas->setFocus();
    QFormLayout *champsNomRepasLayout = new QFormLayout;
    champsNomRepasLayout->addRow("Nom du repas:", nomRepas);

    boutonAjoutRepas = new QPushButton("Ajouter le repas");
    connect(boutonAjoutRepas, SIGNAL(clicked()), this, SLOT(ajouterRepas()));

    // création de la section pour le nom du repas
    QGroupBox *nomRepasBox = new QGroupBox("Repas");
    QVBoxLayout *nomLayout = new QVBoxLayout();
    nomLayout->addLayout(champsNomRepasLayout);
    nomLayout->addWidget(boutonAjoutRepas);
    nomRepasBox->setLayout(nomLayout);



    // création de l'entrée pour le nom de l'item
    nomItem = new QLineEdit;
    connect(nomItem, SIGNAL(textEdited(QString)), this, SLOT(itemModifie(QString)));
    // création de l'entrée pour le prix de l'item
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00");

    QFormLayout *champsItemLayout = new QFormLayout;
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    boutonAjoutItem = new QPushButton("Ajouter l'item");
    boutonAjoutItem->setEnabled(false); // disable par defaut, tant qu'un prix n'est pas entré.
    connect(boutonAjoutItem, SIGNAL(clicked()), this, SLOT(ajouterItem()));
    connect(prixItem, SIGNAL(textEdited(QString)), this, SLOT(itemModifie(QString)));

    // création de la section pour la gestion de l'item
    QGroupBox *itemBox = new QGroupBox("Item");
    QVBoxLayout *itemLayout = new QVBoxLayout();


    itemLayout->addLayout(champsItemLayout);
    itemLayout->addWidget(boutonAjoutItem);

    itemBox->setLayout(itemLayout);


    // bouton pour effacer  un item
    QPushButton *boutonEffacerItem = new QPushButton("Effacer l'item sélectionné");
    connect(boutonEffacerItem, SIGNAL(clicked()), this, SLOT(effacerItem()));
    QHBoxLayout *effacerLayout = new QHBoxLayout;
    effacerLayout->addWidget(boutonEffacerItem);
    QGroupBox *effacerBox = new QGroupBox("effacer");
    effacerBox->setLayout(effacerLayout);

    //Construction de la grille de l'affichage
    QGroupBox *boite = new QGroupBox("Entrées");
    QVBoxLayout *grille = new QVBoxLayout;

    grille->addWidget(nomRepasBox,1);
    grille->addWidget(itemBox,1);
    grille->addWidget(effacerBox,6);

    /*
    grille->setStretch(0,40);
    grille->setStretch(1,10);
    grille->setStretch(2,40);
    */
    boite->setLayout(grille);

    return boite;
}

/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    QGroupBox *boite = new QGroupBox;
    QVBoxLayout *grille = new QVBoxLayout;

    affichageMenu = new QTableWidget;
    affichageMenu->setColumnCount(3);
    affichageMenu->setSelectionBehavior( QAbstractItemView::SelectItems );
    affichageMenu->setSelectionMode( QAbstractItemView::SingleSelection );

    QStringList entete;
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false);

    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers);

    grille->addWidget(affichageMenu);
    boite->setLayout(grille);
    return boite;
}



// slots
void FenetrePrincipale::ajouterRepas() {
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text()));
    nomRepas->clear();
    boutonAjoutRepas->setEnabled(false);
    peutAjouterItem = true;
}

void FenetrePrincipale::ajouterItem() {
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
    boutonAjoutItem->setEnabled(false); // disable par defaut, tant qu'un prix n'est pas entré.
    boutonAjoutRepas->setEnabled(true);
}

void FenetrePrincipale::effacerItem() {
    QList itemsSelectionnes = affichageMenu->selectedRanges();
    if(!itemsSelectionnes.isEmpty()) {
        int ligneSelectionnee = itemsSelectionnes.first().topRow();
        affichageMenu->removeRow(ligneSelectionnee);
        affichageMenu->clearSelection();
    }
}
/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::itemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." || !peutAjouterItem) {
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}

void FenetrePrincipale::viderMenu() {
    affichageMenu->setRowCount(0);
    peutAjouterItem = false;
}

void FenetrePrincipale::sauvegarderMenu() {
    GestionnaireMenu *gestionMenu = new GestionnaireMenu(nomFichier);
    gestionMenu->sauvegarderMenu(convertirQTableIWidgetEnMenu(affichageMenu));
}

void FenetrePrincipale::chargerMenu() {
    GestionnaireMenu *gestionMenu = new GestionnaireMenu(nomFichier);
    menu lMenu;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    gestionMenu->chargerMenu(&lMenu);
    convertirMenuEnQTableWidget(&lMenu);
}

menu FenetrePrincipale::convertirQTableIWidgetEnMenu(QTableWidget *aTable)
{
    menu lMenu;
    repas *lRepas;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    for(int i = 0; i<aTable->rowCount(); i++) {
        QTableWidgetItem *titi = aTable->item(i,0);
        if((aTable->item(i,0))) {
            //s'il y a qqchose dans la colonne 0, c'est donc un repas
            lRepas = (repas *)calloc(1, sizeof(repas)); //sur le tas

            //convertion du titre qui est dans la QString qui est dans la table, vers char*
            QByteArray ba = aTable->item(i,0)->text().toLocal8Bit();
            strcpy(lRepas->titre, ba.data());
            if(!lMenu.listeRepas) {
                lMenu.listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                lMenu.listeRepas = (repas **)realloc(lMenu.listeRepas, (lMenu.nombreRepas+1) *sizeof(repas*));
            }
            lMenu.listeRepas[lMenu.nombreRepas] = lRepas;
            lMenu.nombreRepas++;
        } else {
            //sinon c'est un item
            if(!lRepas->listeItem) {
                lRepas->listeItem = (item *)calloc(1, sizeof(item));
            } else {
                lRepas->listeItem = (item *)realloc(lRepas->listeItem, sizeof(item)*(lRepas->nombreItem+1));
            }
            QByteArray ba = aTable->item(i,1)->text().toLocal8Bit();
            strcpy(lRepas->listeItem[lRepas->nombreItem].titre, ba.data());
            lRepas->listeItem[lRepas->nombreItem].prix = aTable->item(i,2)->text().toFloat();
            lRepas->nombreItem++;
        }
    }
    return lMenu;
}

void FenetrePrincipale::convertirMenuEnQTableWidget(menu *aMenu)
{
    for(int i = 0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        affichageMenu->insertRow(affichageMenu->rowCount());
        affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(lRepas->titre));
        for(int j=0; j<lRepas->nombreItem; j++) {
            item lItem = lRepas->listeItem[j];
            affichageMenu->insertRow(affichageMenu->rowCount());
            affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(lItem.titre));
            affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(QString::number(lItem.prix)));
        }
    }
}


void FenetrePrincipale::quitter() {
    QApplication::quit();
}


