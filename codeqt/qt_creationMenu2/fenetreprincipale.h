#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>
#include <QMenu>
#include <QGroupBox>
#include <QLineEdit>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QPushButton>

#include "repas.h"

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

private slots:
    void chargerMenu();
    void viderMenu();
    void sauvegarderMenu();
    void quitter();

    void ajouterRepas();
    void ajouterItem();
    void itemModifie(QString valeur);

    void effacerItem();
public:
    explicit FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();

private:
    QGroupBox* constructionSectionAjout();
    QGroupBox* constructionSectionAffichage();
    menu convertirQTableIWidgetEnMenu(QTableWidget *aTable);
    void convertirMenuEnQTableWidget(menu *aMenu);
    QLineEdit *nomRepas;
    QLineEdit *nomItem;
    QLineEdit *prixItem;
    QPushButton *boutonAjoutRepas;
    QPushButton *boutonAjoutItem;
    QTableWidget *affichageMenu;

    bool peutAjouterItem;

    char nomFichier[50] = "menu.txt";

};
#endif // FENETREPRINCIPALE_H
