<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_CA">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="30"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="37"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="44"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="51"/>
        <source>9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="65"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="72"/>
        <source>8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="79"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="86"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>AC</source>
        <translation>vider</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <source>/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="126"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="133"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="147"/>
        <source>=</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="195"/>
        <source>Ruban</source>
        <translation>Ruban</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="208"/>
        <source>Opérations</source>
        <translation>Opérateurs</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Calculatrice</source>
        <translation>Calculatrice</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>Fichier</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="242"/>
        <source>Langues</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>&amp;Sortir</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Francais</source>
        <translation>Francais</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="263"/>
        <source>English</source>
        <translation>English</translation>
    </message>
</context>
</TS>
