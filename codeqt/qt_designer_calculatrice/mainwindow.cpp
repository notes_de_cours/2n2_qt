
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QActionGroup>
#include <QDir>
#include <QTranslator>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    on_pushButtonAC_clicked();

    m_langPath = ":/traductions/langue";
    changerLangue(QString("fr_ca"));
    creerMenuLangue();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ajouterNombre(int aValeur)
{
    if(operationPrecedente == 0) {
        valeurActuelle = 0;
        operationPrecedente = -1;
    }
    valeurActuelle = valeurActuelle*10+aValeur;
    afficherValeurActuelle();
    unNombreEntre = true;
    if(operationEnCours) {
        deuxNombresEntre = true;
    }
}
void MainWindow::afficherValeurActuelle()
{
    ui->lcdNumber->display(valeurActuelle);
}

void MainWindow::on_pushButtonEgal_clicked()
{
    if(deuxNombresEntre) {

        afficherNombreSurRuban(valeurActuelle);
        afficherOperateurSurRuban(0);
        total = calculerTotal();
        afficherNombreSurRuban(total);
        valeurActuelle = total;
        afficherValeurActuelle();
        operationEnCours = false;
        deuxNombresEntre = false;
        operationPrecedente = 0;
    }

}
void MainWindow::appliquerOperation(int aOperateur)
{
    if(unNombreEntre && !operationEnCours  ) {
        if(operationPrecedente == -1) {
            sautDeLigneSurRuban();
        }
        if(operationPrecedente != 0) {
            afficherNombreSurRuban(valeurActuelle);
        }
        afficherOperateurSurRuban(aOperateur);
        operationPrecedente = aOperateur;
        operande1 = valeurActuelle;
        valeurActuelle = 0;
        operationEnCours = true;
    }

}
int MainWindow::calculerTotal()
{
    int lTotal = 0;
    switch (operationPrecedente) {
    case 1:
        lTotal = operande1+valeurActuelle;
        break;
    case 2:
        lTotal = operande1-valeurActuelle;
    default:
        break;
    }

    return lTotal;
}


void MainWindow::afficherNombreSurRuban(int aNombre)
{
    ui->ruban->insertPlainText(QString::number(aNombre));
}

void MainWindow::afficherOperateurSurRuban(int aOperateur)
{
    QString affichage;
    affichage.append(operations[aOperateur]);
    ui->ruban->insertPlainText(affichage);
    sautDeLigneSurRuban();

}
void MainWindow::sautDeLigneSurRuban() {
    QString affichage;
    affichage.append("\r");
    ui->ruban->insertPlainText(affichage);
}

void MainWindow::changerLangue(QString aLangueEtPays) {
    QTranslator* lTraducteur = new QTranslator(this);
    qApp->removeTranslator(lTraducteur);
    QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
    if(lTraducteur->load(lLangue)) {
        qApp->installTranslator(lTraducteur);
        ui->retranslateUi(this);
        m_langCourrant = aLangueEtPays;
    } else {
        qDebug() << "Fichier de langue non trouvé" << aLangueEtPays;
    }
}
void MainWindow::creerMenuLangue() {

    QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangues);
    lGroupeLangue->setExclusive(true);

    connect(lGroupeLangue, &QActionGroup::triggered, this,
            &MainWindow::on_menu_action_triggered);

    // Va chercher le langage par défaut sur cet ordi
    QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
    localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

    // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

    QDir dir(m_langPath);
    QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

    for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
        // extraction du langage à partir du nom de fichier
        QString localeLangue;
        QString localeLangueEtPays;
        localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
        localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
        localeLangueEtPays = localeLangue;
        localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
        localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
        localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

        QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
        nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

        QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
        QAction *action = new QAction(ico, nomDeLangue, this);

        action->setCheckable(true);
        action->setData(localeLangueEtPays);

        ui->menuLangues->addAction(action);
        lGroupeLangue->addAction(action);

        // Sélectionne la langue du système par défaut dans le menu
        if (localeSysteme == localeLangue) {
            action->setChecked(true);
        }
    }
}


void MainWindow::on_pushButtonPlus_clicked()
{
    operation = 1;
    appliquerOperation(1);
}
void MainWindow::on_pushButtonMoins_clicked()
{
    operation = 2;
    appliquerOperation(2);
}


void MainWindow::on_pushButton0_clicked()
{
    ajouterNombre(0);
}
void MainWindow::on_pushButton1_clicked()
{
    ajouterNombre(1);
}
void MainWindow::on_pushButton2_clicked()
{
    ajouterNombre(2);
}


void MainWindow::on_pushButton3_clicked()
{
    ajouterNombre(3);
}


void MainWindow::on_pushButton4_clicked()
{
    ajouterNombre(4);
}


void MainWindow::on_pushButton5_clicked()
{
    ajouterNombre(5);
}


void MainWindow::on_pushButton6_clicked()
{
    ajouterNombre(6);
}


void MainWindow::on_pushButton7_clicked()
{
    ajouterNombre(7);
}


void MainWindow::on_pushButton8_clicked()
{
    ajouterNombre(8);
}


void MainWindow::on_pushButton9_clicked()
{
    ajouterNombre(9);
}




void MainWindow::on_pushButtonAC_clicked()
{
    operation = 0;
    valeurActuelle = 0;
    total = 0;
    unNombreEntre= false;
    deuxNombresEntre =false;
    operationPrecedente= -1;
    operationEnCours = false;
    afficherValeurActuelle();
    ui->ruban->clear();
}

void MainWindow::on_menu_action_triggered(QAction *action)
{
    if (0 != action) {
        // Charge le langage selon le data de l'action
        changerLangue(action->data().toString());
    }
}


void MainWindow::on_action_Sortir_triggered()
{
    qApp->quit();
}

