
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow

{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_pushButton1_clicked();
    void on_pushButton0_clicked();
    void on_pushButtonEgal_clicked();
    void on_pushButtonPlus_clicked();
    void on_pushButton2_clicked();
    void on_pushButton3_clicked();
    void on_pushButton4_clicked();
    void on_pushButton5_clicked();
    void on_pushButton6_clicked();
    void on_pushButton7_clicked();
    void on_pushButton8_clicked();
    void on_pushButton9_clicked();
    void on_pushButtonMoins_clicked();
    void on_pushButtonAC_clicked();



    void on_menu_action_triggered(QAction *);
    void on_action_Sortir_triggered();

private:

    QString m_langPath; // le path pour les fichiers de langue
    QString m_langCourrant; // le nom du langage présentement chargé
    QString operations = "=+-*/ ";
    int operation =0 ; // 0 =, 1 +, 2 -, 3 *, 4 /, 5 reset
    int valeurActuelle = 0;
    int total = 0;
    bool unNombreEntre = false; //est ce qu'un nombre a été entré pour que je puisse faire une op dessus.
    bool deuxNombresEntre = false; //est-ce que j'ai les 2 operandes, ou juste 1?
    int operationPrecedente = -1;
    int operande1 = 0;
    bool operationEnCours = false;

    Ui::MainWindow *ui;
    void ajouterNombre(int valeur);

    void afficherValeurActuelle();
    void appliquerOperation(int aOperateur);
    void afficherOperateurSurRuban(int aOperateur);
    void afficherNombreSurRuban(int aNombre);
    int calculerTotal();
    void sautDeLigneSurRuban();
    void creerMenuLangue();
    void changerLangue(QString aLangueEtPays);
};

#endif // MAINWINDOW_H
