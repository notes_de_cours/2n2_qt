#include "fenetreprincipale.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QMenuBar>
#include <QApplication>

#include <QSoundEffect>

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::slotQuitter);

    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    scene = new QGraphicsScene;

    placerPiecesDepart();
    rafraichirJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    //position de départ
    departXY = new QLineEdit;
    departXY->setFocus();
    departXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("départ", departXY);
    grille->addLayout(xyChoisieForm);
    departXY->setCursorPosition(0);

    //position de fin
    destXY = new QLineEdit;
    destXY->setFocus();
    destXY->setInputMask("A9");
    QFormLayout *xyDestChoisieForm = new QFormLayout;
    xyDestChoisieForm->addRow("destination", destXY);
    grille->addLayout(xyDestChoisieForm);
    destXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotJouer);

    setCentralWidget(fenetre);

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/mixkit-home-standard-ding-dong-109.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);


}

FenetrePrincipale::~FenetrePrincipale()
{
}

QGraphicsPixmapItem *FenetrePrincipale::associerImagePiece(int aPieceId)
{
    QGraphicsPixmapItem *item;

    switch(aPieceId ){
    case 0:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/roi.png"));
    break;
    case 1:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/reine.png"));
    break;
    case 2:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/fou.png"));
    break;
    case 3:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/chevalier.png"));
    break;
    case 4:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/tour.png"));
    break;
    case 5:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/pion.png"));
    break;
    case 10:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/roi_n.png"));
    break;
    case 11:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/reine_n.png"));
    break;
    case 12:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/fou_n.png"));
    break;
    case 13:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/chevalier_n.png"));
    break;
    case 14:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/tour_n.png"));
    break;
    case 15:
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/pion_n.png"));
    break;
    }
    return item;

}

void FenetrePrincipale::rafraichirJeu()
{
    scene->clear();
    dessinerJeuVide(scene);
    for(int i=0; i<NBR_CELL;i++){
        for(int j=0; j<NBR_CELL; j++) {
            int id = tableau[i][j];
            if(id != 0) {
                QGraphicsPixmapItem *item = associerImagePiece(id-1);
                dessinerPiece(i, j, item);
            }
        }
    }

}

void FenetrePrincipale::placerPiecesDepart()
{
    int id;
    QGraphicsPixmapItem *item;

    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0;j<NBR_CELL; j++){
            tableau[i][j] = 0;
        }
    }
    // les blancs en bas de l'écran
    for(int i=0;i<NBR_CELL; i++){
        id = PIECES_ID.indexOf(POSITION_DEPART[i]);
        tableau[i][NBR_CELL-1] = id+1;
    }
    //Les pions
    id=PIECES_ID.indexOf("P");
    for(int i=0;i<NBR_CELL;i++) {
        tableau[i][NBR_CELL-2] = id+1;
    }

    // les noirs en haut de l'écran
    for(int i=0;i<NBR_CELL; i++){
        id = PIECES_ID.indexOf(POSITION_DEPART[i]);
        tableau[i][0] = id+11;
    }
    //Les pions
    id=PIECES_ID.indexOf("P");
    for(int i=0;i<NBR_CELL;i++) {
        tableau[i][1] = id+11;
    }

}
void FenetrePrincipale::dessinerJeuVide(QGraphicsScene *aScene) {


    for(int i =0; i<NBR_CELL; i++) {
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        aScene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        aScene->addItem(txt);
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            aScene->addItem(rectItem);
        }
    }
}

void FenetrePrincipale::dessinerPiece(int aCol, int aLigne, QGraphicsPixmapItem *aPiece) {
    aPiece->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    aPiece->setScale(0.05);
    scene->addItem(aPiece);
};




void FenetrePrincipale::slotJouer() {

    if(!(departXY->text().isEmpty())) {
        QString colDepartTxt = departXY->text()[0];
        QString ligneDepartTxt = departXY->text()[1];
        int colDepart = ETIQUETTE.indexOf(colDepartTxt.toUpper());
        int ligneDepart = ligneDepartTxt.toInt()-1;

        QString colDestTxt = destXY->text()[0];
        QString ligneDestTxt = destXY->text()[1];
        int colDest = ETIQUETTE.indexOf(colDestTxt.toUpper());
        int ligneDest = ligneDestTxt.toInt()-1;

        if(tableau[colDepart][ligneDepart]!=0
                && colDepart>=0 && colDepart < NBR_CELL
                && ligneDepart >=0 && ligneDepart < NBR_CELL
                && colDest>=0 && colDest < NBR_CELL
                && ligneDest >=0 && ligneDest < NBR_CELL
                && tableau[colDepart][ligneDepart] > 1+(10*joueur)  // est-ce sa piece?
                && tableau[colDepart][ligneDepart] < 8+(10*joueur)
                ) { // reste a valider que la destination lui appartient, et si le move est legal

            int pieceId=tableau[colDepart ][ligneDepart];
            tableau[colDepart ][ligneDepart] = 0;
            tableau[colDest][ligneDest] = pieceId;
            rafraichirJeu();
            joueur =  ((joueur+1)%2);
        } else {
            buzzer->play();
        }
    }
    departXY->clear();
    departXY->setFocus();
    departXY->setCursorPosition(0);
    destXY->clear();
    destXY->setCursorPosition(0);
}

void FenetrePrincipale::slotQuitter()
{
    QApplication::quit();
}
