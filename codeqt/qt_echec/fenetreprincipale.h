#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QGraphicsPixmapItem;
class QSoundEffect;
class QGraphicsScene;
class QLineEdit;


#define TAILLE 50
#define NBR_CELL 8


class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();

private slots:
    void slotJouer();
    void slotQuitter();
private:

    const QString ETIQUETTE = "ABCDEFGHI";
    const QString PIECES_ID = "KRFCTP"; //King, Reine, Fou, Chevalier, Tour, Pion
    const QString POSITION_DEPART = "TCFKRFCT";
    QGraphicsPixmapItem *pieces;
    int tableau[NBR_CELL][NBR_CELL];
    QLineEdit *departXY;
    QLineEdit *destXY;
    QGraphicsScene *scene;
    int joueur = 0;
    QSoundEffect *buzzer;

    void dessinerJeuVide(QGraphicsScene *aScene);
    void dessinerPiece(int aCol, int aLigne, QGraphicsPixmapItem *aPiece);
    void placerPiecesDepart();
    QGraphicsPixmapItem *associerImagePiece(int aPieceId);

    void rafraichirJeu();
};
#endif // FENETREPRINCIPALE_H
