#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>
#include <QGroupBox>
#include <QVBoxLayout>
#include "repas.h"
#include "commande.h"

class QTableWidget;
class QPushButton;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

private slots:
    void quitter();
    void slotItemChoisi(int aLigne, int aColonne);
    void slotEffacerItem();
    void slotPayerCommande();
    void slotViderCommande();
public:
    FenetrePrincipale(QTableWidget *parent = nullptr);
    ~FenetrePrincipale();

private:
    const int nombreLigneTotauxFacture = 4;
    void chargerMenu(menu *aMenu);
    QTableWidget *construireSectionMenu(menu *aMenu);
    QTableWidget *construireSectionFacture();
    QPushButton *construireBoutonEffacer();
    char nomFichierMenu[50] = "menu.txt";
    char nomFichierCommande[50] = "commande.txt";
    menu menuCourant;

    QTableWidget *menuAffiche;
    QTableWidget *commandeTable;
    int idProchaineCommande;

    void totaliserItems(float prix);
    void convertirQTableWidgetEnCommande(structCommande *aCommande, QTableWidget *aTable);
    void ajouterTotaux(QTableWidget *aCommande);
};


#endif // FENETREPRINCIPALE_H
