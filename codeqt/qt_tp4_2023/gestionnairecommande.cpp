#include "gestionnairecommande.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "commande.h"

GestionnaireCommande::GestionnaireCommande(char *aNomFichier)
{
    strcpy(nomFichier, aNomFichier);
}
GestionnaireCommande::~GestionnaireCommande()
{

}

int GestionnaireCommande::chercherDerniereCommande() {
    int idDerniereCommande = 1;
    FILE *lFichier = fopen(nomFichier, "rb");
    if(lFichier) {  // le fichier existe, recherche la dernière ligne
        fseek(lFichier, -(sizeof(itemCommande)), SEEK_END);

        itemCommande *itemEnMemoire = (itemCommande *)calloc(1, sizeof(itemCommande));
        fread(itemEnMemoire, sizeof(itemCommande), 1, lFichier);
        idDerniereCommande = itemEnMemoire->id+1;
    }
    // si le fichier n'existe pas, id est initialisé à 1
    return idDerniereCommande;

}

void GestionnaireCommande::sauvegarderCommande(structCommande aCommande) {
    FILE *lFichier = fopen(nomFichier, "ab");

    for(int i = 0; i< aCommande.nombreItem; i++) {
        fwrite(&aCommande.listeItem[i], sizeof(itemCommande),1, lFichier);
    }

    fclose(lFichier);
}
