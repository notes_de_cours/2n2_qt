#ifndef GESTIONNAIREMENU_H
#define GESTIONNAIREMENU_H

#include "repas.h"
#include <stdio.h>


class GestionnaireMenu
{
public:
    GestionnaireMenu(char *aNomFichier);
    ~GestionnaireMenu();
    void sauvegarderMenu(menu aMenu);
    void chargerMenu(menu *aMenu);
private:
    char nomFichier[50];
};

#endif // GESTIONNAIREMENU_H
