#ifndef GESTIONNAIRECOMMANDE_H
#define GESTIONNAIRECOMMANDE_H

#include "commande.h"

class GestionnaireCommande
{
public:
    GestionnaireCommande(char *aNomFichier);
    ~GestionnaireCommande();

    int chercherDerniereCommande();
    void sauvegarderCommande(structCommande aCommande);
private:
    char nomFichier[50];
};

#endif // GESTIONNAIRECOMMANDE_H
