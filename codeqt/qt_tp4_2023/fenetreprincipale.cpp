#include "fenetreprincipale.h"
#include <QApplication>
#include <QButtonGroup>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QMenuBar>
#include <QPushButton>
#include <QRadioButton>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QVBoxLayout>

#include "gestionnairecommande.h"
#include "gestionnairemenu.h"

FenetrePrincipale::FenetrePrincipale(QTableWidget *parent)
    : QMainWindow(parent) {
  resize(800, 600);
  QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
  fichierMenu->addAction("Quitter", this, &FenetrePrincipale::quitter);

  chargerMenu(&menuCourant);

  QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));
  menuMenu->addAction("Payer", this, &FenetrePrincipale::slotPayerCommande);
  menuMenu->addAction("Vider", this, &FenetrePrincipale::slotViderCommande);

  QTableWidget *fenetre = new QTableWidget(this);
  QHBoxLayout *grille = new QHBoxLayout();
  fenetre->setLayout(grille);
  menuAffiche = construireSectionMenu(&menuCourant);
  grille->addWidget(menuAffiche);

  // charge l'id de la dernière commande afin d'avoir celui de la prochaine
  GestionnaireCommande *gestionCommande =
      new GestionnaireCommande(nomFichierCommande);
  idProchaineCommande = gestionCommande->chercherDerniereCommande();

  commandeTable = construireSectionFacture();
  QGroupBox *sectionCommande = new QGroupBox("Commande");
  QVBoxLayout *grilleCommande = new QVBoxLayout();
  sectionCommande->setLayout(grilleCommande);
  grilleCommande->addWidget(commandeTable);
  grilleCommande->addWidget(construireBoutonEffacer());

  grille->addWidget(sectionCommande);
  setCentralWidget(fenetre);
}

FenetrePrincipale::~FenetrePrincipale() {}

void FenetrePrincipale::chargerMenu(menu *aMenu) {
  GestionnaireMenu *gestionMenu = new GestionnaireMenu(nomFichierMenu);
  aMenu->listeRepas = NULL;
  aMenu->nombreRepas = 0;
  gestionMenu->chargerMenu(aMenu);
}

QTableWidget *FenetrePrincipale::construireSectionMenu(menu *aMenu) {
  QTableWidget *lMenuAffiche = new QTableWidget;

  lMenuAffiche->setColumnCount(3);
  // met la table en lecture seulement
  lMenuAffiche->setEditTriggers(QAbstractItemView::NoEditTriggers);

  // la table ne permettra de selectionner qu'une ligne à la fois
  lMenuAffiche->setSelectionBehavior(QAbstractItemView::SelectRows);
  lMenuAffiche->setSelectionMode(QAbstractItemView::SingleSelection);

  QStringList entete;
  entete.append("Repas");
  entete.append("Item");
  entete.append("Prix");
  lMenuAffiche->setHorizontalHeaderLabels(entete);
  lMenuAffiche->verticalHeader()->setVisible(false);

  connect(lMenuAffiche, &QTableWidget::cellClicked, this,
          &FenetrePrincipale::slotItemChoisi);

  //remplie l'affichage du menu à partir des données du fichier
  for (int i = 0; i < aMenu->nombreRepas; i++) {
    repas *lRepas = aMenu->listeRepas[i];
    lMenuAffiche->insertRow(lMenuAffiche->rowCount());
    lMenuAffiche->setItem(
        lMenuAffiche->rowCount() - 1, 0,
        new QTableWidgetItem(QString::fromLatin1(lRepas->titre)));
    for (int j = 0; j < lRepas->nombreItem; j++) {
      item lItem = lRepas->listeItem[j];
      lMenuAffiche->insertRow(lMenuAffiche->rowCount());
      lMenuAffiche->setItem(
          lMenuAffiche->rowCount() - 1, 1,
          new QTableWidgetItem(QString::fromLatin1(lItem.titre)));
      QTableWidgetItem *prix =
          new QTableWidgetItem(QString::number(lItem.prix, 'f', 2));
      prix->setTextAlignment(Qt::AlignRight);
      lMenuAffiche->setItem(lMenuAffiche->rowCount() - 1, 2, prix);
    }
  }
  return lMenuAffiche;
}

QTableWidget *FenetrePrincipale::construireSectionFacture() {

  QTableWidget *lCommande = new QTableWidget;

  lCommande->setColumnCount(2);
  // met la table en lecture seulement
  lCommande->setEditTriggers(QAbstractItemView::NoEditTriggers);

  // la table ne permettra de selectionner qu'une ligne à la fois
  lCommande->setSelectionBehavior(QAbstractItemView::SelectRows);
  lCommande->setSelectionMode(QAbstractItemView::SingleSelection);
  QStringList entete;
  entete.append("Item");
  entete.append("Prix");

  lCommande->setHorizontalHeaderLabels(entete);
  lCommande->verticalHeader()->setVisible(false);

  ajouterTotaux(lCommande);
  return lCommande;
}

void FenetrePrincipale::ajouterTotaux(QTableWidget *aCommande) {
  // la ligne pour le total
  aCommande->insertRow(0);
  aCommande->setItem(0, 0, new QTableWidgetItem("total:"));
  aCommande->item(0, 0)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);
  QTableWidgetItem *totalDummy =
      new QTableWidgetItem(QString::number(0, 'f', 2));
  totalDummy->setTextAlignment(Qt::AlignRight);
  aCommande->setItem(0, 1, totalDummy);
  aCommande->item(0, 1)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  // la ligne pour les taxes
  aCommande->insertRow(1);
  aCommande->setItem(1, 0, new QTableWidgetItem("taxes:"));
  aCommande->item(1, 0)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  QTableWidgetItem *taxeDummy =
      new QTableWidgetItem(QString::number(0, 'f', 2));
  taxeDummy->setTextAlignment(Qt::AlignRight);
  aCommande->setItem(1, 1, taxeDummy);
  aCommande->item(1, 1)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  // la ligne pour le grand total
  aCommande->insertRow(2);
  aCommande->setItem(2, 0, new QTableWidgetItem("grand total:"));
  aCommande->item(2, 0)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  QTableWidgetItem *grandTotalDummy =
      new QTableWidgetItem(QString::number(0, 'f', 2));
  grandTotalDummy->setTextAlignment(Qt::AlignRight);
  aCommande->setItem(2, 1, grandTotalDummy);
  aCommande->item(2, 1)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  // la ligne pour le numéro de facture
  aCommande->insertRow(3);
  aCommande->setItem(3, 0, new QTableWidgetItem("numéro facture"));
  aCommande->item(3, 0)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);

  QTableWidgetItem *numeroFacture =
      new QTableWidgetItem(QString::number(idProchaineCommande, 10));
  grandTotalDummy->setTextAlignment(Qt::AlignRight);
  aCommande->setItem(3, 1, numeroFacture);
  aCommande->item(3, 1)->setFlags(aCommande->item(0, 0)->flags() &
                                  ~Qt::ItemIsEnabled);
}

QPushButton *FenetrePrincipale::construireBoutonEffacer() {
  QPushButton *lBoutonEffacer = new QPushButton("Effacer Item");
  connect(lBoutonEffacer, &QPushButton::clicked, this,
          &FenetrePrincipale::slotEffacerItem);
  return lBoutonEffacer;
}

void FenetrePrincipale::totaliserItems(float prix) {
  float prixActuel =
      commandeTable
          ->item(commandeTable->rowCount() - nombreLigneTotauxFacture, 1)
          ->text()
          .toFloat();
  prixActuel += prix;
  float taxes = prixActuel * 0.15;
  float prixTotal = prixActuel + taxes;
  commandeTable->item(commandeTable->rowCount() - nombreLigneTotauxFacture, 1)
      ->setText(QString::number(prixActuel, 'f', 2));
  commandeTable
      ->item(commandeTable->rowCount() - nombreLigneTotauxFacture + 1, 1)
      ->setText(QString::number(taxes, 'f', 2));
  commandeTable
      ->item(commandeTable->rowCount() - nombreLigneTotauxFacture + 2, 1)
      ->setText(QString::number(prixTotal, 'f', 2));
}

void FenetrePrincipale::convertirQTableWidgetEnCommande(
    structCommande *aCommande, QTableWidget *aTable) {

  for (int i = 0; i < aTable->rowCount() - nombreLigneTotauxFacture; i++) {
    if (!aCommande->listeItem) {
      aCommande->listeItem = (itemCommande *)calloc(1, sizeof(itemCommande));
    } else {
      aCommande->listeItem = (itemCommande *)realloc(
          aCommande->listeItem,
          sizeof(itemCommande) * (aCommande->nombreItem + 1));
    }
    QByteArray ba = aTable->item(i, 0)->text().toLocal8Bit();
    itemCommande *tempItem = &aCommande->listeItem[aCommande->nombreItem];
    strcpy(tempItem->titre, ba.data());
    tempItem->prix = aTable->item(i, 1)->text().toFloat();
    tempItem->id = idProchaineCommande;
    aCommande->nombreItem++;
  }
};



// slots

void FenetrePrincipale::quitter() { QApplication::quit(); }

void FenetrePrincipale::slotItemChoisi(int aLigne, int aColonne) {
  // si la colonne 0 n'est pas vide, c'est un item et il doit être ajouté.
  // sinon, c'est un repas, et ne doit pas être ajouté.
  if (!(menuAffiche->item(aLigne, 0))) { // un item
    commandeTable->insertRow(commandeTable->rowCount() -
                             nombreLigneTotauxFacture);
    // le titre
    commandeTable->setItem(
        commandeTable->rowCount() - nombreLigneTotauxFacture - 1, 0,
        new QTableWidgetItem(menuAffiche->item(aLigne, 1)->text()));
    // le prix
    float prix = menuAffiche->item(aLigne, 2)->text().toFloat();
    QTableWidgetItem *itemPrix =
        new QTableWidgetItem(QString::number(prix, 'f', 2));
    itemPrix->setTextAlignment(Qt::AlignRight);
    commandeTable->setItem(
        commandeTable->rowCount() - nombreLigneTotauxFacture - 1, 1, itemPrix);
    totaliserItems(prix);
  }
}

void FenetrePrincipale::slotEffacerItem() {
  QList itemsSelectionnes = commandeTable->selectedRanges();
  if (!itemsSelectionnes.isEmpty()) {
    int ligneSelectionnee = itemsSelectionnes.first().topRow();
    float prix = commandeTable->item(ligneSelectionnee, 1)->text().toFloat();
    totaliserItems(-prix);
    commandeTable->removeRow(ligneSelectionnee);
    commandeTable->clearSelection();
  }
}

void FenetrePrincipale::slotPayerCommande() {
  GestionnaireCommande *gestionCommande =
      new GestionnaireCommande(nomFichierCommande);
  structCommande lCommande;
  lCommande.nombreItem = 0;
  lCommande.listeItem = NULL;
  convertirQTableWidgetEnCommande(&lCommande, commandeTable);
  gestionCommande->sauvegarderCommande(lCommande);
  free(lCommande.listeItem);
  idProchaineCommande++;
  slotViderCommande();
}

void FenetrePrincipale::slotViderCommande() {
  commandeTable->setRowCount(0);
  ajouterTotaux(commandeTable);
}
