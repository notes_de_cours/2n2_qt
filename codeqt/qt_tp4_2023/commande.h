#ifndef COMMANDE_H
#define COMMANDE_H
#define LONGUEUR_TITRE 20

// un item dans un repas
typedef struct itemCommande {
    int id;
    char titre[LONGUEUR_TITRE];
    float prix;
} itemCommande;

// une commande contenant une série d'item
typedef struct structCommande {
    itemCommande *listeItem; //un tableau d'items
    int nombreItem;
} structCommande;


#endif // COMMANDE_H
