/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Sortir;
    QAction *actionFrancais;
    QAction *actionEnglish;
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *pushButton3;
    QPushButton *pushButton4;
    QPushButton *pushButton1;
    QPushButton *pushButton9;
    QPushButton *pushButton6;
    QPushButton *pushButton7;
    QPushButton *pushButton8;
    QPushButton *pushButton5;
    QPushButton *pushButton2;
    QPushButton *pushButton0;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButtonAC;
    QPushButton *pushButtonDiv;
    QPushButton *pushButtonMult;
    QPushButton *pushButtonMoins;
    QPushButton *pushButtonPlus;
    QPushButton *pushButtonEgal;
    QTextEdit *ruban;
    QLCDNumber *lcdNumber;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QMenuBar *menubar;
    QMenu *menuFichier;
    QMenu *menuLangues;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(622, 467);
        action_Sortir = new QAction(MainWindow);
        action_Sortir->setObjectName("action_Sortir");
        actionFrancais = new QAction(MainWindow);
        actionFrancais->setObjectName("actionFrancais");
        actionEnglish = new QAction(MainWindow);
        actionEnglish->setObjectName("actionEnglish");
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName("gridLayoutWidget");
        gridLayoutWidget->setGeometry(QRect(30, 150, 261, 271));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName("gridLayout");
        gridLayout->setContentsMargins(0, 0, 0, 0);
        pushButton3 = new QPushButton(gridLayoutWidget);
        pushButton3->setObjectName("pushButton3");

        gridLayout->addWidget(pushButton3, 2, 2, 1, 1);

        pushButton4 = new QPushButton(gridLayoutWidget);
        pushButton4->setObjectName("pushButton4");

        gridLayout->addWidget(pushButton4, 1, 0, 1, 1);

        pushButton1 = new QPushButton(gridLayoutWidget);
        pushButton1->setObjectName("pushButton1");

        gridLayout->addWidget(pushButton1, 2, 0, 1, 1);

        pushButton9 = new QPushButton(gridLayoutWidget);
        pushButton9->setObjectName("pushButton9");

        gridLayout->addWidget(pushButton9, 0, 2, 1, 1);

        pushButton6 = new QPushButton(gridLayoutWidget);
        pushButton6->setObjectName("pushButton6");

        gridLayout->addWidget(pushButton6, 1, 2, 1, 1);

        pushButton7 = new QPushButton(gridLayoutWidget);
        pushButton7->setObjectName("pushButton7");

        gridLayout->addWidget(pushButton7, 0, 0, 1, 1);

        pushButton8 = new QPushButton(gridLayoutWidget);
        pushButton8->setObjectName("pushButton8");

        gridLayout->addWidget(pushButton8, 0, 1, 1, 1);

        pushButton5 = new QPushButton(gridLayoutWidget);
        pushButton5->setObjectName("pushButton5");

        gridLayout->addWidget(pushButton5, 1, 1, 1, 1);

        pushButton2 = new QPushButton(gridLayoutWidget);
        pushButton2->setObjectName("pushButton2");

        gridLayout->addWidget(pushButton2, 2, 1, 1, 1);

        pushButton0 = new QPushButton(gridLayoutWidget);
        pushButton0->setObjectName("pushButton0");

        gridLayout->addWidget(pushButton0, 3, 1, 1, 1);

        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName("verticalLayoutWidget");
        verticalLayoutWidget->setGeometry(QRect(300, 150, 141, 271));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButtonAC = new QPushButton(verticalLayoutWidget);
        pushButtonAC->setObjectName("pushButtonAC");

        verticalLayout->addWidget(pushButtonAC);

        pushButtonDiv = new QPushButton(verticalLayoutWidget);
        pushButtonDiv->setObjectName("pushButtonDiv");

        verticalLayout->addWidget(pushButtonDiv);

        pushButtonMult = new QPushButton(verticalLayoutWidget);
        pushButtonMult->setObjectName("pushButtonMult");

        verticalLayout->addWidget(pushButtonMult);

        pushButtonMoins = new QPushButton(verticalLayoutWidget);
        pushButtonMoins->setObjectName("pushButtonMoins");

        verticalLayout->addWidget(pushButtonMoins);

        pushButtonPlus = new QPushButton(verticalLayoutWidget);
        pushButtonPlus->setObjectName("pushButtonPlus");

        verticalLayout->addWidget(pushButtonPlus);

        pushButtonEgal = new QPushButton(verticalLayoutWidget);
        pushButtonEgal->setObjectName("pushButtonEgal");

        verticalLayout->addWidget(pushButtonEgal);

        ruban = new QTextEdit(centralwidget);
        ruban->setObjectName("ruban");
        ruban->setGeometry(QRect(450, 150, 141, 271));
        lcdNumber = new QLCDNumber(centralwidget);
        lcdNumber->setObjectName("lcdNumber");
        lcdNumber->setGeometry(QRect(30, 70, 251, 71));
        lcdNumber->setSmallDecimalPoint(false);
        lcdNumber->setDigitCount(10);
        lcdNumber->setSegmentStyle(QLCDNumber::Outline);
        lcdNumber->setProperty("intValue", QVariant(0));
        label = new QLabel(centralwidget);
        label->setObjectName("label");
        label->setGeometry(QRect(490, 120, 63, 20));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(330, 120, 63, 20));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(30, 30, 63, 20));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 622, 22));
        menuFichier = new QMenu(menubar);
        menuFichier->setObjectName("menuFichier");
        menuLangues = new QMenu(menubar);
        menuLangues->setObjectName("menuLangues");
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFichier->menuAction());
        menubar->addAction(menuLangues->menuAction());
        menuFichier->addAction(action_Sortir);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        action_Sortir->setText(QCoreApplication::translate("MainWindow", "&Sortir", nullptr));
        actionFrancais->setText(QCoreApplication::translate("MainWindow", "Francais", nullptr));
        actionEnglish->setText(QCoreApplication::translate("MainWindow", "English", nullptr));
        pushButton3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        pushButton4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        pushButton1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        pushButton9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        pushButton6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        pushButton7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        pushButton8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        pushButton5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        pushButton2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        pushButton0->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pushButtonAC->setText(QCoreApplication::translate("MainWindow", "AC", nullptr));
        pushButtonDiv->setText(QCoreApplication::translate("MainWindow", "/", nullptr));
        pushButtonMult->setText(QCoreApplication::translate("MainWindow", "*", nullptr));
        pushButtonMoins->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        pushButtonPlus->setText(QCoreApplication::translate("MainWindow", "+", nullptr));
        pushButtonEgal->setText(QCoreApplication::translate("MainWindow", "=", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Ruban", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Op\303\251rations", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Calculatrice", nullptr));
        menuFichier->setTitle(QCoreApplication::translate("MainWindow", "Fichier", nullptr));
        menuLangues->setTitle(QCoreApplication::translate("MainWindow", "Langues", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
