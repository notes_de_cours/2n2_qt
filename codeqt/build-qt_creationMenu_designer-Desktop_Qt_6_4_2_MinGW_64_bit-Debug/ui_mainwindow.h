/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *nomDuRepasLabel;
    QLineEdit *nomRepas;
    QPushButton *boutonAjoutRepas;
    QFormLayout *formLayout_2;
    QLabel *nomDeLItemLabel;
    QLineEdit *nomItem;
    QLabel *prixDeLItemLabel;
    QLineEdit *prixItem;
    QPushButton *boutonAjoutItem;
    QSpacerItem *verticalSpacer;
    QTableWidget *affichageMenu;
    QPlainTextEdit *plainTextEdit;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QMenuBar *menubar;
    QMenu *menuLangage;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(1179, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName("verticalLayoutWidget");
        verticalLayoutWidget->setGeometry(QRect(70, 70, 251, 411));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        formLayout = new QFormLayout();
        formLayout->setObjectName("formLayout");
        nomDuRepasLabel = new QLabel(verticalLayoutWidget);
        nomDuRepasLabel->setObjectName("nomDuRepasLabel");

        formLayout->setWidget(0, QFormLayout::LabelRole, nomDuRepasLabel);

        nomRepas = new QLineEdit(verticalLayoutWidget);
        nomRepas->setObjectName("nomRepas");

        formLayout->setWidget(0, QFormLayout::FieldRole, nomRepas);


        verticalLayout->addLayout(formLayout);

        boutonAjoutRepas = new QPushButton(verticalLayoutWidget);
        boutonAjoutRepas->setObjectName("boutonAjoutRepas");
        boutonAjoutRepas->setEnabled(false);

        verticalLayout->addWidget(boutonAjoutRepas);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName("formLayout_2");
        nomDeLItemLabel = new QLabel(verticalLayoutWidget);
        nomDeLItemLabel->setObjectName("nomDeLItemLabel");

        formLayout_2->setWidget(0, QFormLayout::LabelRole, nomDeLItemLabel);

        nomItem = new QLineEdit(verticalLayoutWidget);
        nomItem->setObjectName("nomItem");
        nomItem->setEnabled(false);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, nomItem);

        prixDeLItemLabel = new QLabel(verticalLayoutWidget);
        prixDeLItemLabel->setObjectName("prixDeLItemLabel");

        formLayout_2->setWidget(1, QFormLayout::LabelRole, prixDeLItemLabel);

        prixItem = new QLineEdit(verticalLayoutWidget);
        prixItem->setObjectName("prixItem");
        prixItem->setEnabled(false);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, prixItem);


        verticalLayout->addLayout(formLayout_2);

        boutonAjoutItem = new QPushButton(verticalLayoutWidget);
        boutonAjoutItem->setObjectName("boutonAjoutItem");
        boutonAjoutItem->setEnabled(false);

        verticalLayout->addWidget(boutonAjoutItem);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        affichageMenu = new QTableWidget(centralwidget);
        if (affichageMenu->columnCount() < 3)
            affichageMenu->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        affichageMenu->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        affichageMenu->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        affichageMenu->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        affichageMenu->setObjectName("affichageMenu");
        affichageMenu->setGeometry(QRect(380, 70, 361, 411));
        affichageMenu->setContextMenuPolicy(Qt::CustomContextMenu);
        affichageMenu->setSelectionMode(QAbstractItemView::SingleSelection);
        affichageMenu->setColumnCount(3);
        affichageMenu->verticalHeader()->setVisible(true);
        plainTextEdit = new QPlainTextEdit(centralwidget);
        plainTextEdit->setObjectName("plainTextEdit");
        plainTextEdit->setGeometry(QRect(770, 70, 371, 411));
        label = new QLabel(centralwidget);
        label->setObjectName("label");
        label->setGeometry(QRect(780, 40, 49, 16));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(850, 40, 49, 16));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(930, 40, 49, 16));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 1179, 22));
        menuLangage = new QMenu(menubar);
        menuLangage->setObjectName("menuLangage");
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuLangage->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        nomDuRepasLabel->setText(QCoreApplication::translate("MainWindow", "Nom du repas", nullptr));
        boutonAjoutRepas->setText(QCoreApplication::translate("MainWindow", "Ajouter le repas", nullptr));
        nomDeLItemLabel->setText(QCoreApplication::translate("MainWindow", "Nom de l'item", nullptr));
        prixDeLItemLabel->setText(QCoreApplication::translate("MainWindow", "Prix de l'item", nullptr));
        prixItem->setInputMask(QCoreApplication::translate("MainWindow", "09.00", nullptr));
        boutonAjoutItem->setText(QCoreApplication::translate("MainWindow", "Ajouter l'item", nullptr));
        QTableWidgetItem *___qtablewidgetitem = affichageMenu->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("MainWindow", "Repas", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = affichageMenu->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("MainWindow", "Item", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = affichageMenu->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("MainWindow", "Prix", nullptr));
        plainTextEdit->setPlainText(QCoreApplication::translate("MainWindow", "Repas 	Item 	Prix", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Repas", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Item", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Prix", nullptr));
        menuLangage->setTitle(QCoreApplication::translate("MainWindow", "Langage", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
