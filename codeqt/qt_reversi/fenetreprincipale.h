#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H


#include <QMainWindow>

#define TAILLE 50
#define NBR_CELL 8

class QLineEdit;
class QTableWidget;
class QGraphicsScene;
class QSoundEffect;

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT



private slots:
    void quitter();
    void slotJouer();
public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private:

    const QString ETIQUETTE = "ABCDEFGHI";
    int tableau[NBR_CELL][NBR_CELL];
    QLineEdit *choixXY;
    QGraphicsScene *scene;
    int joueur = 1;
    QSoundEffect *effect;
    void dessinerJeu();
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);
    void initialiserJeu();
    int verifierProximiteOccupee(int aCol, int aLigne);
    void inverser(int aCol, int aLigne, int aJoueur);
    int verifierLigne(int aPosX, int aPosY, int aDirX, int aDirY, int aJoueur);
    void inverserLigne(int aPosX, int aPosY, int aDirX, int aDirY, int aJoueur);
    int verifierInversion(int aCol, int aLigne, int aJoueur);
};

#endif // FENETREPRINCIPALE_H
