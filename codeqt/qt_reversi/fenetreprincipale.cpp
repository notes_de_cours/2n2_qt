#include "fenetreprincipale.h"
#include <QApplication>
#include <QMenuBar>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QFormLayout>
#include <QGraphicsPixmapItem>
#include <QSoundEffect>   //ajouter QT += multimedia  dans .pro

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent) {
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier"));
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::quitter);

    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *bouton = new QPushButton("Jouer");
    grille->addWidget(bouton);
    connect(bouton, &QPushButton::clicked, this, &FenetrePrincipale::slotJouer);

    bouton = new QPushButton("Passer son tour");
    grille->addWidget(bouton);
    bouton = new QPushButton("Terminer la partie");
    grille->addWidget(bouton);

    setCentralWidget(fenetre);
    effect = new QSoundEffect;
    effect->setSource(QUrl::fromLocalFile(":/pieces/sons/game-show-buzz.wav"));
    effect->setLoopCount(1);
    effect->setVolume(0.5f);
}

FenetrePrincipale::~FenetrePrincipale() {}

void FenetrePrincipale::initialiserJeu()
{
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }

    // Les 4 pièces de base
    tableau[3][3]=2;
    tableau[3][4]=1;
    tableau[4][3]=1;
    tableau[4][4]=2;
    dessinerJeu();
}
void FenetrePrincipale::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            rectItem->setBrush(Qt::green);
            scene->addItem(rectItem);
            if(tableau[i][j] != 0)
            {
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}

void FenetrePrincipale::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 20, 20);
    if(aJoueurId==1) {
       rond->setBrush(Qt::black);
    } else {
       rond->setBrush(Qt::white);
    }
    scene->addItem(rond);
};


/**
 * @brief FenetrePrincipale::verifierProximiteOccupee
 * Vérifie si la pièce serait déposée à côté d'une pièce déjà présente.
 * @param aCol
 * @param aLigne
 * @return
 */
int FenetrePrincipale::verifierProximiteOccupee(int aCol, int aLigne)
{
    int caseOccupee = 0;
    // Vérifie dans les 8 cases connexes.
    for(int i=-1; i<=1; i++) {
        for(int j=-1; j<=1; j++) {
            if(!(i==0 && j==0)
                    && aCol+i >= 0
                    && aCol+i < NBR_CELL
                    && aLigne+j >= 0
                    && aLigne+j < NBR_CELL
                    ) {
                if(tableau[aCol+i][aLigne+j] != 0) {
                    caseOccupee = 1;
                }
            }
        }
    }
    return caseOccupee;
}

/**
 * @brief FenetrePrincipale::verifierLigne
 * Vérification si le joueur à une autre pièce dans la direction aDirX,aDirY
 * Cette direction a été prise parce qu'il y avait une pièce de l'adversaire
 * dans celle-ci à partir de la pièce déposée en aPosX, aPosY
 * @param aPosX
 * @param aPosY
 * @param aDirX
 * @param aDirY
 * @param aJoueur
 * @return
 */
int FenetrePrincipale::verifierLigne(int aPosX, int aPosY, int aDirX, int aDirY, int aJoueur)
{
    int doitInverser = 0;
    int lPosX = aPosX+aDirX;
    int lPosY = aPosY+aDirY;
    while(!doitInverser
          && lPosX >= 0 && lPosX < NBR_CELL // vérification pour ne pas sortir du jeu
          && lPosY >= 0 && lPosY < NBR_CELL
          && tableau[lPosX][lPosY] != 0  // arrête si la case est vide
          ){
          if(tableau[lPosX][lPosY]==aJoueur) { // rencontre d'une pièce du joueur après une pièce de l'adversaire
              doitInverser = 1;
          } else {
              lPosX += aDirX;
              lPosY += aDirY;
          }
    }
    return doitInverser;
}

/**
 * @brief FenetrePrincipale::inverserLigne
 * Inverse les pièces de l'adversaire dans la direction aDirX, aDirY jusqu'à la prochaine pièce du joueur
 * @param aPosX
 * @param aPosY
 * @param aDirX
 * @param aDirY
 * @param aJoueur
 */
void FenetrePrincipale::inverserLigne(int aPosX, int aPosY, int aDirX, int aDirY, int aJoueur)
{
    int continuer = 1;
    int lPosX = aPosX+aDirX;
    int lPosY = aPosY+aDirY;
    while(continuer
          && lPosX >= 0 && lPosX < NBR_CELL
          && lPosY >= 0 && lPosY < NBR_CELL
          ){
          if(tableau[lPosX][lPosY]==aJoueur) { // la première pièce du joueur, arrête la recherche
              continuer = 0;
          } else {
              tableau[lPosX][lPosY] = aJoueur; // inverse la pièce
              lPosX += aDirX;
              lPosY += aDirY;
          }
    }
}

/**
 * @brief FenetrePrincipale::inverser
 * Inverse les pièces de l'adversaire
 *
 * @param aCol
 * @param aLigne
 * @param aJoueur
 */
void FenetrePrincipale::inverser(int aCol, int aLigne, int aJoueur)
{
    int doitInverser = 0;

    // boucle dans les 9 directions autour de la pièce déposée
    for(int i=-1; i<=1; i++) {
        for(int j=-1; j<=1; j++) {
            if(!(i==0 && j==0) // si ce n'est pas directement sur la pièce déposée
                    && aCol+i >= 0 // et que le déplacement dans cette direction ne sort pas du jeu
                    && aCol+i < NBR_CELL
                    && aLigne+j >= 0
                    && aLigne+j < NBR_CELL
                    ) {
                if(tableau[aCol+i][aLigne+j] != aJoueur) { // une piece potentiellement à inverser
                    doitInverser = verifierLigne(aCol, aLigne, i,j, aJoueur);
                    if(doitInverser) {
                        inverserLigne(aCol,aLigne,i,j,aJoueur);
                        dessinerJeu();
                    }
                }
            }
        }
    }
}

/**
 * @brief FenetrePrincipale::verifierInversion
 * Vérifie si le fait de déposer un pièce à la position aCol, aLigne fera en sorte
 * qu'au moins une pièce de l'adversaire sera inversée
 * @param aCol
 * @param aLigne
 * @param aJoueur
 * @return vrai si ca cause une inversion, faux sinon.
 */
int FenetrePrincipale::verifierInversion(int aCol, int aLigne, int aJoueur)
{
    int doitInverser = 0;
    int peuInverser = 0;
    for(int i=-1; i<=1; i++) {
        for(int j=-1; j<=1; j++) {
            if(!(i==0 && j==0)
                    && aCol+i >= 0
                    && aCol+i < NBR_CELL
                    && aLigne+j >= 0
                    && aLigne+j < NBR_CELL
                    && tableau[aCol+i][aLigne+j] != 0
                    ) {
                if(tableau[aCol+i][aLigne+j] != aJoueur) { // une piece potentiellement à inverser
                    doitInverser = verifierLigne(aCol, aLigne, i,j, aJoueur);
                    if(doitInverser) {
                        peuInverser = 1;  // pas efficace car ca devrait sortir dès que vrai. Faudrait changer les for en while
                    }
                }
            }
        }
    }
    return peuInverser;
}
// slots

void FenetrePrincipale::quitter() { QApplication::quit(); }

void FenetrePrincipale::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;


        if(         col>=0 && col < NBR_CELL // si la colonne est bonne
                && ligne >=0 && ligne < NBR_CELL // et que la ligne est bonne
                && tableau[col][ligne]==0 // et que la case est libre
                && verifierProximiteOccupee(col,ligne)  // et qu'il y a une pièce adjacente
                && verifierInversion(col, ligne, joueur) // et que ca causera une inversion.
                ) {
            tableau[col][ligne] = joueur;
            dessinerPiece(col, ligne, joueur);
            inverser(col, ligne, joueur);
            joueur =  ((joueur)%2)+1; // passe au joueur suivant
        } else {

            effect->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}
