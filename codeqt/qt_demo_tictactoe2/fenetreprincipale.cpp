#include "fenetreprincipale.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

#include <QSoundEffect>

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotJouer);

    // 10
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotVerifierGagnant);

    setCentralWidget(fenetre);

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/game-show-buzz.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);
}

FenetrePrincipale::~FenetrePrincipale()
{
}

void FenetrePrincipale::initialiserJeu()
{
    // 7
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }
    // 12

    dessinerJeu();
}
void FenetrePrincipale::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);

        //dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(tableau[i][j] != 0)
            {
                // 8
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}

void FenetrePrincipale::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    /*
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 20, 20);
    if(aJoueurId==1) {
       rond->setBrush(Qt::black);
    } else {
       rond->setBrush(Qt::white);
    }

    scene->addItem(rond);
    */
    QGraphicsPixmapItem *item;
    if(aJoueurId==1) {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/rond.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/x.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);
};




// Slots

void FenetrePrincipale::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
//8
        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();

            // 11
            derniereCol = col;
            derniereLigne = ligne;

            joueur =  ((joueur)%2)+1;
        } else {
            // 5
            buzzer->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}


// 12
void FenetrePrincipale::slotVerifierGagnant() {
    int gagnant = 0;
    int couleur = tableau[derniereCol][derniereLigne];
    int compteurLigne = 0;
    int compteurCol = 0;
    int compteurDiag1 =0;
    int compteurDiag2 = 0;
    if(couleur != 0)  {
        for(int i=0; i<NBR_CELL; i++) {
            // verifie la colonne
            if(tableau[derniereCol][i]==couleur) {
                compteurCol++;
            }
            //verifie la ligne
            if(tableau[i][derniereLigne] == couleur) {
                compteurLigne++;
            }

        }

        // verifie si sur une diagonale
        // soit que col==ligne, soit que col+ligne = NBR_CELL-1
        if(derniereLigne==derniereCol || derniereLigne+derniereCol == NBR_CELL-1) {
            for(int i=0; i<NBR_CELL; i++) {
                if(tableau[i][i] == couleur){
                    compteurDiag1++;
                }
                if(tableau[(NBR_CELL-1) -i][i] == couleur) {
                    compteurDiag2++;
                }
            }
        }

        gagnant = (compteurCol == NBR_CELL) || (compteurLigne == NBR_CELL)
                ||(compteurDiag1 == NBR_CELL) || (compteurDiag2 == NBR_CELL);

        if(gagnant) {
            QMessageBox msgBox;
            msgBox.setText("Nous avons un gagnant!!");
            msgBox.exec();

            scene->clear();
            joueur = 1;
            initialiserJeu();
        }
    }
}
