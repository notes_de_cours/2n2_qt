#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
#define TAILLE 50
#define NBR_CELL 3

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();

private slots:
    void slotJouer();
    void slotVerifierGagnant();
private:
    int joueur = 1;
    int tableau[NBR_CELL][NBR_CELL];

    const QString ETIQUETTE = "ABCDEFGHI";
    QLineEdit *choixXY;
    QGraphicsScene *scene;

    QSoundEffect *buzzer;

    //11
    int derniereCol = 0;
    int derniereLigne = 0;
    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);

    int verifierGagnant(int aCol, int aLigne);
};
#endif // FENETREPRINCIPALE_H
