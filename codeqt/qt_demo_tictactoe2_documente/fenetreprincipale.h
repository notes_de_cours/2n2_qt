#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
#define TAILLE 50
#define NBR_CELL 3

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();

private slots:
    void slotJouer();
    void slotVerifierGagnant();
private:
    int joueur = 1; /**< l'id du joueur actif */
    int tableau[NBR_CELL][NBR_CELL]; /**< les valeurs du tableau du jeu.

                         - Contient **0,1 ou 2**.
                         - 0 indique une case vide.
                         - 1 ou 2 indiquant quel joueur occupe cette case
        */

    const QString ETIQUETTE = "ABCDEFGHI"; /**< étiquette des coordonnées */

    QLineEdit *choixXY;     /**< le champ pour entrer les coordonnées où jouer */
    QGraphicsScene *scene;  /**< la scène où est dessiné le jeu */
    QSoundEffect *buzzer;   /**< le son quand le joueur entre une mauvaise coordonnée */

    //11
    int derniereCol = 0;   /**< la dernière colonne entrée, utilisée pour déterminer le gagnant */
    int derniereLigne = 0; /**< la dernière ligne entrée, utilisée pour déterminer le gagnant */


    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);

    int verifierGagnant(int aCol, int aLigne);
};
#endif // FENETREPRINCIPALE_H
