#include "fenetreprincipale.h"

#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

#include <QSoundEffect>

/*!
 * \brief FenetrePrincipale::FenetrePrincipale
 * Construction du tableau de jeu.
 * \param parent le widget dans lequel insérer cette fenêtre.
 *
 * Le tableau consiste en une grille de jeu, d'un champ de texte
 * pour faire l'entrée de la coordonnée où insérer le O ou le X, et
 * d'un bouton pour jouer le coup.
 */
FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    // 1
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    // 3
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotJouer);

    // 10
    connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotVerifierGagnant);

    setCentralWidget(fenetre);

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/game-show-buzz.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/*!
 * \brief FenetrePrincipale::initialiserJeu
 *          mise à 0 de toutes les cellules du jeu
 */
void FenetrePrincipale::initialiserJeu()
{
    // 7
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }
    // 12

    dessinerJeu();
}

/*!
 * \brief FenetrePrincipale::dessinerJeu
 *  Affiche le système de coordonnées autour des cases
 *  ainsi que les cases du jeu.
 *
 *  Le tableau a NBR_CELL x NBR_CELL, et la taille des
 *  carrés est définie par TAILLE
 *
 *  Les coordonnées des colonnes sont déterminées par ETIQUETTE.
 *  Les coordonnées des lignes sont numériques et partent à 1.
 */
void FenetrePrincipale::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));
        scene->addItem(txt);

        // dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));
            if((i+j)%2 == 1) {
                rectItem->setBrush(Qt::gray);
            } else {
                rectItem->setBrush(Qt::white);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(tableau[i][j] != 0)
            {
                // 8
                dessinerPiece(i,j,tableau[i][j]);
            }
        }
    }
}

/*!
 * \brief FenetrePrincipale::dessinerPiece
 * Affichage du X ou du O
 * \param aCol[in], aLigne[in] les coordonnées où insérer la pièce
 * \param aJoueurId[in] L'id du joueur: 1=O, 2=X (0 est réservé pour les cases vides)
 */
void FenetrePrincipale::dessinerPiece(int aCol, int aLigne, int aJoueurId) {
    QGraphicsPixmapItem *item;
    if(aJoueurId==1) {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/rond.png"));
    } else {
        item = new QGraphicsPixmapItem(QPixmap(":/res/images/x.png"));
    }
    item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
    item->setScale(0.25);
    scene->addItem(item);
};


// Slots

/*!
 * \brief FenetrePrincipale::slotJouer
 *
 *  **mauvais** : connectée au bouton jouer
 *  (ce n'est pas ce que ca fait, et ca pourrait être appelé autrement)
 *
 *  *bon* : Place un X ou un O sur le jeu.
 *
 *  **mauvais**: Les coordonnées sont dans le QLineEdit choixXY
 *  (ce n'est pas assurement un QLineEdit, et on ne sait pas comment c'est stocké)
 *
 *  *bon* : La colonne est dans choixXY[0], et la ligne dans choixXY[1]
 *
 *  La colonne doit être dans ETIQUETTE.
 *
 *  La ligne doit être un numérique entre 1 et NBR_CELL inclusivement.
 *
 *  Si les coordonnées sont mauvaises, un buzzer se fera entendre.
 */
void FenetrePrincipale::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];

        // conversion en index numérique de la lettre représentant la colonne
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
//8
        // vérification de la validité des coordonnées
        if(tableau[col][ligne]==0
                && col>=0 && col < NBR_CELL
                && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();

            // 11
            //conserve les coordonnées afin de savoir où s'est fait le
            // dernier coup afin de déterminer s'il y a un gagnant.
            // FIXME: comment séparer slotJouer et slotVerifierGagnant?
            //        ces 2 variables ne devraient pas être nécessaires.
            derniereCol = col;
            derniereLigne = ligne;

            // calcul de l'id du prochain joueur: 1 ou 2
            joueur =  ((joueur)%2)+1;
        } else { // mauvaise coordonnées
            // 5
            buzzer->play();
        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}


// 12
/*!
 * \brief FenetrePrincipale::slotVerifierGagnant
 *
 * Détermine s'il y a un gagnant suite au dernier coup.
 *
 * Pour gagner, il faut avoir soit une ligne horizontale, verticale,
 * ou une grande diagonale pleine.
 *
 * Si un gagnant est détecté, un popup apparaitra. Le jeu est par la
 * suite effacé, et la partie peut recommencer.
 *
 * Si aucun gagnant, rien ne se passe.
 *
 *  **mauvais** : dernierCol et derniereLigne sont assignées dans slotJouer
 *   ( C'est une considération interne qui ne doit pas être dans la documentation
 *   externe)
 */
void FenetrePrincipale::slotVerifierGagnant() {
    int gagnant = 0;

    //l'id du joueur qui a joué le dernier coup
    //TODO: changer couleur pour joueurId (afin de clarifier le code)
    int couleur = tableau[derniereCol][derniereLigne];

    //compteurs du nombre de cases sur une même ligne/colonne/diagonale
    //appartenant au joueur ayant fait le dernier coup
    //Si un de ces compteurs atteint NBR_CELL, nous avons un gagnant
    int compteurLigne = 0;
    int compteurCol = 0;
    int compteurDiag1 =0;
    int compteurDiag2 = 0;

    //l'id du joueur peut être 0 si de mauvaises coordonnées sont entrées au premier coup.
    if(couleur != 0)  {
        for(int i=0; i<NBR_CELL; i++) {
            // verifie la colonne dans laquelle le dernier coup a été fait
            if(tableau[derniereCol][i]==couleur) {
                compteurCol++;
            }
            //verifie la ligne
            if(tableau[i][derniereLigne] == couleur) {
                compteurLigne++;
            }

        }

        // verifie si le dernier coup est sur une diagonale
        // soit que col==ligne, soit que col+ligne = NBR_CELL-1
        // FIXME ce genre de commentaire demande une clarification mathématique
        //   pourquoi col+ligne = NBR_CELL-1 indique que c'est sur une diag ?
        if(derniereLigne==derniereCol || derniereLigne+derniereCol == NBR_CELL-1) {
            for(int i=0; i<NBR_CELL; i++) {
                if(tableau[i][i] == couleur){
                    compteurDiag1++;
                }
                if(tableau[(NBR_CELL-1) -i][i] == couleur) {
                    compteurDiag2++;
                }
            }
        }

        gagnant = (compteurCol == NBR_CELL) || (compteurLigne == NBR_CELL)
                ||(compteurDiag1 == NBR_CELL) || (compteurDiag2 == NBR_CELL);

        if(gagnant) {
            QMessageBox msgBox;
            msgBox.setText("Nous avons un gagnant!!");
            msgBox.exec();

            scene->clear();
            joueur = 1;
            initialiserJeu();
        }
    }
}
