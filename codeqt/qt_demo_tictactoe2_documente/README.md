# Jeu de tictactoe 

Un jeu de tictactoe développé dans le cadre du cours 420-2n2-DM Programmation Native  1

![ecranPrincipal](./images/CaptureEcran1.png)

## Comment jouer

Ce jeu se joue à 2. 
Le but est de construire une ligne complète horizontale, verticale, 
ou sur les grandes diagonales.

Au début d'une partie, le tableau de jeu sera vide. 

Le premier joueur sélectionne une coordonnée en entrant l'identificateur 
de la colonne (une lettre) suivit de l'identificateur de la ligne (un nombre). 

Le joueur appuie ensuite sur le bouton Jouer au bas de
