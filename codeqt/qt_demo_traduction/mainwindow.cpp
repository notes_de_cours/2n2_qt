#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QActionGroup>
#include <QDir>
#include <QTranslator>


void MainWindow::changerLangue(QString aLangueEtPays) {
    if (m_langCourrant != aLangueEtPays) {
        QTranslator* lTraducteur = new QTranslator(this);
        qApp->removeTranslator(lTraducteur);
        QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
        if(lTraducteur->load(lLangue)) {
            qApp->installTranslator(lTraducteur);
            ui->retranslateUi(this);
            m_langCourrant = aLangueEtPays;
        } else {
            qDebug() << "Fichier de langue non trouvé";
        }
    }
}
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_langPath = ":/traductions/langue";
    changerLangue(QString("fr_ca"));
    creerMenuLangue();

    texte1 = new QLabel( this);
    texte1->setText(tr("allo"));
    texte1->setGeometry(10,150,50,30);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::creerMenuLangue() {

  QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangue);
  lGroupeLangue->setExclusive(true);

  connect(lGroupeLangue, &QActionGroup::triggered, this,
          &MainWindow::on_menu_action_triggered);

  // Va chercher le langage par défaut sur cet ordi
  QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
  localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

  // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

  QDir dir(m_langPath);
  QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

  for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
    // extraction du langage à partir du nom de fichier
    QString localeLangue;
    QString localeLangueEtPays;
    localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
    localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
    localeLangueEtPays = localeLangue;
    localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
    localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
    localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

    QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
    nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

    QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
    QAction *action = new QAction(ico, nomDeLangue, this);

    action->setCheckable(true);
    action->setData(localeLangueEtPays);

    ui->menuLangue->addAction(action);
    lGroupeLangue->addAction(action);

    // Sélectionne la langue du système par défaut dans le menu
    if (localeSysteme == localeLangue) {
      action->setChecked(true);
    }
  }
}



void MainWindow::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange) {
        texte1->setText(tr("allo"));
    }
    QWidget::changeEvent(event);
}
// slots

void MainWindow::on_menu_action_triggered(QAction *action)
{
    if (0 != action) {
      // Charge le langage selon le data de l'action
      changerLangue(action->data().toString());
    }
}

