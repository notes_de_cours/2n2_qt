#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE

class QLabel;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void changeEvent(QEvent *event);
private slots:
    void on_actionFrancais_triggered();

    void on_actionAnglais_triggered();

    void on_menu_action_triggered(QAction *action);
private:
    Ui::MainWindow *ui;
    QString m_langPath ; // le path pour les fichiers de langue
    QString m_langCourrant; // le nom du langage présentement chargé

    QLabel *texte1;
    void changerLangue(QString nomLangue);
    void creerMenuLangue();
};
#endif // MAINWINDOW_H
