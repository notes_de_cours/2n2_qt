<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_CA">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Fenêtre Principale</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="27"/>
        <source>Bouton 1</source>
        <translation>Le bouton #1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="40"/>
        <source>Label 1</source>
        <translation>Etiquette 1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="55"/>
        <source>Langue</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="63"/>
        <source>Francais</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="66"/>
        <source>Interface en Francais</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="71"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>English interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="32"/>
        <source>allo</source>
        <translation>salut toé</translation>
    </message>
</context>
</TS>
