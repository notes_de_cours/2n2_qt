<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Main Window</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="27"/>
        <source>Bouton 1</source>
        <translation>The number one button</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="40"/>
        <source>Label 1</source>
        <translation>The first label</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="55"/>
        <source>Langue</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="63"/>
        <source>Francais</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="66"/>
        <source>Interface en Francais</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="71"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>English interface</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="32"/>
        <source>allo</source>
        <translation>hello</translation>
    </message>
</context>
</TS>
