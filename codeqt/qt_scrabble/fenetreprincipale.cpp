#include "fenetreprincipale.h"

#include <QComboBox>
#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

#include <QSoundEffect>


FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);

    QHBoxLayout *sectionPointage = new QHBoxLayout;
    for(int i =0; i<NBR_MAX_JOUEUR; i++) {
        affichagePointage[i]= new QLineEdit;
        affichagePointage[i]->setEnabled(false);
        QFormLayout *pointForm = new QFormLayout;
        pointForm->addRow(QString::number(i+1), affichagePointage[i]);
        sectionPointage->addLayout(pointForm);
    }
    grille->addLayout(sectionPointage);

    scene = new QGraphicsScene;
    QGraphicsView *view = new QGraphicsView(scene);
    grille->addWidget(view);

    affichageLettre = new QLineEdit;
    affichageLettre->setEnabled(false);
    grille->addWidget(affichageLettre);

    mot = new QLineEdit;
    mot->setFocus();
    mot->setInputMask("xxxxxxx");
    QFormLayout *motForm = new QFormLayout;
    motForm->addRow("mot", mot);
    grille->addLayout(motForm);
    mot->setCursorPosition(1);

    choixColonne = new QComboBox;
    for(int i= 0; i< NBR_CELL; i++) {
        choixColonne->addItem(ETIQUETTE[i]);
    }
    choixColonne->setCurrentIndex(7);
    grille->addWidget(choixColonne);

    choixLigne = new QComboBox;
    for(int i= 0; i< NBR_CELL; i++) {
        choixLigne->addItem(QString::number(i+1));
    }
    choixLigne->setCurrentIndex(7);
    grille->addWidget(choixLigne);

    QHBoxLayout *boiteJouer = new QHBoxLayout;
    QPushButton *boutonJouerH = new QPushButton("Jouer Horizontal");
    boiteJouer->addWidget(boutonJouerH);
    connect(boutonJouerH, &QPushButton::clicked, this, &FenetrePrincipale::slotJouerH);
    QPushButton *boutonJouerV = new QPushButton("Jouer Vertical");
    boiteJouer->addWidget(boutonJouerV);
    connect(boutonJouerV, &QPushButton::clicked, this, &FenetrePrincipale::slotJouerV);

    grille->addLayout(boiteJouer);


    //connect(boutonJouer, &QPushButton::clicked, this, &FenetrePrincipale::slotVerifierGagnant);

    setCentralWidget(fenetre);

    initialiserJeu();

    buzzer = new QSoundEffect;
    buzzer->setSource(QUrl::fromLocalFile(":/res/sons/game-show-buzz.wav"));
    buzzer->setLoopCount(1);
    buzzer->setVolume(0.5f);
}

FenetrePrincipale::~FenetrePrincipale()
{
}

void FenetrePrincipale::initialiserJeu()
{
    premierCoup = true;
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            lettresAffichees[i][j] = ' ';
        }
    }

    for(int i=0; i< NBR_MAX_JOUEUR;i++) {
        pointageJoueur[i] = 0;
    }

    // genère les mots pour les joueurs
    std::srand(std::time(nullptr));
    for(int i=0; i< nombreDeJoueur;i++) {

        for(int j=0; j<NBR_TUILE_JOUEUR; j++) {
            lettresJoueur[i] += lettresValides[(rand() % lettresValides.length())];
        }
        qDebug() << lettresJoueur[i];
    }

    dessinerJeu();

}
void FenetrePrincipale::dessinerJeu() {

    affichageLettre->setText(lettresJoueur[joueurActif]);

    scene->clear();

    for(int ligne =0; ligne<NBR_CELL; ligne++) {
        // 2
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[ligne]);
        txt->setPos((ligne+1)*(TAILLE)+TAILLE/2, 20);
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(ligne+1));
        txt->setPos(20, (ligne+1.5)*(TAILLE));
        scene->addItem(txt);

        //dessine les cases
        for(int col=0; col<NBR_CELL; col++) {
            QGraphicsRectItem *rectItem =
                    new QGraphicsRectItem(QRectF((ligne+1)*TAILLE, (col+1)*TAILLE, TAILLE, TAILLE));
            QString bonus = FOND[ligne][col];
            if(bonus  == "T") {
                rectItem->setBrush(Qt::red);
            } else if(bonus  == "t") {
                    rectItem->setBrush(Qt::darkBlue);
            } else if(bonus  == "D") {
                rectItem->setBrush(Qt::magenta);
            } else if(bonus  == "d") {
                rectItem->setBrush(Qt::cyan);
            } else if(bonus  == "c") {
                rectItem->setBrush(Qt::magenta);
            }
            scene->addItem(rectItem);
            // 6
            //dessine les pieces
            if(lettresAffichees[ligne][col] != ' ')
            {
                // 8
                dessinerPiece(ligne,col,lettresAffichees[ligne][col]);
            }
        }
    }
}

void FenetrePrincipale::dessinerPiece(int aCol, int aLigne, char aLettre) {


    if(aLettre == '_') {

        QGraphicsRectItem *item = new QGraphicsRectItem((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE, 30, 30);
        item->setBrush(Qt::gray);
        scene->addItem(item);

    }  else if(aLettre == 'a' || aLettre == 'b' ){
        QGraphicsPixmapItem *item;
        QString nomFichier = ":res/images/"+QString(QChar::fromLatin1( aLettre).toLower())+".png";
        item = new QGraphicsPixmapItem(QPixmap(nomFichier));
        item->setScale(0.5);
        item->setPos((aCol+1.25)*TAILLE, (aLigne+1.25)*TAILLE);
        scene->addItem(item);
    } else {

        QGraphicsTextItem *lLettreDessin = scene->addText(QString(QChar::fromLatin1(aLettre).toUpper()));
        lLettreDessin->setPos((aCol+1)*TAILLE, (aLigne+1)*TAILLE);
        lLettreDessin->setScale(2);
        int posLettre = valeurLettres[lettresValides.indexOf(aLettre)];
        QGraphicsTextItem *lValeurDessin = scene->addText(QString(QString::number(posLettre)));
        lValeurDessin->setPos((aCol+1.5)*TAILLE, (aLigne+1.5)*TAILLE);

    }

};

int FenetrePrincipale::trouverDebutMot(int aCol, int aLigne, int aDeltaX, int aDeltaY)
{
    int offsetDuMot =1; // part à 1 car je veux vérifier la case juste avant

    while ( (aCol - (offsetDuMot*aDeltaX) >= 0) && (aLigne -(offsetDuMot*aDeltaY) >=0)
            && lettresAffichees[aCol - (offsetDuMot*aDeltaX)][aLigne -(offsetDuMot*aDeltaY)] != ' ' ) {
        offsetDuMot++;
    }

    return offsetDuMot-1;
}


int FenetrePrincipale::calculerValeurTuile(char aLettre)
{
    int indexLettre = lettresValides.indexOf(aLettre);
    return valeurLettres[indexLettre];
}

bool FenetrePrincipale::verifierSiMotEntre(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot)
{
    int posMot = 0;
    while (posMot < aMot.length() && aCol < NBR_CELL && aLigne < NBR_CELL)
    {
        if(lettresAffichees[aCol][aLigne] == ' ') {
            // avance dans le mot et dans le tableau
            posMot++;
        }
        aCol+= aDeltaX;
        aLigne+= aDeltaY;
    }

    // si toutes les lettres ont une place avant d'arriver à la bordure: retourne VRAI
    return (posMot == aMot.length());
}

bool FenetrePrincipale::verifierSiMotVoisin(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot)
{
    int posMot  = 0;
    bool voisinTrouver = false;
    while(!voisinTrouver && posMot<aMot.length())
    {
        int n = fmax(0, aLigne-1+(posMot*aDeltaY));
        int s = fmin(NBR_CELL, aLigne+1+(posMot*aDeltaY));
        int o = fmax(0,aCol-1+(posMot*aDeltaX));
        int e = fmin(NBR_CELL, aCol+1+(posMot*aDeltaX));
        voisinTrouver = lettresAffichees[aCol][n] != ' ' ||
                        lettresAffichees[aCol][s] != ' ' ||
                        lettresAffichees[aCol][aLigne] != ' ' ||
                        lettresAffichees[o][aLigne] != ' ' ||
                        lettresAffichees[e][aLigne] != ' ';
        posMot++;
    }

    return voisinTrouver;
}

bool FenetrePrincipale::verifierSiAuCentre(int x, int y, int deltaX, int deltaY, int longueur)
{
    int centre = NBR_CELL/2;
    return ((y == centre && deltaX == 1 && x<=centre && x+longueur >=centre) ||
            (x == centre && deltaY == 1 && y<=centre && y+longueur >=centre));
}

bool FenetrePrincipale::verifierLettresValides(QString aMot)
{
    bool toutesLettresTrouvees = true;
    QString sac = lettresJoueur[joueurActif];
    for(int i=0;i<aMot.length(); i++) {
        int index = sac.indexOf(aMot[i]);
        if(index != -1) {
            sac = sac.remove(index, 1);
        } else {
            toutesLettresTrouvees = false;
        }
    }
    return toutesLettresTrouvees;
}
int FenetrePrincipale::valeurMultiplicateurMot(int x, int y)
{
    QString multiplicateur = FOND[x][y];

    if(multiplicateur == "T") {
        return 3;
    } else if(multiplicateur == "D") {
        return 2;
    } else {
        return 1;
    }
}
int FenetrePrincipale::valeurMultiplicateurTuile(int x, int y)
{
    QString multiplicateur = FOND[x][y];

    if(multiplicateur == "t") {
        return 3;
    } else if(multiplicateur == "d") {
        return 2;
    } else {
        return 1;
    }
}

int FenetrePrincipale::calculerPointageMot2(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot, bool recherchePerpendiculaire)
{
    int pointage = 0;
    int posX, posY;
    int pointagePrefix =0 , pointageSuffix =0;
    int offset = trouverDebutMot(aCol,aLigne,aDeltaX, aDeltaY);
    int pointagePerpendiculaire = 0;
    int multiplicateurMot = 1;
    for(int i=0 ; i< aMot.length(); i++) {
        pointagePrefix = 0;
        //calcule le pointage du prefix et des entres lettres
        while(lettresAffichees[aCol -(offset*aDeltaX)][aLigne-(offset*aDeltaY)] != ' ') {
            pointagePrefix += calculerValeurTuile(lettresAffichees[aCol -(offset*aDeltaX)][aLigne-(offset*aDeltaY)]);
            offset--;
        }
        posX = aCol -(offset*aDeltaX);
        posY = aLigne-(offset*aDeltaY);

        //calcule le pointage de la lettre ajoutée

        // 1 triple = 3; 2 triples = 9, 3 triples = 27
        multiplicateurMot *= valeurMultiplicateurMot(posX, posY);
        if(recherchePerpendiculaire) {
            pointagePerpendiculaire +=
                    calculerPointageMot2(posX, posY,aDeltaY, aDeltaX, aMot[i],false); //notez l'inversion de deltaY et deltaX
            // tout est calculé, on peut ajouter la lettre sur le tableau
            lettresAffichees[posX][posY]= aMot[i].toLatin1();
        }
        pointage +=  pointagePrefix + calculerValeurTuile(aMot[i].toLatin1())*valeurMultiplicateurTuile(posX,posY);
        offset--;
    }
    //calcul le pointage du suffixe
    posX = aCol -(offset*aDeltaX);
    posY = aLigne-(offset*aDeltaY);
    while(posX < NBR_CELL && posY < NBR_CELL && lettresAffichees[posX][posY] != ' ') {
        pointageSuffix += calculerValeurTuile(lettresAffichees[posX][posY]);
        offset--;
        posX = aCol-(offset*aDeltaX);
        posY = aLigne-(offset*aDeltaY);
    }
    if(pointagePrefix == 0 && pointageSuffix == 0 && aMot.length() == 1) {
        //il n'y a pas de prefix ni de suffix, et c'est le calcul des perpendiculaire,
        // ce qui veut dire que c'est une seule lettre, et il n'y a pas de mot perpendiculaire
        pointage = 0; // on ne doit pas recalculer cette lettre.
    }
    pointage = (pointage + pointageSuffix)*multiplicateurMot + pointagePerpendiculaire;
    //qDebug() << pointage << " " << recherchePerpendiculaire;
    return pointage;

}

void FenetrePrincipale::jouer(int lDirection) {

    int lCol = choixColonne->currentIndex();
    int lLigne = choixLigne->currentIndex();
    int deltaX =0, deltaY = 0;

    QString lMot = mot->text().trimmed();
    int pointage =0;
    if(lDirection == HORIZONTAL) {
        deltaX = 1;
    } else {
        deltaY = 1;
    }


    if(verifierSiMotEntre(lCol, lLigne, deltaX, deltaY, lMot)
            && (  (premierCoup && verifierSiAuCentre(lCol, lLigne, deltaX, deltaY, lMot.length()))
                || verifierSiMotVoisin(lCol, lLigne, deltaX, deltaY, lMot))
            && verifierLettresValides(lMot)

       ) {
        pointage = calculerPointageMot2(lCol, lLigne, deltaX, deltaY, lMot, true);
        // reste le 50 points si 7 lettres
        pointageJoueur[joueurActif] += pointage;
        for(int i=0; i<NBR_MAX_JOUEUR;i++ ){
            affichagePointage[i]->setText(QString::number(pointageJoueur[i]));
        }
        joueurActif =  ((joueurActif+1)%nombreDeJoueur);
        dessinerJeu();
        premierCoup = false;

    } else {
        buzzer->play();
    }



}

// Slots

void FenetrePrincipale::slotJouerH()
{
    jouer(HORIZONTAL);
}

void FenetrePrincipale::slotJouerV()
{
    jouer(VERTICAL);
}

void FenetrePrincipale::slotVerifierGagnant() {

}





