#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>

class QSoundEffect;
class QGraphicsScene;
class QLineEdit;
class QComboBox;

#define TAILLE 50
#define NBR_CELL 15
#define NBR_MAX_JOUEUR 4
#define NBR_TUILE_JOUEUR 7

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();

private slots:
    void slotJouerH();
    void slotJouerV();
    void slotVerifierGagnant();
private:
    const int  HORIZONTAL =  0;
    const int  VERTICAL = 1;

    QString lettresValides = "abcdefghijklmnopqrstuvwxyz_";

    //                      a b c d e  f g h i j k  l m n o p q r s t u v w  x  y  z   vide
    int valeurLettres[27] = {1,3,3,2,1 ,4,2,4,1,8,10,1,2,1,1,3,8,1,1,1,1,4,10,10,10,10, 0};
    int nombreLettres[27] = {9,2,2,3,15,2,2,2,8,1,1 ,5,3,6,6,2,1,6,6,6,6,2,1 ,1 ,1 ,1 , 2};

    const QString ETIQUETTE = "ABCDEFGHIJKLMNOPQRSTU";
    const QString FOND[15] =
    {
        "T  d   T   d  T",
        " D   t   t   D ",
        "  D   d d   D  ",
        "d  D   d   D  d",
        "    D     D    ",
        " t   t   t   t ",
        "  d   d d   d  ",
        "T  d   c   d  T",
        "  d   d d   d  ",
        " t   t   t   t ",
        "    D     D    ",
        "d  D   d   D  d",
        "  D   d d   D  ",
        " D   t   t   D ",
        "T  d   T   d  T"
    };


    int nombreDeJoueur =2;
    int joueurActif = 0;
    char lettresAffichees[NBR_CELL][NBR_CELL];
    int pointageJoueur[NBR_MAX_JOUEUR];
    QString lettresJoueur[NBR_MAX_JOUEUR];

    QLineEdit *affichageLettre;
    QLineEdit *affichagePointage[NBR_MAX_JOUEUR];

    QLineEdit *mot;
    QComboBox *choixLigne;
    QComboBox *choixColonne;
    QComboBox *choixDirection;
    QGraphicsScene *scene;

    QSoundEffect *buzzer;

    bool premierCoup = true;
    void dessinerJeu();
    void initialiserJeu();
    void rafraichirJeu(int aCol, int aLigne);
    void dessinerPiece(int aCol, int aLigne, char aJoueurId);

    void jouer(int aDirection);
    int verifierGagnant(int aCol, int aLigne);
    int calculerPointageMot(int aCol, int aLigne, int aDirection, QString aMot);
    int trouverDebutMot(int aCol, int aLigne, int aDeltaX, int aDeltaY);
    int calculerPointageMot2(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot, bool recherchePerpendiculaire);
    int calculerValeurTuile(char aLettre);
    int valeurMultiplicateurTuile(int x, int y);
    int valeurMultiplicateurMot(int x, int y);
    bool verifierSiMotEntre(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot);
    bool verifierSiMotVoisin(int aCol, int aLigne, int aDeltaX, int aDeltaY, QString aMot);
    bool verifierSiAuCentre(int x, int y, int deltaX, int deltaY, int longueur);
    bool verifierLettresValides(QString aMot);
};
#endif // FENETREPRINCIPALE_H
