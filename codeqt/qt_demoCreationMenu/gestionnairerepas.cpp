#include "gestionnairerepas.h"
#include <string.h>
#include <stdio.h>
#include <cstdlib>

GestionnaireRepas::GestionnaireRepas(char *aNomFichier)  // D6
{
    strcpy( nomFichier, aNomFichier); // D8
}


/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void GestionnaireRepas::chargerMenu(menu *aMenu) {  // D9
    FILE *lFichier = fopen(nomFichier, "rb");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = (repas *)calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = (item *)calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = (repas **)realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}

/**
 * @brief sauvegarde sur disque du menu au complet
 *
 * par simplicité, le fichier est écrasé à chaque écriture
 *
 * @param aMenu le menu à sauvegarder
 */
void GestionnaireRepas::sauvegarderMenu(menu aMenu) { //E1
    FILE *lFichier = fopen(nomFichier, "wb");
    repasFichier lRepasASauvegarder;
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        strcpy(lRepasASauvegarder.titre, aMenu.listeRepas[i]->titre);
        lRepasASauvegarder.nombreItem =  aMenu.listeRepas[i]->nombreItem;
        fwrite(&lRepasASauvegarder, sizeof(repasFichier),1, lFichier);
        for(int j = 0; j<aMenu.listeRepas[i]->nombreItem; j++) {
            fwrite(&aMenu.listeRepas[i]->listeItem[j], sizeof(item), 1, lFichier);
        }
    }
    fclose(lFichier);
}
