#ifndef GESTIONNAIREREPAS_H
#define GESTIONNAIREREPAS_H

#include "repas.h"

class GestionnaireRepas
{
public:
    GestionnaireRepas(char *aNomFichier); // D7
    void chargerMenu(menu *aMenu);

    void sauvegarderMenu(menu aMenu);
private:
    char nomFichier[50];  // D5
};

#endif // GESTIONNAIREREPAS_H
