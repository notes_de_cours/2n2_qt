#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QGroupBox>
#include <QMainWindow>
#include "repas.h"

class QPushButton;
class QTableWidget;
class QLineEdit;
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(QWidget *parent = nullptr);
    ~FenetrePrincipale();
private slots:
    void slotAjouterRepas();
    void slotAjouterItem();
    void slotItemModifie(QString valeur);
    void slotQuitter();
    void slotChargerMenu();
    void slotSauvegarderMenu();
    void slotViderMenu();
    void slotEffacerItem();
private:
    QGroupBox *constructionBoiteEntrees();
    QGroupBox *constructionSectionAffichage();
    void convertirMenuEnQTableWidget(menu *aMenu);

    QLineEdit *nomRepas; // 21
    QTableWidget *affichageMenu; // 23
    QLineEdit *nomItem;
    QLineEdit *prixItem;

    QPushButton *boutonAjoutRepas; // C1
    QPushButton *boutonAjoutItem;
    menu convertirQTableIWidgetEnMenu(QTableWidget *aTable);
};
#endif // FENETREPRINCIPALE_H
