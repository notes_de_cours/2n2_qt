#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include "fenetreprincipale.h"
#include <QMenu>
#include <QMenuBar>
#include <QApplication>

#include "gestionnairerepas.h"

FenetrePrincipale::FenetrePrincipale(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600); // 1
    QMenu *fichierMenu = menuBar()->addMenu(tr("&Fichier")); // D1
    fichierMenu->addAction("Quitter", this, &FenetrePrincipale::slotQuitter);

    QMenu *menuMenu = menuBar()->addMenu(tr("&Menu"));  // D2
    menuMenu->addAction("Charger", this, &FenetrePrincipale::slotChargerMenu);
    menuMenu->addAction("Sauvegarder", this, &FenetrePrincipale::slotSauvegarderMenu); // E2
    menuMenu->addAction("Vider", this, &FenetrePrincipale::slotViderMenu); // E3

    QWidget *fenetre = new QWidget(); // 2a
    QHBoxLayout *grille = new QHBoxLayout; // 3

    grille->addWidget(constructionBoiteEntrees()); // 4 // 9
    grille->addWidget(constructionSectionAffichage()); // 17

    fenetre->setLayout(grille); // 3a
    setCentralWidget(fenetre);  // 2b
}

FenetrePrincipale::~FenetrePrincipale()
{
}

/**
 * @brief construction de la section pour ajouter un repas et ses items
 * @return un QGroupBox contenant les widgets servant à faire l'entrée du repas et de ses items
 */
QGroupBox *FenetrePrincipale::constructionBoiteEntrees() {
    //création de l'entrée pour le nom du repas
    nomRepas = new QLineEdit; // 5 //22
    nomRepas->setFocus();
    QFormLayout *nomRepasForm = new QFormLayout; // 5a
    nomRepasForm->addRow("Nom du repas:", nomRepas);

    //le bouton pour sauvegarder le nom du repas
    boutonAjoutRepas = new QPushButton("Ajouter le repas"); //18 // C2
    connect(boutonAjoutRepas, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterRepas); // 19

    //création de l'entrée pour le nom et le prix de l'item
    nomItem = new QLineEdit; // B1
    prixItem = new QLineEdit;
    prixItem->setInputMask("09.00"); //B2
    connect(nomItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie); // C7
    connect(prixItem, &QLineEdit::textEdited, this, &FenetrePrincipale::slotItemModifie);

    QFormLayout *champsItemLayout = new QFormLayout; // B3
    champsItemLayout->addRow("Nom de l'item:", nomItem);
    champsItemLayout->addRow("Prix de l'item", prixItem);

    boutonAjoutItem = new QPushButton("Ajouter l'item"); // B4 // C2a
    boutonAjoutItem->setEnabled(false); // C3 // disable par defaut, tant qu'un prix n'est pas entré.

    connect(boutonAjoutItem, &QPushButton::clicked, this, &FenetrePrincipale::slotAjouterItem);

    // bouton pour effacer  un item
    QPushButton *boutonEffacerItem = new QPushButton("Effacer la ligne sélectionnée"); // E12
    connect(boutonEffacerItem, &QPushButton::clicked, this, &FenetrePrincipale::slotEffacerItem);

    //Construction de la boite pour les Entrées
    QVBoxLayout *grille = new QVBoxLayout; // 6
    QGroupBox *boite = new QGroupBox("Entrées");

    grille->addLayout(nomRepasForm); // 7
    grille->addWidget(boutonAjoutRepas); // 20
    grille->addLayout(champsItemLayout); // B5
    grille->addWidget(boutonAjoutItem);
    grille->addWidget(boutonEffacerItem); // E12a

    boite->setLayout(grille);

    return boite; // 8
}


/**
 * @brief Construction de la section servant à afficher les repas et leurs items
 * @return un QGroupBox contenant les widgets servant à afficher les repas et leurs items
 */
QGroupBox *FenetrePrincipale::constructionSectionAffichage() {
    affichageMenu = new QTableWidget; // 10 // 24
    affichageMenu->setColumnCount(3);  // 10b
    //met la table en lecture seulement
    affichageMenu->setEditTriggers(QAbstractItemView::NoEditTriggers); // 11

    //change le mode et le comportement de la selection
    affichageMenu->setSelectionBehavior( QAbstractItemView::SelectRows ); // E13
    affichageMenu->setSelectionMode( QAbstractItemView::SingleSelection );

    QStringList entete; //12
    entete.append("Repas");
    entete.append("Item");
    entete.append("Prix");
    affichageMenu->setHorizontalHeaderLabels(entete);
    affichageMenu->verticalHeader()->setVisible(false); // 13

    QVBoxLayout *grille = new QVBoxLayout; // 14
    QGroupBox *boite = new QGroupBox;  // 15
    grille->addWidget(affichageMenu);  //15a
    boite->setLayout(grille); // 15b
    return boite; // 16
}


void FenetrePrincipale::convertirMenuEnQTableWidget(menu *aMenu)
{
    for(int i = 0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        affichageMenu->insertRow(affichageMenu->rowCount());
        affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(QString::fromLatin1(lRepas->titre)));
        for(int j=0; j<lRepas->nombreItem; j++) {
            item lItem = lRepas->listeItem[j];
            affichageMenu->insertRow(affichageMenu->rowCount());
            affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(QString::fromLatin1(lItem.titre)));
            affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(QString::number(lItem.prix,'f', 2)));
        }
    }
}

menu FenetrePrincipale::convertirQTableIWidgetEnMenu(QTableWidget *aTable) // E5
{
    menu lMenu;
    repas *lRepas;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    for(int i = 0; i<aTable->rowCount(); i++) {
        if((aTable->item(i,0))) {
            //s'il y a qqchose dans la colonne 0, c'est donc un repas
            lRepas = (repas *)calloc(1, sizeof(repas)); //sur le tas
            //conversion du titre qui est dans la QString qui est dans la table, vers char*
            QByteArray ba = aTable->item(i,0)->text().toLocal8Bit(); // E6
            strcpy(lRepas->titre, ba.data()); // E7
            if(!lMenu.listeRepas) { // E8
                lMenu.listeRepas = (repas **)calloc(1, sizeof(repas*));
            } else {
                lMenu.listeRepas = (repas **)realloc(lMenu.listeRepas, (lMenu.nombreRepas+1) *sizeof(repas*));
            }
            lMenu.listeRepas[lMenu.nombreRepas] = lRepas;
            lMenu.nombreRepas++;
        } else {
            //sinon c'est un item
            if(!lRepas->listeItem) { // E9
                lRepas->listeItem = (item *)calloc(1, sizeof(item));
            } else {
                lRepas->listeItem = (item *)realloc(lRepas->listeItem, sizeof(item)*(lRepas->nombreItem+1));
            }
            QByteArray ba = aTable->item(i,1)->text().toLocal8Bit();
            strcpy(lRepas->listeItem[lRepas->nombreItem].titre, ba.data());
            //conversion du prix qui est dans la QSTring vers float.
            lRepas->listeItem[lRepas->nombreItem].prix = aTable->item(i,2)->text().toFloat(); // E10
            lRepas->nombreItem++;
        }
    }
    return lMenu;
}
// slots
void FenetrePrincipale::slotAjouterRepas() {
  affichageMenu->insertRow(affichageMenu->rowCount()); // 25
  affichageMenu->setItem(affichageMenu->rowCount()-1, 0, new QTableWidgetItem(nomRepas->text())); // 26
  nomRepas->clear(); // 27
  //boutonAjoutItem->setEnabled(true); // C4
  boutonAjoutRepas->setEnabled(false); // C5
}

void FenetrePrincipale::slotAjouterItem() { //B6
    affichageMenu->insertRow(affichageMenu->rowCount());
    affichageMenu->setItem(affichageMenu->rowCount()-1, 1, new QTableWidgetItem(nomItem->text()));
    affichageMenu->setItem(affichageMenu->rowCount()-1, 2, new QTableWidgetItem(prixItem->text()));
    nomItem->clear();
    prixItem->clear();
    boutonAjoutRepas->setEnabled(true); // C6
    boutonAjoutItem->setEnabled(false); // C9
}

/**
 * @brief Change l'état du bouton de sauvegarde d'item basé sur le text du nom et du prix.
 * Le bouton de sauvegarde de l'item sera "enabled" seulement s'il y a du text dans le
 * nom et dans le prix.
 */
void FenetrePrincipale::slotItemModifie(QString valeur) {
    // le text du prix contient "." par defaut à cause du inputMask.
    if (nomItem->text().isEmpty() || prixItem->text()=="." ) { // C8
        boutonAjoutItem->setEnabled(false);
    } else {
        boutonAjoutItem->setEnabled(true);
    }
}


/**
 * @brief sortie de l'application
 */
void FenetrePrincipale::slotQuitter() { // D3
    QApplication::quit();
}

void FenetrePrincipale::slotChargerMenu() {  // D4
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt"); //D10
    menu lMenu;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    gestionRepas->chargerMenu(&lMenu); // D11
    convertirMenuEnQTableWidget(&lMenu); // D12
}

void FenetrePrincipale::slotSauvegarderMenu() { // E4
    GestionnaireRepas *gestionRepas = new GestionnaireRepas("menu.txt");
    gestionRepas->sauvegarderMenu(convertirQTableIWidgetEnMenu(affichageMenu));
}

void FenetrePrincipale::slotViderMenu() { // E11
    affichageMenu->setRowCount(0);

}

void FenetrePrincipale::slotEffacerItem() { // E14
    QList itemsSelectionnes = affichageMenu->selectedRanges();
    if(!itemsSelectionnes.isEmpty()) {
        int ligneSelectionnee = itemsSelectionnes.first().topRow();
        affichageMenu->removeRow(ligneSelectionnee);
        affichageMenu->clearSelection();
    }
}
