/****************************************************************************
** Meta object code from reading C++ file 'fenetreprincipale.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../qt_creationMenu2/fenetreprincipale.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fenetreprincipale.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
namespace {
struct qt_meta_stringdata_FenetrePrincipale_t {
    uint offsetsAndSizes[22];
    char stringdata0[18];
    char stringdata1[12];
    char stringdata2[1];
    char stringdata3[10];
    char stringdata4[16];
    char stringdata5[8];
    char stringdata6[13];
    char stringdata7[12];
    char stringdata8[12];
    char stringdata9[7];
    char stringdata10[12];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_FenetrePrincipale_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_FenetrePrincipale_t qt_meta_stringdata_FenetrePrincipale = {
    {
        QT_MOC_LITERAL(0, 17),  // "FenetrePrincipale"
        QT_MOC_LITERAL(18, 11),  // "chargerMenu"
        QT_MOC_LITERAL(30, 0),  // ""
        QT_MOC_LITERAL(31, 9),  // "viderMenu"
        QT_MOC_LITERAL(41, 15),  // "sauvegarderMenu"
        QT_MOC_LITERAL(57, 7),  // "quitter"
        QT_MOC_LITERAL(65, 12),  // "ajouterRepas"
        QT_MOC_LITERAL(78, 11),  // "ajouterItem"
        QT_MOC_LITERAL(90, 11),  // "itemModifie"
        QT_MOC_LITERAL(102, 6),  // "valeur"
        QT_MOC_LITERAL(109, 11)   // "effacerItem"
    },
    "FenetrePrincipale",
    "chargerMenu",
    "",
    "viderMenu",
    "sauvegarderMenu",
    "quitter",
    "ajouterRepas",
    "ajouterItem",
    "itemModifie",
    "valeur",
    "effacerItem"
};
#undef QT_MOC_LITERAL
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_FenetrePrincipale[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,   62,    2, 0x08,    1 /* Private */,
       3,    0,   63,    2, 0x08,    2 /* Private */,
       4,    0,   64,    2, 0x08,    3 /* Private */,
       5,    0,   65,    2, 0x08,    4 /* Private */,
       6,    0,   66,    2, 0x08,    5 /* Private */,
       7,    0,   67,    2, 0x08,    6 /* Private */,
       8,    1,   68,    2, 0x08,    7 /* Private */,
      10,    0,   71,    2, 0x08,    9 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject FenetrePrincipale::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_FenetrePrincipale.offsetsAndSizes,
    qt_meta_data_FenetrePrincipale,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_FenetrePrincipale_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<FenetrePrincipale, std::true_type>,
        // method 'chargerMenu'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'viderMenu'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'sauvegarderMenu'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quitter'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'ajouterRepas'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'ajouterItem'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'itemModifie'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<QString, std::false_type>,
        // method 'effacerItem'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void FenetrePrincipale::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<FenetrePrincipale *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->chargerMenu(); break;
        case 1: _t->viderMenu(); break;
        case 2: _t->sauvegarderMenu(); break;
        case 3: _t->quitter(); break;
        case 4: _t->ajouterRepas(); break;
        case 5: _t->ajouterItem(); break;
        case 6: _t->itemModifie((*reinterpret_cast< std::add_pointer_t<QString>>(_a[1]))); break;
        case 7: _t->effacerItem(); break;
        default: ;
        }
    }
}

const QMetaObject *FenetrePrincipale::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FenetrePrincipale::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FenetrePrincipale.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int FenetrePrincipale::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
