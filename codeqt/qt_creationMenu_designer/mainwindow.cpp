#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QActionGroup>
#include <QDir>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    createLanguageMenu();

    // macOS setNativeMenuBar(false)
    ui->menubar->setNativeMenuBar(false);

    QString resourceFileName = ":/traduction/Langages/TranslationExample_fr.qm";
    switchTranslator(m_translator, resourceFileName);


}

MainWindow::~MainWindow()
{
    delete ui;
}

// we create the menu entries dynamically, dependent on the existing translations.
void MainWindow::createLanguageMenu(void)
{
    // format systems language
    QString defaultLocale = QLocale::system().name(); // e.g. "de_DE"
    defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"
    m_langPath = ":/traduction";
    m_langPath.append("/Langages");
    QDir dir(m_langPath);
    qDebug("exists %s ", dir.exists() ? "true" : "false" );

    // Create Language Menu to match qm translation files found
    QActionGroup* langGroup = new QActionGroup(ui->menuLangage);
    langGroup->setExclusive(true);
    connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));
    qDebug("%s", m_langPath.toLatin1().data());

    QStringList fileNames = dir.entryList(QStringList("TranslationExample_*.qm"));
    for (int i = 0; i < fileNames.size(); ++i) {
        // get locale extracted by filename
        QString locale;
        locale = fileNames[i]; // "TranslationExample_de.qm"

        locale.truncate(locale.lastIndexOf('.')); // "TranslationExample_de"
        locale.remove(0, locale.indexOf('_') + 1); // "de"

        QString lang = QLocale::languageToString(QLocale(locale).language());
        QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(locale));

        QAction *action = new QAction(ico, lang, this);
        action->setCheckable(true);
        // action->setData(resourceFileName);
        action->setData(locale);

        ui->menuLangage->addAction(action);
        langGroup->addAction(action);

        // set default translators and language checked
        if (defaultLocale == locale)
        {
            action->setChecked(true);
        }
    } // for: end
}

// Called every time, when a menu entry of the language menu is called
void MainWindow::slotLanguageChanged(QAction* action)
{
    if(0 == action) {
        return;
    }

    // load the language dependant on the action content
    qDebug("action->data %s", action->data().toString().toLatin1().data());
    loadLanguage(action->data().toString());
    setWindowIcon(action->icon());
    QMessageBox::question(
        this,
        tr("Application Name"),
        tr("An information message."),

        QMessageBox::Yes |
            QMessageBox::No |
            QMessageBox::Cancel,

        QMessageBox::Cancel );
}
void MainWindow::switchTranslator(QTranslator& translator, const QString& filename)
{
    // remove the old translator
    qApp->removeTranslator(&translator);

    // load the new translator
    bool result = translator.load(filename);
    qDebug("translator.load(%s) %s", filename.toLatin1().data(), result ? "true" : "false" );

    if(!result) {
        qWarning("*** Failed translator.load(\"%s\")", filename.toLatin1().data());
        return;
    }
    qApp->installTranslator(&translator);
}

void MainWindow::loadLanguage(const QString& rLanguage)
{
    if(m_currLang == rLanguage) {
        return;
    }
    m_currLang = rLanguage;
    qDebug("loadLanguage %s", rLanguage.toLatin1().data());

    QLocale locale = QLocale(m_currLang);
    QLocale::setDefault(locale);
    QString languageName = QLocale::languageToString(locale.language());

    // m_translator contains the app's translations
    QString resourceFileName = QString("%1/TranslationExample_%2.qm").arg(m_langPath).arg(rLanguage);
    switchTranslator(m_translator, resourceFileName);

    // m_translatorQt contains the translations for qt
    // Why do I need this ? Common dialogs?
    //
    // http://doc.qt.io/qt-5/linguist-programmers.html
    // switchTranslator(m_translatorQt, QString("qt_%1.qm").arg(rLanguage));

    //ui->statusBar->showMessage(tr("Current Language changed to %1").arg(languageName));
}


void MainWindow::changeEvent(QEvent* event)
{
    if(0 != event) {
        switch(event->type()) {
        // this event is send if a translator is loaded
        case QEvent::LanguageChange:
            // UI will not update unless you call retranslateUi
            ui->retranslateUi(this);
            break;

            // this event is send, if the system, language changes
        case QEvent::LocaleChange:
        {
            QString locale = QLocale::system().name();
            locale.truncate(locale.lastIndexOf('_'));
            loadLanguage(locale);
        }
            break;
        }
    }
    QMainWindow::changeEvent(event);
}

// slots
void MainWindow::on_boutonAjoutRepas_clicked()
{


    QTableWidget *table = ui->affichageMenu;
    table->insertRow(table->rowCount());
    table->setItem(table->rowCount()-1, 0, new QTableWidgetItem(ui->nomRepas->text()));
    ui->plainTextEdit->appendPlainText(ui->nomRepas->text());
    ui->nomRepas->clear();
    ui->boutonAjoutRepas->setEnabled(false);
    ui->nomRepas->setEnabled(false);
    ui->nomItem->setEnabled(true);
    ui->prixItem->setEnabled(true);

}


void MainWindow::on_boutonAjoutItem_clicked()
{

    QTableWidget *table = ui->affichageMenu;
    table->insertRow(table->rowCount());
    table->setItem(table->rowCount()-1, 1, new QTableWidgetItem(ui->nomItem->text()));
    table->setItem(table->rowCount()-1, 2, new QTableWidgetItem(ui->prixItem->text()));
    QString toto = "\t" + ui->nomItem->text() + "\t" + ui->prixItem->text();
    ui->plainTextEdit->appendPlainText(toto);

    ui->nomItem->clear();
    ui->prixItem->clear();
    ui->nomRepas->setEnabled(true);
    ui->boutonAjoutItem->setEnabled(false);

    ui->affichageMenu->clearSelection();
}


void MainWindow::on_nomItem_textEdited(const QString &arg1)
{
    if(ui->nomItem->text().isEmpty() || ui->prixItem->text() == "."){
        ui->boutonAjoutItem->setEnabled(false);
    } else {
        ui->boutonAjoutItem->setEnabled(true);
    }
}


void MainWindow::on_prixItem_textEdited(const QString &arg1)
{
    on_nomItem_textEdited(arg1);

}


void MainWindow::on_nomRepas_textEdited(const QString &arg1)
{
    if(ui->nomRepas->text().isEmpty()){
        ui->boutonAjoutRepas->setEnabled(false);
    } else {
        ui->boutonAjoutRepas->setEnabled(true);
    }
}




void MainWindow::on_affichageMenu_customContextMenuRequested(const QPoint &pos)
{
    itemSelectionne = ui->affichageMenu->itemAt(pos);
    qDebug() << itemSelectionne;
    if(itemSelectionne) {
        QMenu contextMenu(tr("Context menu"), this);
        QAction action1(tr("Effacer l'item"), this);
        connect(&action1, SIGNAL(triggered()), this, SLOT(removeDataPoint()));
        contextMenu.addAction(&action1);
        contextMenu.exec(ui->affichageMenu->mapToGlobal(pos));
    }
}

void MainWindow::enleverLigne()
{
    ui->affichageMenu->removeRow(itemSelectionne->row());
}
