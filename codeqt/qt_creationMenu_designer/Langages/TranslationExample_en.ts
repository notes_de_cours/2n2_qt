<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="32"/>
        <source>Nom du repas</source>
        <translation>Meal Name</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="47"/>
        <source>Ajouter le repas</source>
        <translation>Add a meal</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="56"/>
        <source>Nom de l&apos;item</source>
        <translation>Item name</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Prix de l&apos;item</source>
        <translation>Item price</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="80"/>
        <source>09.00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="92"/>
        <source>Ajouter l&apos;item</source>
        <translation>Add item</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="134"/>
        <location filename="../mainwindow.ui" line="171"/>
        <source>Repas</source>
        <translation>Meal</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="139"/>
        <location filename="../mainwindow.ui" line="184"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="144"/>
        <location filename="../mainwindow.ui" line="197"/>
        <source>Prix</source>
        <translation>Price</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Repas 	Item 	Prix</source>
        <translation>Meal \t Item \t Price</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="212"/>
        <source>Langage</source>
        <translation>Language</translation>
    </message>
    <message>
        <source>message</source>
        <translation type="vanished">messageAnglais</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>Application Name</source>
        <translation>en app name</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>An information message.</source>
        <translation>en message</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="232"/>
        <source>Context menu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>Effacer l&apos;item</source>
        <translation>Delete item</translation>
    </message>
</context>
</TS>
