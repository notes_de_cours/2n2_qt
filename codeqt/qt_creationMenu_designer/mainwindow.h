#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>

QT_BEGIN_NAMESPACE

class QTableWidgetItem;
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
        // this event is called, when a new translator is loaded or the system language is changed
        void changeEvent(QEvent*);

    protected slots:
        // this slot is called by the language menu actions
        void slotLanguageChanged(QAction* action);

private slots:

    void on_boutonAjoutRepas_clicked();

    void on_boutonAjoutItem_clicked();

    void on_nomItem_textEdited(const QString &arg1);

    void on_prixItem_textEdited(const QString &arg1);

    void on_nomRepas_textEdited(const QString &arg1);


    void on_affichageMenu_customContextMenuRequested(const QPoint &pos);

    void enleverLigne();
private:

    QTableWidgetItem *itemSelectionne;
    Ui::MainWindow *ui;
    QTranslator m_translator; // contains the translations for this application
    QTranslator m_translatorQt; // contains the translations for qt

    QString m_currLang; // contains the currently loaded language
    QString m_langPath; // Path of language files. This is always fixed to /languages.

    // loads a language by the given language shortcur (e.g. de, en)
    void loadLanguage(const QString& rLanguage);

    // creates the language menu dynamically from the content of m_langPath
    void createLanguageMenu(void);

    void switchTranslator(QTranslator &translator, const QString &filename);
};
#endif // MAINWINDOW_H
