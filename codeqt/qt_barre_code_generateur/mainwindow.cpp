
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsLineItem>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

}

void MainWindow::ajouterLettre(int lettre, int* pos)
{
    int couleur = 1;

    for(int i = 0; i< 9; i++) {
        int largeur = code39[lettre][i];
        ajouterLigne(10+ *pos,10,largeur,couleur);
        couleur = (couleur+1)%2;
        *pos+=largeur*TAILLE+ 1;
    }
    ajouterLigne(10+ *pos,10,0,1);
    *pos+=TAILLE+1;
}
void  MainWindow::ajouterLigne(int x, int y, int largeur, int valeur)
{
    QPen crayon;
    QBrush brosse;
    if(valeur == 1) {
        crayon = QPen(Qt::black);
        brosse = QBrush(Qt::black);
    } else {
        crayon = QPen(Qt::white);
        brosse = QBrush(Qt::white);
    }

    scene->addRect(x,y,largeur*TAILLE,40,crayon, brosse);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::validerLettres(QString mot)
{
    bool valide = true;
    for(int i=0; i<mot.length();i++) {
        if(CONVERSION.indexOf(mot[i].toUpper()) == -1) {
            valide = false;
        }
    }
    return valide;
}

void MainWindow::on_pushButton_clicked()
{
    QString mot = ui->lineEdit->text();
    if(validerLettres(mot)) {
        int pos=0;
        ajouterLettre(43,&pos);
        for(int i =0;i<mot.length();i++) {
            qDebug() << mot[i].unicode();
            int lettre = CONVERSION.indexOf(mot[i].toUpper());
            ajouterLettre(lettre,&pos);

        }
        ajouterLettre(43,&pos);
    } else {
        qDebug() << "pas bon";
    }
}

